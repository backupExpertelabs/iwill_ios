//
//  WitnessTableViewCell.m
//  IWill
//
//  Created by A1AUHAIG on 4/26/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "WitnessTableViewCell.h"
#import "Utils.h"

@implementation WitnessTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    _deleteWitnessBtn.layer.cornerRadius = 2;
    _deleteWitnessBtn.layer.masksToBounds = NO;
    _deleteWitnessBtn.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    _deleteWitnessBtn.layer.shadowOpacity = 0.3;
    _deleteWitnessBtn.layer.shadowRadius = 4;
    _deleteWitnessBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    _backImage.layer.cornerRadius = 3;
    _backImage.layer.borderWidth =0.5;
    _backImage.layer.borderColor = [Utils getBorderLineColor].CGColor;

    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

@end
