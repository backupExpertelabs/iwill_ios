//
//  WitnessTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 4/26/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WitnessTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *witnessName;
@property (weak, nonatomic) IBOutlet UILabel *relationshipNa;
@property (weak, nonatomic) IBOutlet UILabel *firstNameText;
@property (weak, nonatomic) IBOutlet UILabel *lastNameText;
@property (weak, nonatomic) IBOutlet UILabel *executerType;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *deleteWitnessBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;


@end
