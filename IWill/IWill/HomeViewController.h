//
//  HomeViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIButton *profileInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *witnessInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *executiveInfoBtn;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic)  NSDictionary *dataDict;

-(IBAction)backBtnAction:(id)sender;
-(IBAction)profileInfoBtnAction:(id)sender;
-(IBAction)witnessInfoBtnAction:(id)sender;
-(IBAction)executiveInfoBtnAction:(id)sender;

-(IBAction)lagecyBtnAction:(id)sender;
-(IBAction)lagecyNoteBtnAction:(id)sender;


@end
