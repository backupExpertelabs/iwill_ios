//
//  LoginViewController.m
//  IWill
//
//  Created by A1AUHAIG on 4/13/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "LoginViewController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "MyProfileViewController.h"
#import "Constant.h"
#import "HomeViewController.h"

@interface LoginViewController ()<UITextFieldDelegate,ConnectionManager_Delegate,UIAlertViewDelegate>

{
    ConnectionManager *connectionManager;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    [self initViewWthLayer];
    
    HomeViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    
}


-(void)initViewWthLayer


{
    //self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
    self.userNameTxt.layer.borderWidth = 0.5;
    self.userNameTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userNameTxt.layer.cornerRadius = 4;
    self.userNameTxt.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    self.passwordText.layer.borderWidth = 0.5;
    self.passwordText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.passwordText.layer.cornerRadius = 4;
    self.passwordText.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
     self.enterBtn.layer.cornerRadius = 4;
    
    
    self.userNameTxt.text =@"testator1@iwill.com";
    self.passwordText.text = @"testator1";
    

    
}



- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)enterBtnAction:(id)sender

{
    
//    MyProfileViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    [self.navigationController pushViewController:vc animated:YES];
    
    if([self checkIsValid])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:_userNameTxt.text forKey:@"email"];
        [dataDict setValue:_passwordText.text forKey:@"password"];
        
        NSLog(@"hdsghj %@",dataDict);
        
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"login_user_new"];
    }
    else{
        
    }
}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:alertTitle message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    });
    
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        
        //        UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
        //        [self.navigationController pushViewController:vc animated:YES];
        
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==1) {
            
            [[NSUserDefaults standardUserDefaults]setObject:[aResponceDic valueForKey:@"Result"] forKey:@"DicKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            HomeViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            
            vc.dataDict = [aResponceDic valueForKey:@"Result"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==0){
            
            [[NSUserDefaults standardUserDefaults]setObject:[aResponceDic valueForKey:@"Result"] forKey:@"DicKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            HomeViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            
            vc.dataDict = [aResponceDic valueForKey:@"Result"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else{
            
            [[NSUserDefaults standardUserDefaults]setObject:[aResponceDic valueForKey:@"Result"] forKey:@"DicKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            HomeViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            
            vc.dataDict = [aResponceDic valueForKey:@"Result"];
            [self.navigationController pushViewController:vc animated:YES];
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}
-(BOOL)checkIsValid
{
    
    if(_userNameTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:emailMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    if(_passwordText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:passwordMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    return YES;
}



- (IBAction)forgotPasswordBtnAction:(id)sender

{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
//    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"];
//    [self.navigationController pushViewController:vc animated:YES];
    
    
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Forgot Password" message:@"Enter your Email id" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
//    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [alert addButtonWithTitle:@"Submit"];
//    [alert show];
    
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Index =%ld",(long)buttonIndex);
    if (buttonIndex == 1) {  //Login
        UITextField *username = [alertView textFieldAtIndex:0];
        NSLog(@"username: %@", username.text);
        
        if (username.text.length ==0)
        {
            [Utils showAlertView:alertTitle message:@"Please enter your email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        else{
            [self hitAPIForForgotPassword:username.text];
        }
        
       
    }
}



-(void)hitAPIForForgotPassword:(NSString * )emailId
{
    
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
    
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:emailId forKey:@"email"];
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"forgetPassword"];

    
}


- (IBAction)signupBtnAction:(id)sender

{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
    
//    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddWitnessesViewController"];
//    [self.navigationController pushViewController:vc animated:YES];
    
    //SignUpViewController
    //AddWitnessesViewController
    
}
- (IBAction)segmenyAction:(id)sender {
}
@end
