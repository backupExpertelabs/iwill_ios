//
//  AddWitness4ViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/12/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AddWitness4ViewController.h"
#import "ConnectionManager.h"
#import "Utils.h"
#import "Constant.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface AddWitness4ViewController ()<UITextFieldDelegate,UIAlertViewDelegate,ConnectionManager_Delegate>
{
    ConnectionManager * connectionManager;
    UITextField * activeField;
}

@end

@implementation AddWitness4ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    
    
    self.datebackgroundView.hidden = YES;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    
    [self registerForKeyboardNotifications];
    [self initViewWthLayer];
    
   // [_backBtn addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void)initViewWthLayer


{
    self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
    
    
    
    
    self.witnessBtnView = [Utils setMaskTo:self.witnessBtnView byRoundingCorners:UIRectCornerAllCorners];
    
    
    self.witnessBtnView.layer.cornerRadius = 2;
    self.witnessBtnView.layer.masksToBounds = NO;
    self.witnessBtnView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.witnessBtnView.layer.shadowOpacity = 0.3;
    self.witnessBtnView.layer.shadowRadius = 4;
    self.witnessBtnView.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self. addressText.delegate = self;
    self.dobText.delegate = self;
    self.phoneNumberText.delegate = self;
    self. permanentMarkText.delegate = self;
    self. socialSecurityNumberText.delegate = self;
    self. relationshitToTestatorText.delegate = self;
    self. nameOfChieldText.delegate = self;
    self. nameOfSpouseText.delegate = self;
    
    
}


- (IBAction)enterBtnAction:(id)sender
{
    
    
    if([self checkIsValid])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        [dataDict setValue:_firstNameText.text forKey:@"first_name"];
        [dataDict setValue:_lastNameText.text forKey:@"last_name"];
        [dataDict setValue:_emailText.text forKey:@"email"];
        [dataDict setValue:_dobText.text forKey:@"dob"];
        [dataDict setValue:_addressText.text forKey:@"address"];
        [dataDict setValue:_phoneNumberText.text forKey:@"mobile"];
        [dataDict setValue:_permanentMarkText.text forKey:@"permanent_mark"];
        [dataDict setValue:_nameOfChieldText.text forKey:@"child_name"];
        [dataDict setValue:_nameOfSpouseText.text forKey:@"spouse_name"];
        [dataDict setValue:_relationshitToTestatorText.text forKey:@"reletionship"];
        [dataDict setValue:_socialSecurityNumberText.text forKey:@"security_code"];
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addWitness"];
    }
    else{
        
    }
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:@"You have added two witness. Do you want to add another witness." delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
            
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if(alertView.tag ==100 && buttonIndex == 0)
    {
        
    }
    
    if(alertView.tag ==100 && buttonIndex == 1)
    {
        
    }
}


-(BOOL)checkIsValid
{
    
    if(_firstNameText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessFirstName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_lastNameText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessLastName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_dobText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessDOB delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_relationshitToTestatorText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessRelationship delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if(_emailText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessEmail delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if([Utils validateEmail:_emailText.text])
    {
        [Utils showAlertView:alertTitle message:emailValidationMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if(_phoneNumberText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessPhoneNumber delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    if(_permanentMarkText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessPermanentMark delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if(_nameOfSpouseText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessSpouseName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    if(_nameOfChieldText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:witnessChildName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    if(_socialSecurityNumberText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:withnessSSNNumber delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    if(_socialSecurityNumberText.text.length <4)
    {
        [Utils showAlertView:alertTitle message:witnessSSNNumberValidation delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    return YES;
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    if(textField == self.dobText)
    {
        
        _datebackgroundView.hidden = NO;
        return NO;
    }
    
    else
    {
        return YES;
    }
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == self.socialSecurityNumberText)
    {
        NSInteger textLenth = self.socialSecurityNumberText.text.length;
        
        if(textLenth == 4 && string.length>0)
        {
            return NO;
        }
        
    }
    
    return YES;
}




-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+50, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    
    //}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}


-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}


- (IBAction)addOneWitBtnAction:(id)sender {
}

- (IBAction)addTwoWitBtnAction:(id)sender {
}
- (IBAction)addThreeWitBtnAction:(id)sender {
}


- (IBAction)dateSelectedBtnAction:(id)sender
{
    _dobText.text = [Utils getDateFromDateForAPI:_iWilldatePicker.date];
    _datebackgroundView.hidden = YES;
}

@end
