//
//  AddWitness3ViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomeTextField.h"

@interface AddWitness3333ViewController : UIViewController
@property (weak, nonatomic) IBOutlet CustomeTextField *firstNameText;
@property (weak, nonatomic) IBOutlet CustomeTextField *lastNameText;
@property (weak, nonatomic) IBOutlet CustomeTextField *emailText;
@property (weak, nonatomic) IBOutlet CustomeTextField *addressText;
@property (weak, nonatomic) IBOutlet CustomeTextField *dobText;
@property (weak, nonatomic) IBOutlet CustomeTextField *phoneNumberText;
@property (weak, nonatomic) IBOutlet CustomeTextField *permanentMarkText;
@property (weak, nonatomic) IBOutlet CustomeTextField *socialSecurityNumberText;
@property (weak, nonatomic) IBOutlet CustomeTextField *relationshitToTestatorText;
@property (weak, nonatomic) IBOutlet CustomeTextField *nameOfChieldText;
@property (weak, nonatomic) IBOutlet CustomeTextField *nameOfSpouseText;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
- (IBAction)enterBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *witnessBtnView;
@property (weak, nonatomic) IBOutlet UIButton *addOneWitBtn;
- (IBAction)addOneWitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addTwoWitBtn;
- (IBAction)addTwoWitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addThreeWitBtn;
- (IBAction)addThreeWitBtnAction:(id)sender;



@end
