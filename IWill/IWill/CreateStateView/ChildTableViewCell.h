//
//  ChildTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 5/29/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface ChildTableViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UILabel * childNameLbl;
@property(nonatomic,weak)IBOutlet UILabel * childDobLbl;
@property(nonatomic,weak)IBOutlet UITextField * childNameTxtF;
@property(nonatomic,weak)IBOutlet UITextField * childDobTxtF;

@property(nonatomic,weak)IBOutlet JVFloatLabeledTextField * childNameTxtFFloat;
@property(nonatomic,weak)IBOutlet JVFloatLabeledTextField * childDobTxtFFloat;


@property(nonatomic,weak)IBOutlet UIButton * deleteBtn;
@property(nonatomic,weak)IBOutlet UIButton * selectChildBtn;
@property(nonatomic,weak)IBOutlet UIImageView * backImage;







@end
