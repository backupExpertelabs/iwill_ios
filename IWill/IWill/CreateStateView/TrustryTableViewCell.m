//
//  TrustryTableViewCell.m
//  IWill
//
//  Created by A1AUHAIG on 5/30/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "TrustryTableViewCell.h"

@implementation TrustryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _nameTextF.layer.cornerRadius = 3;
    _nameTextF.layer.borderWidth = 0.3;
    _nameTextF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _aditionalInfoTxtV.layer.cornerRadius = 3;
    _aditionalInfoTxtV.layer.borderWidth = 0.3;
    _aditionalInfoTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _contactInfoTxtV.layer.cornerRadius = 3;
   _contactInfoTxtV.layer.borderWidth = 0.3;
    _contactInfoTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _locationTextF.layer.cornerRadius = 3;
    _locationTextF.layer.borderWidth = 0.3;
    _locationTextF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _valueTextF.layer.cornerRadius = 3;
    _valueTextF.layer.borderWidth = 0.3;
    _valueTextF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _contactInfoTxtF.layer.cornerRadius = 3;
    _contactInfoTxtF.layer.borderWidth = 0.3;
    _contactInfoTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    _backImage.layer.cornerRadius = 3;
    _addIndividualItemsBtn.layer.cornerRadius =3;
    _addBanificiaryBtn.layer.cornerRadius =3;

     //_backView.layer.cornerRadius = 2;
     //  _backView.layer.borderWidth = 1;
     //_backView.layer.borderColor = [UIColor blackColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
