//
//  TrustryTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 5/30/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrustryTableViewCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel * name;
@property(nonatomic,weak) IBOutlet UILabel * aditionalInfo;
@property(nonatomic,weak) IBOutlet UILabel * contactInfo;
@property(nonatomic,weak) IBOutlet UITextField * nameTextF;
@property(nonatomic,weak) IBOutlet UITextView * aditionalInfoTxtV;
@property(nonatomic,weak) IBOutlet UITextView * contactInfoTxtV;
@property(nonatomic,weak) IBOutlet UIButton * deleteBtn;
@property(nonatomic,weak) IBOutlet UIView * backView;
@property(nonatomic,weak) IBOutlet UITextField * valueTextF;
@property(nonatomic,weak) IBOutlet UITextField * locationTextF;
@property(nonatomic,weak) IBOutlet UITextField * contactInfoTxtF;

@property(nonatomic,weak) IBOutlet UIButton * addBanificiaryBtn;
@property(nonatomic,weak) IBOutlet UIButton * addIndividualItemsBtn;

@property(nonatomic,weak)IBOutlet UIImageView * backImage;



@end
