//
//  MyWillPersonalItemsViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/31/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillPersonalItemsViewController.h"
#import "PersonalItemsTableViewCell.h"
#import "ConnectionManager.h"
#import "Constant.h"
#import "Utils.h"
#import "MyWillRecipientViewController.h"

@interface MyWillPersonalItemsViewController ()<ConnectionManager_Delegate,UITextFieldDelegate>

{
    // int numberOfPersonalItems;
    
    NSMutableArray * personalItemArray;
    
    ConnectionManager *connectionManager;
    NSString * pIId;
    UITextField * tempTxtF;
}
@end

@implementation MyWillPersonalItemsViewController

{
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //numberOfPersonalItems = 1;
    personalItemArray = [[NSMutableArray alloc] init];
    
    visiblePopTipViews = [[NSMutableArray alloc]init];
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    //totalAmountTxtF
    _totalAmountTxtF.layer.borderWidth = 0.5;
    _totalAmountTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    _totalAmountTxtF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _savePIBtn.layer.cornerRadius =3;
    
    
    
    [self registerForKeyboardNotifications];
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self getAllPI];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return personalItemArray.count;
    
    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    PersonalItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[PersonalItemsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:MyIdentifier];
    }
    
    
    NSDictionary * piDict = [personalItemArray objectAtIndex:indexPath.row];
    NSDictionary * piInfo = [piDict valueForKey:@"pi_info"];
    
    cell.nameTextF.text = [piInfo valueForKey:@"name"];
    cell.locationTxtV.text = [piInfo valueForKey:@"location"];
    cell.valueTxtV.text = [piInfo valueForKey:@"amount"];
    cell.addRecipientBtn.tag = indexPath.row;
    [cell.addRecipientBtn addTarget:self action:@selector(addRecipientTo:) forControlEvents:UIControlEventTouchUpInside];
    cell.deleteBtn .tag = [[piDict valueForKey:@"pi_id"] integerValue];
    [cell.deleteBtn addTarget:self action:@selector(deletePI:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(void)addRecipientTo:(UIButton*)sender
{
    [tempTxtF resignFirstResponder];
    
    NSIndexPath  *legalChildInde = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    
    PersonalItemsTableViewCell * selecCell = [_personalItemsTableView cellForRowAtIndexPath:legalChildInde];
    
    
    if(selecCell.nameTextF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter personal item Name and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    if(selecCell.valueTxtV.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter personal item value and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    if(selecCell.locationTxtV.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter personal item location abd save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    
    MyWillRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillRecipientViewController"];
    vc.benificiaryDataDict = [personalItemArray objectAtIndex:sender.tag];
    vc.benificiaryType = @"personalItems";
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addMorePersonalItemsBtnAction:(id)sender
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSString * trustyUrl = [NSString stringWithFormat:@"%@/%@",@"getPIJson",[userData valueForKey:@"id"]];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:trustyUrl];
    
    
}


////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"A person or firm appointed by you separate from the will that holds and administers property or assets for the benefit of a third party.";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        /* Some options to try.
         */
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        //popTipView.hasGradientBackground = NO;
        //popTipView.cornerRadius = 2.0;
        //popTipView.sidePadding = 30.0f;
        //popTipView.topMargin = 20.0f;
        //popTipView.pointerSize = 50.0f;
        //popTipView.hasShadow = NO;
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}




-(void)getAllPI
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    NSString * childUrl = [NSString stringWithFormat:@"%@/%@",@"getAllPI",[userData valueForKey:@"id"]];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:childUrl];
    
}



#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getPIC"])
            {
                [personalItemArray removeAllObjects];
                for (NSDictionary * ind in [[aResponceDic valueForKey:@"Result"] valueForKey:@"all_pi"]) {
                    [personalItemArray addObject:ind];
                }
                
                int totalAmount =0;
                
                for (NSDictionary * priceDect in personalItemArray) {
                    totalAmount = totalAmount+ [[[priceDect valueForKey:@"pi_info"] valueForKey:@"amount"] intValue];
                    
                   
                }
                _totalAmountTxtF.text = [NSString stringWithFormat:@"%d",totalAmount];
                
               // personalItemArray = [[aResponceDic valueForKey:@"Result"] valueForKey:@"all_pi"];
                
                [_personalItemsTableView reloadData];
            }
            
            else if ([[aResponceDic valueForKey:@"method"]isEqualToString:@"deletePIC"])
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                [self getAllPI];
            }
            
            else if([[aResponceDic valueForKey:@"method"]isEqualToString:@"getPIJsonC"])
            {
                [personalItemArray addObject:[aResponceDic valueForKey:@"Result"]];
                [_personalItemsTableView reloadData];
            }
            
            
            else if([[aResponceDic valueForKey:@"method"]isEqualToString:@"updateWholePIC"])
            {
                 [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            }
            
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(void)deletePI:(UIButton*)sender
{
    pIId = [NSString stringWithFormat:@"%d", sender.tag];
    
    [Utils showAlertViewWithTag:200 title:alertTitle message:trusteeDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        
    }
    
    
    if(alertView.tag == 200 && buttonIndex ==1)
    {
        [self deletePI];
    }
    
}




-(void)deletePI
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSString * trustyUrl = [NSString stringWithFormat:@"%@/%@/%@",@"deletePI",[userData valueForKey:@"id"],pIId];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:trustyUrl];
    
}

- (IBAction)savePIBtnAction:(id)sender
{
    
    NSMutableArray * piDataArr = [[NSMutableArray alloc] init];
    
    for (int i=0; i<personalItemArray.count;i++)
    {
    NSIndexPath  *legalChildInde = [NSIndexPath indexPathForRow:i inSection:0];
    
    PersonalItemsTableViewCell * selecCell = [_personalItemsTableView cellForRowAtIndexPath:legalChildInde];
    
    
    if(selecCell.nameTextF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter personal item Name and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    if(selecCell.valueTxtV.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter personal item value and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    if(selecCell.locationTxtV.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter personal item location abd save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
        //NSMutableDictionary * piInfo = [[NSMutableDictionary alloc] init];

        NSMutableDictionary * piInfo = [[NSMutableDictionary alloc] init];
        [piInfo setValue: [[personalItemArray objectAtIndex:i]valueForKey:@"pi_id"] forKey:@"pi_id"];
        [piInfo setValue:selecCell.nameTextF.text forKey:@"name"];
        [piInfo setValue:selecCell.valueTxtV.text forKey:@"amount"];
        [piInfo setValue:selecCell.locationTxtV.text forKey:@"location"];
        
        [piDataArr addObject:piInfo];
    }
    

    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:piDataArr forKey:@"pi_info"];
    [dataDict setValue:@"" forKey:@"total_amount"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateWholePI"];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.personalItemsTableView];
    NSIndexPath * indexPath = [self.personalItemsTableView indexPathForRowAtPoint:point];
   

    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    
    //[piDict valueForKey:@"pi_info"]
    
    dataDict = [[personalItemArray objectAtIndex:indexPath.row] mutableCopy];
    
    
    NSMutableDictionary * pidataDict = [[NSMutableDictionary alloc]init];
    pidataDict = [[dataDict valueForKey:@"pi_info"] mutableCopy];
    
    if(textField.tag ==100)
    {
        [pidataDict setValue:textField.text forKeyPath:@"name" ];
    }
    if(textField.tag ==200)
    {
        [pidataDict setValue:textField.text forKeyPath:@"amount"];

    }
    if(textField.tag ==300)
    {
        [pidataDict setValue:textField.text forKeyPath:@"location"];

    }
    [dataDict setValue:pidataDict forKey:@"pi_info"];
    [personalItemArray replaceObjectAtIndex:indexPath.row withObject:dataDict];
    
    int totalAmount =0;
    
    for (NSDictionary * priceDect in personalItemArray) {
        totalAmount = totalAmount+ [[[priceDect valueForKey:@"pi_info"] valueForKey:@"amount"] intValue];
        
        
    }
    _totalAmountTxtF.text = [NSString stringWithFormat:@"%d",totalAmount];
    
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tempTxtF = textField;
    
    if(textField.tag == 200)
    {
        [self addToolBarForDoneButton:textField];
    }
    
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
    
    
}




-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _personalItemsTableView.contentInset = contentInsets;
    _personalItemsTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _personalItemsTableView.contentInset = contentInsets;
    _personalItemsTableView.scrollIndicatorInsets = contentInsets;
}



-(void)addToolBarForDoneButton:(UITextField*)textF
{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    textF.inputAccessoryView = keyboardToolbar;
}

-(void)yourTextViewDoneButtonPressed
{
    [tempTxtF resignFirstResponder];
}



@end
