//
//  MyChildListViewController.h
//  IWill
//
//  Created by A1AUHAIG on 8/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyChildListViewController : UIViewController
@property(nonatomic,weak)IBOutlet UITableView * legalChildrenTableView;
- (IBAction)addChildBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;
@property(nonatomic,weak)IBOutlet UIButton * addBtn;

@end
