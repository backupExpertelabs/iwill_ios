//
//  MyWillFuneralRecomendationViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/29/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"
#import "JVFloatLabeledTextView.h"

@interface MyWillFuneralRecomendationViewController : UIViewController<CMPopTipViewDelegate>

@property(nonatomic,weak) IBOutlet UILabel * servicedbyLbl;
@property(nonatomic,weak) IBOutlet UILabel * arrangementsLbl;
@property(nonatomic,weak) IBOutlet UILabel * obituaryLbl;
@property(nonatomic,weak) IBOutlet UITextView * servicedByTextF;
@property(nonatomic,weak) IBOutlet UITextView * arrangementsTxtV;
@property(nonatomic,weak) IBOutlet UITextView * obituaryTxtV;

@property(nonatomic,weak) IBOutlet UIScrollView * mainScrollView;

- (IBAction)backBtnAction:(id)sender;
- (IBAction)servicedHintBtnAction:(id)sender;
- (IBAction)arrangementsHintBtnAction:(id)sender;
- (IBAction)obituaryHintBtnAction:(id)sender;

- (IBAction)saveBtnAction:(id)sender;
@property(nonatomic,weak) IBOutlet UIButton * saveBtn;



@property(nonatomic,weak) IBOutlet UIView * hintBackView;
@property(nonatomic,weak) IBOutlet UILabel * hintTextLbl;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *specialRequestTxtV;
- (IBAction)hintCloseBtnAction:(id)sender;

@end
