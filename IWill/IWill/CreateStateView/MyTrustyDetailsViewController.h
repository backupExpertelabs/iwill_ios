//
//  MyTrustyDetailsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 8/9/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTrustyDetailsViewController : UIViewController


@property(nonatomic,weak)IBOutlet UITextField * nameTxtF;
@property(nonatomic,weak)IBOutlet UITextView * contactInfoTxtF;
@property(nonatomic,weak)IBOutlet UITextView * additionalInfoTxtf;
@property(nonatomic,weak)IBOutlet UIButton * submitBtn;
- (IBAction)submitBtnAction:(id)sender;


@property(nonatomic,weak)IBOutlet UIScrollView * mainScrollView;

@property(nonatomic,strong)NSMutableDictionary * dataDict;

@end
