//
//  GaudianTableViewCell.m
//  IWill
//
//  Created by A1AUHAIG on 8/14/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "GaudianTableViewCell.h"

@implementation GaudianTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _backImage.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
