//
//  MyWillTrustyInformationViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/30/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillTrustyInformationViewController.h"
#import "TrustryTableViewCell.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"

@interface MyWillTrustyInformationViewController ()<ConnectionManager_Delegate,UITextFieldDelegate,UITextViewDelegate>




{
    NSMutableArray * trusteeList;
    //int numberOfRow;
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    
    ConnectionManager * connectionManager;
    
    NSMutableArray * trustrylistArray;
    
    
}

@end

@implementation MyWillTrustyInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    visiblePopTipViews = [[NSMutableArray alloc]init];
    trusteeList = [[NSMutableArray alloc]init];
    trustrylistArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary * trustyDict = [[NSMutableDictionary alloc]init];
    [trustyDict setValue:@"" forKey:@"name"];
    [trustyDict setValue:@"" forKey:@"contact_info"];
    [trustyDict setValue:@"" forKey:@"additional_info"];
    [trustrylistArray addObject:trustyDict];
    
    
//    numberOfRow = 1;
    _trusteeTableView.scrollEnabled = NO;
    
    
    
    _addMoreBtn.layer.cornerRadius = 3;
    _submitBtn.layer.cornerRadius = 3;
    
    
    
//    _trusteeTableView.layer.borderWidth = 1;
//    _trusteeTableView.layer.borderColor = [UIColor blackColor].CGColor;
    
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return trustrylistArray.count;
    
    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    TrustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[TrustryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:MyIdentifier];
    }
    
    if(tableView == _trusteeTableView)
    {
        
    }
    if(indexPath.row ==0)
    {
        cell.deleteBtn.hidden = YES;
    }
    
    
    NSMutableDictionary * trustDict = [trustrylistArray objectAtIndex:indexPath.row];
    
    cell.nameTextF.tag =1;
    cell.contactInfoTxtV.tag = 2;
    cell.aditionalInfoTxtV.tag = 3;
    
    cell.nameTextF.text = [trustDict valueForKey:@"name"];
    cell.aditionalInfoTxtV.text = [trustDict valueForKey:@"additional_info"];
    cell.contactInfoTxtV.text = [trustDict valueForKey:@"contact_info"];
    
    
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deleteTrustry:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button addTarget:self
               action:@selector(addMoreAdoptedChildren:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Add More" forState:UIControlStateNormal];
    button.frame = CGRectMake(80.0, 0.0, 160.0, 40.0);
    [footerView addSubview:button];
    return footerView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)deleteTrustry:(UIButton*)sender
{
    NSLog(@"sssss %ld",(long)sender.tag);
    
    
    [trustrylistArray removeObjectAtIndex:sender.tag];
    
    _mainViewHeightConstant.constant = 70 + (300*trustrylistArray.count-1);
    _tableViewHeightConstant.constant = (300*trustrylistArray.count-1);
    [_trusteeTableView reloadData];
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addMoreTrusteeBtnAction:(id)sender
{
    
    NSMutableDictionary * trustyDict = [[NSMutableDictionary alloc]init];
    [trustyDict setValue:@"" forKey:@"name"];
    [trustyDict setValue:@"" forKey:@"contact_info"];
    [trustyDict setValue:@"" forKey:@"additional_info"];
    [trustrylistArray addObject:trustyDict];
    
    _mainViewHeightConstant.constant = 70 + (300*trustrylistArray.count-1);
    _tableViewHeightConstant.constant = 300+(300*trustrylistArray.count-1);
    [_trusteeTableView reloadData];
    
    
    //_mainView.backgroundColor = [UIColor redColor];
    //_trusteeTableView.backgroundColor = [UIColor greenColor];
    
}



- (IBAction)yesBtnAction:(id)sender
{
    _mainView.hidden = NO;
    [_noBtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    [_yesBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];

    
}

- (IBAction)noBtnAction:(id)sender

{
    _mainView.hidden = YES;
    [_noBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    [_yesBtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
}



////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"A person or firm appointed by you separate from the will that holds and administers property or assets for the benefit of a third party.";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
       
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}
- (IBAction)submitBtnAction:(id)sender
{
    
    
    
    for (NSDictionary * ledalDict in trustrylistArray) {
        
        if([[ledalDict valueForKey:@"name"] length]==0)
        {
            [Utils showAlertView:@"Alert" message:@"Please enter name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return ;
        }
        
        if([[ledalDict valueForKey:@"contact_info"] length]==0)
        {
            [Utils showAlertView:@"Alert" message:@"Please enter contact info" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return ;
        }
        
        if([[ledalDict valueForKey:@"additional_info"] length]==0)
        {
            [Utils showAlertView:@"Alert" message:@"Please enter additional Info" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return ;
        }
        
        
    }
    
    
    
    
    
    
    
    [self callAPIForAddTrustry];
}




//http://localhost/iwill/index.php/addTrusty            Add the particular addTrusty     POST
//http://localhost/iwill/index.php/getTrusties/$testator_id       get All trusty          GET
//http://localhost/iwill/index.php/getTrusty/$testator_id/$trusty_id      get single trusty  info        GET
//http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id    delete a single trusty        GET
//http://localhost/iwill/index.php/updateTrusty          for update trusty         POST


//"testator_id": 1,
//"trusty_info": [
//                {
//                    "name": "trusty1",
//                    "contact_info": "123",
//                    "additional_info": "verma1"
//                },



-(void)callAPIForAddTrustry
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
     [dataDict setValue:trustrylistArray forKey:@"trusty_info"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addTrusty"];
    
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}

-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1)
        {
            
        [Utils showAlertViewWithTag:100 title:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}


//-(void)deleteWitnessAction:(UIButton*)sender
//{
//
//
//    NSDictionary* dataDict = [allExecuterArray objectAtIndex:sender.tag];
//    executerID = [dataDict valueForKey:@"executor_id"];
//    [Utils showAlertViewWithTag:200 title:alertTitle message:executerDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
//
//}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}



-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    
        
        CGPoint point = [textField.superview convertPoint:origin toView:_trusteeTableView];
        NSIndexPath * indexPath = [_trusteeTableView indexPathForRowAtPoint:point];
        
        
        NSMutableDictionary * trustDict = [trustrylistArray objectAtIndex:indexPath.row];
        
        if(textField.tag ==1 )
        {
            [trustDict setValue:textField.text forKey:@"name"];
            
            [trustrylistArray replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:trustDict];
        }
        else if(textField.tag ==2 )
        {
            [trustDict setValue:textField.text forKey:@"contact_info"];
            [trustrylistArray replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:trustDict];
        }
    
        else if(textField.tag ==3 )
        {
            [trustDict setValue:textField.text forKey:@"additional_info"];
            [trustrylistArray replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:trustDict];
        }
        
        
    }
    


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    
    //    if(textField.superview.superview.superview.superview.tag ==1)
    //    {
    //
    //
    //        CGPoint point = [textField.superview convertPoint:origin toView:_legalChildrenTableView];
    //        NSIndexPath * indexPath = [_legalChildrenTableView indexPathForRowAtPoint:point];
    //
    //
    //        NSMutableDictionary * legalChildDict = [legalChildArry objectAtIndex:indexPath.row];
    //
    //        if(textField.tag ==1 )
    //        {
    //            [legalChildDict setValue:textField.text forKey:@"child_name"];
    //
    //            [legalChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
    //        }
    //        else
    //        {
    //            [legalChildDict setValue:textField.text forKey:@"child_dob"];
    //            [legalChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
    //        }
    //
    //
    //    }
    //
    //    else{
    //        CGPoint point = [textField.superview convertPoint:origin toView:_adoptedChildrenTableView];
    //        NSIndexPath * indexPath = [_adoptedChildrenTableView indexPathForRowAtPoint:point];
    //
    //
    //        NSMutableDictionary * legalChildDict = [adoptedChildArry objectAtIndex:indexPath.row];
    //
    //        if(textField.tag ==1 )
    //        {
    //            [legalChildDict setValue:textField.text forKey:@"child_name"];
    //
    //            [adoptedChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
    //        }
    //        else
    //        {
    //            [legalChildDict setValue:textField.text forKey:@"child_dob"];
    //            [adoptedChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
    //        }
    //
    //    }
    //
    //[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    [textView resignFirstResponder];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    CGPoint origin = textView.frame.origin;
    
    
    CGPoint point = [textView.superview convertPoint:origin toView:_trusteeTableView];
    NSIndexPath * indexPath = [_trusteeTableView indexPathForRowAtPoint:point];
    
    
    NSMutableDictionary * trustDict = [trustrylistArray objectAtIndex:indexPath.row];
    
    
    if(textView.tag ==2 )
    {
        [trustDict setValue:textView.text forKey:@"contact_info"];
        [trustrylistArray replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:trustDict];
    }
    
    else if(textView.tag ==3 )
    {
        [trustDict setValue:textView.text forKey:@"additional_info"];
        [trustrylistArray replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:trustDict];
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
