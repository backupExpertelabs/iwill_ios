//
//  PersonalItemsTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 5/31/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalItemsTableViewCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel * name;
@property(nonatomic,weak) IBOutlet UILabel * value;
@property(nonatomic,weak) IBOutlet UILabel * location;
@property(nonatomic,weak) IBOutlet UITextField * nameTextF;
@property(nonatomic,weak) IBOutlet UITextField * valueTxtV;
@property(nonatomic,weak) IBOutlet UITextField * locationTxtV;
@property(nonatomic,weak) IBOutlet UIButton * deleteBtn;
@property(nonatomic,weak) IBOutlet UIButton * addRecipientBtn;
@property(nonatomic,weak) IBOutlet UIView * backView;

@property(nonatomic,weak) IBOutlet UIImageView *backImage;


@end
