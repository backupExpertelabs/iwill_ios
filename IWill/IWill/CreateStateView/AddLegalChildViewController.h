//
//  AddLegalChildViewController.h
//  IWill
//
//  Created by A1AUHAIG on 9/4/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddLegalChildViewController : UIViewController




@property(nonatomic,weak)IBOutlet UITableView * legalChildrenTableView;

- (IBAction)backBtnAction:(id)sender;

- (IBAction)saveBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UIButton * saveBtn;


///////////  legal children ////////

@property (weak, nonatomic) IBOutlet UIButton *legalChildAddMoreBtn;
- (IBAction)legalChildAddMoreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *numberOfLegalChildrenTxtf;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *legalChildrenViewHeightConstant;

@property (weak, nonatomic) IBOutlet UIView *legalChildrenView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstant;



 //////////////////// Date Picker ///////////////////

@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;



@end
