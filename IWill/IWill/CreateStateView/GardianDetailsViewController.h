//
//  GardianDetailsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 8/14/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GardianDetailsViewController : UIViewController


- (IBAction)backBtnAction:(id)sender;
- (IBAction)saveGaurdianBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UITableView * minorChildTableView;

@property(nonatomic,retain)IBOutlet UITextField * nameTextF;
@property(nonatomic,retain)IBOutlet UITextField * dobTextF;
@property(nonatomic,retain)IBOutlet UITextField * relationShipTextF;
@property(nonatomic,retain)IBOutlet UITextField * commentTextf;
@property(nonatomic,retain)IBOutlet UIButton * saveBtn;
@property(nonatomic,retain)IBOutlet UIScrollView * mainScrollView;

@property(nonatomic,retain) NSMutableDictionary * dataDict;
@property(nonatomic,retain) NSString * isNewGaurdianAdded;
@property(nonatomic,retain) NSMutableArray * selectedChildArray;
- (IBAction)deleteBtnAction:(id)sender;
@property(nonatomic,retain)IBOutlet UIButton * deleteBtn;


///////////  Date Picker /////////

@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;



@end
