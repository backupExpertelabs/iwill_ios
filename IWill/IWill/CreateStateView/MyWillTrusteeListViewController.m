//
//  MyWillTrusteeListViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillTrusteeListViewController.h"
#import "MyWillTrusteeListTableViewCell.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "MyTrustyDetailsViewController.h"

@interface MyWillTrusteeListViewController ()<UITableViewDataSource,UITableViewDelegate,ConnectionManager_Delegate>
{
    int numberOfRow ;
    
    ConnectionManager * connectionManager;
    NSMutableArray * trustyArrayList;
    NSString * trusrtyId;
    
    
}

@end

@implementation MyWillTrusteeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    trustyArrayList = [[NSMutableArray alloc]init];
    
    
    
    _saveBtn.layer.cornerRadius =3;
    _headerView.layer.cornerRadius =3;
    //[self callAPIForAllTrusty];
    
    //numberOfRow = 5;
    // Do any additional setup after loading the view.
}



-(void)viewWillAppear:(BOOL)animated
{
    [self callAPIForAllTrusty];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return trustyArrayList.count;
    
    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    MyWillTrusteeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[MyWillTrusteeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                     reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary * trustyDic  =  [trustyArrayList objectAtIndex:indexPath.row];
    
    cell.name.text = [trustyDic valueForKey:@"name"];
    cell.contactInfo.text = [trustyDic valueForKey:@"contact_info"];
    cell.aditionalInfo.text = [trustyDic valueForKey:@"additional_info"];
    cell.deleteBtn.tag =[[trustyDic valueForKey:@"trusty_id"] intValue];
    [cell.deleteBtn addTarget:self action:@selector(deleteTrustyAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTrustyDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyTrustyDetailsViewController"];
    NSMutableDictionary * trustyDic  =  [trustyArrayList objectAtIndex:indexPath.row];
    vc.dataDict = trustyDic;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addMoreTrusteeBtnAction:(id)sender
{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillTrustyInformationViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)callAPIForAllTrusty
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    //    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    //    NSLog(@"hdsghj %@",dataDict);
    
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@",@"getTrusties",[userData valueForKey:@"id"]];
    
http://localhost/iwill/index.php/getTrusties/$testator_id
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:urlStr];
    
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}


-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getTrustiesC"])
            {
                trustyArrayList = [[NSMutableArray alloc] init];
                trustyArrayList   = [aResponceDic valueForKey:@"Result"];
                [_trusteeTableView reloadData];
            }
            else if ([[aResponceDic valueForKey:@"method"] isEqualToString:@"deleteTrustyC"])
            {
                [self callAPIForAllTrusty];
            }
            
        }
        
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            // [trustyArrayList removeAllObjects];
            
            if(![[aResponceDic valueForKey:@"method"] isEqualToString:@"deleteTrustyC"])
            
            trustyArrayList   = [aResponceDic valueForKey:@"Result"];
            [_trusteeTableView reloadData];
            
        }
        
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        
    });
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        
    }
    
    
    if(alertView.tag == 200 && buttonIndex ==1)
    {
        [self deleteTrustyAPI];
    }
    
}

-(void)deleteTrustyAction:(UIButton*)sender
{
    
    
    trusrtyId = [NSString stringWithFormat:@"%ld", (long)sender.tag];
    
    
    //    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    //
    //    NSString * witnessCount = [userData valueForKey:@"countWitnesses"];
    //
    //    if(witnessCount.intValue < 3 )
    //    {
    //
    //    }
    //
    //
    //    else{
    //    NSDictionary* dataDict = [allExecuterArray objectAtIndex:sender.tag];
    //    executerID = [dataDict valueForKey:@"executor_id"];
    [Utils showAlertViewWithTag:200 title:alertTitle message:trusteeDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
    //}
}


-(void)deleteTrustyAPI
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    //    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"executor_id"];
    //
    //    NSLog(@"hdsghj %@",dataDict);
    
    
    //trusrtyId
    NSString * trustyUrl = [NSString stringWithFormat:@"%@/%@/%@",@"deleteTrusty",[userData valueForKey:@"id"],trusrtyId];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:trustyUrl];
    
}


@end
