//
//  MyWillTrustyInformationViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/30/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"
@interface MyWillTrustyInformationViewController : UIViewController<CMPopTipViewDelegate>


@property(nonatomic,weak)IBOutlet UITableView * trusteeTableView;



- (IBAction)backBtnAction:(id)sender;
- (IBAction)addMoreTrusteeBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstant;

- (IBAction)yesBtnAction:(id)sender;
- (IBAction)noBtnAction:(id)sender;
@property(nonatomic,weak)IBOutlet UIButton * yesBtn;
@property(nonatomic,weak)IBOutlet UIButton * noBtn;
@property(nonatomic,weak)IBOutlet UIButton * addMoreBtn;
@property(nonatomic,weak)IBOutlet UIButton * submitBtn;
- (IBAction)submitBtnAction:(id)sender;



@property(nonatomic,weak)IBOutlet UIView * mainView;

@end
