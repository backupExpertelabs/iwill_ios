//
//  MyWillChildrenInformationViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/29/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWillChildrenInformationViewController : UIViewController

@property(nonatomic,weak)IBOutlet UITableView * legalChildrenTableView;
@property(nonatomic,weak)IBOutlet UITableView * adoptedChildrenTableView;

- (IBAction)backBtnAction:(id)sender;

- (IBAction)saveBtnAction:(id)sender;



///////////  legal children ////////

@property (weak, nonatomic) IBOutlet UIButton *legalChildAddMoreBtn;
- (IBAction)legalChildAddMoreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *numberOfLegalChildrenTxtf;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *legalChildrenViewHeightConstant;

@property (weak, nonatomic) IBOutlet UIView *legalChildrenView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstant;


///////////  Adopted children ////////////

@property (weak, nonatomic) IBOutlet UIButton *adoptedChildAddMoreBtn;
- (IBAction)adoptedChildAddMoreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *numberOfadoptedChildrenTxtf;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adoptedChildrenViewHeightConstant;
@property (weak, nonatomic) IBOutlet UIView *adoptedChildrenView;





@end
