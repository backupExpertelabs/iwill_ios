//
//  AddNewChildViewController.h
//  IWill
//
//  Created by A1AUHAIG on 9/18/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaterialTabs.h"

@interface AddNewChildViewController : UIViewController

@property(weak, nonatomic) IBOutlet UIButton *legalChildBtn;
@property(weak, nonatomic) IBOutlet UIButton *adoptedChildBtn;
@property(weak, nonatomic) IBOutlet UILabel *buttonHilightLbl;

-(IBAction)legalBtnAction:(id)sender;
-(IBAction)adoptedBtnAction:(id)sender;



/////////////


@property(nonatomic,weak)IBOutlet UITableView * legalChildrenTableView;
@property(nonatomic,weak)IBOutlet UITableView * adoptedChildrenTableView;
- (IBAction)backBtnAction:(id)sender;
- (IBAction)saveLegalBtnAction:(id)sender;
- (IBAction)saveAdoptedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveLegalBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveAdoptedBtn;
///////////  legal children ////////

@property (weak, nonatomic) IBOutlet UIButton *legalChildAddMoreBtn;
- (IBAction)legalChildAddMoreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *numberOfLegalChildrenTxtf;


///////////  Adopted children ////////////

@property (weak, nonatomic) IBOutlet UIButton *adoptedChildAddMoreBtn;
- (IBAction)adoptedChildAddMoreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *numberOfadoptedChildrenTxtf;

 //////  Date Picker //////


@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;

//////////////

@property (weak, nonatomic) IBOutlet UIView *legalChildView;
@property (weak, nonatomic) IBOutlet UIView *adoptedChildView;


@end
