//
//  PersonalItemsTableViewCell.m
//  IWill
//
//  Created by A1AUHAIG on 5/31/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "PersonalItemsTableViewCell.h"

@implementation PersonalItemsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _nameTextF.layer.cornerRadius = 2;
    _nameTextF.layer.borderWidth = 0.5;
    _nameTextF.layer.borderColor = [UIColor whiteColor].CGColor;
    _nameTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    _locationTxtV.layer.cornerRadius = 2;
    _locationTxtV.layer.borderWidth = 0.5;
    _locationTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    _locationTxtV.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    _valueTxtV.layer.cornerRadius = 2;
    _valueTxtV.layer.borderWidth = 0.5;
    _valueTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    _valueTxtV.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

//    _backView.layer.cornerRadius = 2;
//    _backView.layer.borderWidth = 0.5;
//    _backView.layer.borderColor = [UIColor blackColor].CGColor;
//
    _backImage.layer.cornerRadius = 3;
    _addRecipientBtn.layer.cornerRadius = 3;

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
