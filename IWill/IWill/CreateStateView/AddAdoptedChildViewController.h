//
//  AddAdoptedChildViewController.h
//  IWill
//
//  Created by A1AUHAIG on 9/5/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAdoptedChildViewController : UIViewController
@property(nonatomic,weak)IBOutlet UITableView * adoptedChildrenTableView;

- (IBAction)backBtnAction:(id)sender;

- (IBAction)saveBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UIButton * saveBtn;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstant;


///////////  Adopted children ////////////

@property (weak, nonatomic) IBOutlet UIButton *adoptedChildAddMoreBtn;
- (IBAction)adoptedChildAddMoreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *numberOfadoptedChildrenTxtf;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adoptedChildrenViewHeightConstant;
@property (weak, nonatomic) IBOutlet UIView *adoptedChildrenView;


//////////////////// Date Picker ///////////////////

@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;






@end
