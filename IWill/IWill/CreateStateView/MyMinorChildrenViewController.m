//
//  MyMinorChildrenViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyMinorChildrenViewController.h"
#import "MyWillTrusteeListTableViewCell.h"
#import "Utils.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
#import "ConnectionManager.h"
#import "Constant.h"
#import "CNPPopupController.h"
#import "GardianDetailsViewController.h"
#import "GaudianTableViewCell.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MyMinorChildrenViewController ()<UITableViewDataSource,UITableViewDelegate,ConnectionManager_Delegate,CNPPopupControllerDelegate,UITextFieldDelegate,UITextViewDelegate>

{
    NSMutableArray * minorChildList;
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    
    ConnectionManager* connectionManager;
    NSMutableArray *adoptedChildArray;
    NSMutableArray * legalChildArray;
    NSMutableArray * selectedChildArray;
    
    NSMutableArray * gaurdianList;
    NSString * gaurdianId;
    UIButton * tempBtn;
    UITextField * activeField;
    
}

@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation MyMinorChildrenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    gaurdianId = @"";
    _gaurdianView.hidden = YES;
    selectedChildArray = [[[NSMutableArray alloc] init] mutableCopy];
    gaurdianList = [[NSMutableArray alloc]init];
    connectionManager = [[ConnectionManager alloc]init];
    legalChildArray = [[NSMutableArray alloc]init];
    adoptedChildArray = [[NSMutableArray alloc]init];
    
    visiblePopTipViews = [[NSMutableArray alloc] init];
    minorChildList  = [[NSMutableArray alloc]init];
    
    _nameTextF.placeholder = @"Full Name";
    _dobTextF.placeholder = @"Date Of Birth";
    _relationShipTextF.placeholder = @"Relationship";
    _commentTextf.placeholder = @"Comment";
    
    _minorChildTableView.scrollEnabled = YES;
    _tableViewHeightConstant.constant = 50*minorChildList.count;
    
    _gaurdianTableView.tag = 100;
    _minorChildTableView.tag =200;
    
    _gaurdianTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //    _addGaurdianView.layer.borderWidth =1;
    //    _addGaurdianView.layer.borderColor = [UIColor blackColor].CGColor;
    //
    //
    //    _minorChildTableView.layer.borderWidth =1;
    //    _minorChildTableView.layer.borderColor = [UIColor blackColor].CGColor;
    
    
    
    _existingGaurdianBtn.layer.cornerRadius = 4;
    _saveGaurdianBtn.layer.cornerRadius = 4;
    _gaurdianInnerViewView.layer.cornerRadius = 4;
    
    
    
    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self getAllChieldInfo];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if(tableView.tag==200)
    {
        return minorChildList.count;
        
    }
    else{
        return gaurdianList.count;
        
    }
    
    //return minorChildList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    if(tableView.tag==200)
    {
        
        
        MyWillTrusteeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil)
        {
            cell = [[MyWillTrusteeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                         reuseIdentifier:MyIdentifier];
        }
        
        
        
        NSDictionary * childDict = [minorChildList objectAtIndex:indexPath.row];
        
        cell.childName.text = [childDict valueForKey:@"child_name"];
        cell.childDOB.text = [childDict valueForKey:@"child_dob"];
        
        if([[childDict valueForKey:@"guardian"] count]>0)
        {
        if([[childDict valueForKey:@"guardian"] allKeys].count>0)
        {
            cell.childView.tag = indexPath.row;
            [cell.childView addTarget:self action:@selector(viewChild:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.selectChild setHidden:YES];
            [cell.childView setHidden:NO];
        }
        }
        
        else{
            //        cell.selectChild.tag = [[childDict valueForKey:@"id"] integerValue];
            //        [cell.selectChild addTarget:self action:@selector(selectChild:) forControlEvents:UIControlEventTouchUpInside];
            [cell.selectChild setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
            [cell.selectChild setHidden:NO];
            [cell.childView setHidden:YES];
            
            
        }
        
        //    cell.selectChild.tag = [[childDict valueForKey:@"id"] integerValue];
        //    [cell.selectChild addTarget:self action:@selector(selectChild:) forControlEvents:UIControlEventTouchUpInside];
        //
        //
        //
        //
        //
        //    cell.childView.tag = [[childDict valueForKey:@"id"] integerValue];
        //    [cell.childView addTarget:self action:@selector(viewChild:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    else{
        
        GaudianTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil)
        {
            cell = [[GaudianTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:MyIdentifier];
        }
        
        NSDictionary * gaudianDict = [gaurdianList objectAtIndex:indexPath.row];
        cell.selectBtn.tag = indexPath.row;
        
        [cell.selectBtn setAccessibilityIdentifier:[gaudianDict valueForKey:@"guradian_id"]];
        //[cell.selectBtn addTarget:self action:@selector(selectGurdian:) forControlEvents:UIControlEventTouchUpInside];
        cell.name.text = [gaudianDict valueForKey:@"name"];
        cell.contactInfo.text = [gaudianDict valueForKey:@"relation"];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(tableView.tag ==200)
    {
        
        
        NSDictionary * childDict = [minorChildList objectAtIndex:indexPath.row];
        
        if([[childDict valueForKey:@"guardian"] count]>0)
        {
            NSDictionary * childDict1 = [childDict valueForKey:@"guardian"];
            GardianDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GardianDetailsViewController"];
            vc.dataDict = (NSMutableDictionary*)childDict1;
            vc.isNewGaurdianAdded = @"No";
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
        
        NSMutableDictionary * childInfoDict = [[NSMutableDictionary alloc]init];
        [childInfoDict setValue:[childDict valueForKey:@"id"] forKey:@"id"];
        [selectedChildArray removeObject:childInfoDict];
        
        [selectedChildArray addObject:childInfoDict];
        
        MyWillTrusteeListTableViewCell *cell = [_minorChildTableView cellForRowAtIndexPath:indexPath];
        
        
        if(cell.selectChild.selected)
        {
            [cell.selectChild setSelected:NO];
           // [cell.selectChild setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            [cell.selectChild setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            [selectedChildArray removeObject:childInfoDict];
        }
        else
        {
            
            [cell.selectChild setSelected:YES];
            [cell.selectChild setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
           // [cell.selectChild setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            
        }
        
        
        [_minorChildTableView reloadData];
        }
        
    }
    else{
        
        
        GaudianTableViewCell *cell = [_gaurdianTableView cellForRowAtIndexPath:indexPath];
        
        NSDictionary * gardianDict = [gaurdianList objectAtIndex:indexPath.row];
        gaurdianId  = [gardianDict valueForKey:@"guradian_id"];
        [tempBtn setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        tempBtn = cell.selectBtn;
        [tempBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        
        
    }
    
}




- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)addMoreTrusteeBtnAction:(id)sender
{
    
    
    
}









////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        contentMessage = @"The person who would take care of any younger children of testator is absent";
        
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}


-(void)getAllChieldInfo
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    
    
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@",@"getMinorChilds",[userData valueForKey:@"id"]];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getMinorChildsC"])
            {
                
                NSDictionary * dataDict = [aResponceDic valueForKey:@"Result"];
                
                minorChildList = [dataDict valueForKey:@"minor_childs"];
                gaurdianList = [dataDict valueForKey:@"guardians"];
                [_minorChildTableView reloadData];
            }
            
            else{
                [self.popupController dismissPopupControllerAnimated:YES];
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                selectedChildArray = [[NSMutableArray alloc] init];
                
                gaurdianId = @"";
                tempBtn = nil;
                _gaurdianView.hidden = YES;
                [self getAllChieldInfo];
                
            }
        }
        
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}

-(void)selectChild:(UIButton*)sender
{
    
}
-(void)viewChild:(UIButton*)sender
{
    NSDictionary * childDict = [[minorChildList objectAtIndex:sender.tag] valueForKey:@"guardian"];
    GardianDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GardianDetailsViewController"];
    vc.dataDict = (NSMutableDictionary*)childDict;
    vc.isNewGaurdianAdded = @"No";

    [self.navigationController pushViewController:vc animated:YES];
}


- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:87/255.f green:47/255.f blue:139/255.f alpha:1.0];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        //[self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        
        if([self checkGaurdianFieldValidation])
        {
            
            [self addGaurdianOnServer];
            
        }
    };
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
    customView.backgroundColor = [UIColor clearColor];
    
    _nameTextF = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    _nameTextF.delegate = self;
    _nameTextF.layer.borderWidth = 0.5;
    _nameTextF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _nameTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [customView addSubview:_nameTextF];
    
    _dobTextF = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 50)];
    _dobTextF.delegate = self;

    _dobTextF.layer.borderWidth = 0.5;
    _dobTextF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _dobTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [customView addSubview:_dobTextF];
    
    _relationShipTextF = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(10, 120, self.view.frame.size.width-20, 50)];
    _relationShipTextF.layer.borderWidth = 0.5;
    _relationShipTextF.delegate = self;

    _relationShipTextF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _relationShipTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [customView addSubview:_relationShipTextF];
    
    _commentTextf = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(10, 180, self.view.frame.size.width-20, 50)];
    _commentTextf.layer.borderWidth = 0.5;
    _commentTextf.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _commentTextf.delegate = self;

    _commentTextf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [customView addSubview:_commentTextf];
    
    _nameTextF.placeholder = @"Full Name";
    _dobTextF.placeholder = @"Date Of Birth";
    _relationShipTextF.placeholder = @"Relationship";
    _commentTextf.placeholder = @"Comment";
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[customView, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}




#pragma mark - CNPPopupController Delegate

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
}

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
}

#pragma mark - event response

-(void)showPopupCentered:(id)sender {
    [self showPopupWithStyle:CNPPopupStyleCentered];
}
- (IBAction)saveGaurdianBtnAction:(id)sender
{
    
    
    _gaurdianView.hidden = YES;
    
    [self assignGaurdianToChaild];
    //[self addGaurdianOnServer];
    
    
    
    
    //    if(selectedChildArray.count==0)
    //    {
    //        [Utils showAlertView:alertTitle message:@"Please Select At Least One Child" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    //        return;
    //
    //    }
    //    else{
    //        [self showPopupWithStyle:CNPPopupStyleCentered];
    //
    //    }
    
    
    //[self showPopupWithStyle:CNPPopupStyleCentered];
    
}

-(BOOL)checkGaurdianFieldValidation
{
    
    if(_nameTextF.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Gaurdian Name" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_dobTextF.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Gaurdian DOB" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_relationShipTextF.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Gaurdian Relationship" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_commentTextf.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Some Comment" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    return YES;
}

-(void)addGaurdianOnServer
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSString * urlStr = [NSString stringWithFormat:@"%@",@"assignGuardian"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:gaurdianId forKey:@"guardian_id"];
    
    NSMutableDictionary * gardianInfoDict = [[NSMutableDictionary alloc]init];
    [gardianInfoDict setValue:_nameTextF.text forKey:@"name"];
    [gardianInfoDict setValue:_relationShipTextF.text forKey:@"relation"];
    [gardianInfoDict setValue:_commentTextf.text forKey:@"contact_info"];
    
    [dataDict setValue:gardianInfoDict forKey:@"guardian_info"];
    [dataDict setValue:selectedChildArray forKey:@"child_info"];
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:urlStr];
    
}


-(void)assignGaurdianToChaild
{
    
    
    if(selectedChildArray.count==0)
    {
        [Utils showAlertView:alertTitle message:@"Please Select At Least One Child" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return;
        
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Add Gaurdian" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add New Gaudian" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
       // [self showPopupWithStyle:CNPPopupStyleCentered];
        
        
        GardianDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GardianDetailsViewController"];
        vc.isNewGaurdianAdded = @"Yes";
        vc.selectedChildArray = selectedChildArray;
        [self.navigationController pushViewController:vc animated:YES];
        
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Existing" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _gaurdianView.hidden = NO;
        [_gaurdianTableView reloadData];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (IBAction)existingGaurdianBtnAction:(id)sender
{
    
    if(gaurdianId.length ==0)
    {
        [Utils showAlertView:alertTitle message:@"Please Select At Least One Gaurdian" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return;
    }
    
    
    [self addGaurdianOnServer];
}


- (IBAction)hideGuadianView:(id)sender

{
    
    gaurdianId = @"";
    tempBtn = nil;
    _gaurdianView.hidden = YES;
    
}

-(void)selectGurdian:(UIButton*)sender
{
    [tempBtn setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    tempBtn = sender;
    [tempBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == _dobTextF)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    return YES;
}

- (IBAction)dateSelectedBtnAction:(id)sender {
    
    _dobTextF.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


@end
