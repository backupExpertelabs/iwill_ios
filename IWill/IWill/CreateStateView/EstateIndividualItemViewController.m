//
//  EstateIndividualItemViewController.m
//  IWill
//
//  Created by A1AUHAIG on 9/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "EstateIndividualItemViewController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "Constant.h"
#import "TrustryTableViewCell.h"
#import "MyWillRecipientViewController.h"

@interface EstateIndividualItemViewController ()<UITextFieldDelegate,UITextViewDelegate,ConnectionManager_Delegate>

{
    
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    
    UITextField * tempTxtF;
    ConnectionManager * connectionManager;
    NSMutableArray * individualItemsArray;
}
@end

@implementation EstateIndividualItemViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerForKeyboardNotifications];
    
    
    
    individualItemsArray = [[NSMutableArray alloc] init];
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    _saveBtn.layer.cornerRadius =3;
    
    
    for (NSMutableDictionary * dict in [_individualItemDict valueForKey:@"individual_items"])
    {
        [individualItemsArray addObject:dict];
    }
    
    if(individualItemsArray.count==0)
    {
        [self adMoreItemsBtnAction:nil];
    }
    
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backBtnAction:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
    
}





//-(IBAction)addStateItems:(id)sender
//{
//
//
//    NSMutableArray * users_info = [[NSMutableArray alloc] init];
//    if (![Utils isInternetAvailable])
//    {
//        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        return;
//    }
//    [Utils startActivityIndicatorInView:self.view withMessage:@""];
//
//    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
//    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
//    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
//    [dataDict setValue:[estateDict valueForKey:@"estate_id"] forKey:@"estate_id"];
//
//    NSLog(@"hdsghj %@",dataDict);
//    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addStateItem"];
//}
//
//
////    numberOfItems = numberOfItems+1;
////    [_myEstateTableView reloadData];


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return individualItemsArray.count;
    
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    TrustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[TrustryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    
    NSDictionary * stateItemDict = [individualItemsArray objectAtIndex:indexPath.row];
    
    cell.nameTextF.text = [stateItemDict valueForKey:@"name"];
    cell.valueTextF.text = [stateItemDict valueForKey:@"value"];
    cell.locationTextF.text = [stateItemDict valueForKey:@"locality"];
    
    cell.deleteBtn.tag = [[stateItemDict valueForKey:@"individual_items_id"] intValue];
    [cell.deleteBtn addTarget:self action:@selector(deleteEstateItems:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.addBanificiaryBtn.tag = indexPath.row;
    [cell.addBanificiaryBtn addTarget:self action:@selector(addBanificiaryOnIndividualItems:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)deleteEstateItems:(UIButton*)sender
{
    NSString * individual_id = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    //NSString * individual_id =   [individualItemsArray valueForKey:@"estate_items_id"] ;
    
    NSString * url = [NSString stringWithFormat:@"%@/%@/%@/%@",@"deleteIndividualItem",[userData valueForKey:@"id"],[_individualItemDict valueForKey:@"estate_items_id"],individual_id];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:url];
    
}



////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"A person or firm appointed by you separate from the will that holds and administers property or assets for the benefit of a third party.";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}



-(void)getAllState
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    NSString * childUrl = [NSString stringWithFormat:@"%@/%@",@"getEstate",[userData valueForKey:@"id"]];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:childUrl];
    
}



#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"addIndividualItemC"])
            {
                [individualItemsArray addObject:[aResponceDic valueForKey:@"Result"]];
                
                [_individualItemsTableView reloadData];
                
            }
            
            else if ([[aResponceDic valueForKey:@"method"] isEqualToString:@"updateIndividual_itemsC"])
            {
                
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            }
            
            
            else if ([[aResponceDic valueForKey:@"method"] isEqualToString:@"deleteIndividualItemC"])
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            }
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



- (IBAction)saveBtnAction:(id)sender
{
    
    
    NSMutableArray * individual_items_info = [[NSMutableArray alloc] init];
    
    for (int i=0; i<individualItemsArray.count;i++)
    {
        
        
        NSDictionary * individualItemDict = [individualItemsArray objectAtIndex:i];
        
        
        
        if([[individualItemDict valueForKey:@"name"] length] ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter individual item name and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        
        
        if([[individualItemDict valueForKey:@"name"] length] ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter individual item value save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        if([[individualItemDict valueForKey:@"locality"] length] ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter individual item location and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        NSMutableDictionary * individualItemDict1 = [[NSMutableDictionary alloc] init];
        
        [individualItemDict1 setValue:[individualItemDict valueForKey:@"individual_items_id"] forKey:@"individual_items_id"];
        [individualItemDict1 setValue:[individualItemDict valueForKey:@"name"] forKey:@"name"];
        [individualItemDict1 setValue:[individualItemDict valueForKey:@"value"]  forKey:@"value"];
        [individualItemDict1 setValue:[individualItemDict valueForKey:@"locality"] forKey:@"locality"];
        [individual_items_info addObject:individualItemDict1];
        
    }
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    [dataDict setValue:_estateItemId forKey:@"estate_item_id"];
    [dataDict setValue:individual_items_info forKey:@"individual_items_info"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateIndividual_items"];
    
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _individualItemsTableView.contentInset = contentInsets;
    _individualItemsTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _individualItemsTableView.contentInset = contentInsets;
    _individualItemsTableView.scrollIndicatorInsets = contentInsets;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.individualItemsTableView];
    NSIndexPath * indexPath = [self.individualItemsTableView indexPathForRowAtPoint:point];
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    
    //[piDict valueForKey:@"pi_info"]
    
    dataDict = [[individualItemsArray objectAtIndex:indexPath.row] mutableCopy];
    
    
    if(textField.tag ==100)
    {
        [dataDict setValue:textField.text forKeyPath:@"name" ];
    }
    if(textField.tag ==200)
    {
        [dataDict setValue:textField.text forKeyPath:@"value"];
        
    }
    if(textField.tag ==300)
    {
        [dataDict setValue:textField.text forKeyPath:@"locality"];
        
    }
    
    [individualItemsArray replaceObjectAtIndex:indexPath.row withObject:dataDict];
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tempTxtF = textField;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
    
    
}

-(void)addBanificiaryOnIndividualItems:(UIButton*)sender
{
    MyWillRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillRecipientViewController"];
    vc.benificiaryType = @"individual_items";
    vc.benificiaryDataDict = [individualItemsArray objectAtIndex:sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}



-(IBAction)adMoreItemsBtnAction:(id)sender
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    [dataDict setValue:[_individualItemDict valueForKey:@"estate_items_id"] forKey:@"estate_item_id"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addIndividualItem"];
}




@end
