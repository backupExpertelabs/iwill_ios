//
//  MyWillTrusteeListViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWillTrusteeListViewController : UIViewController

@property(nonatomic,weak)IBOutlet UITableView * trusteeTableView;

- (IBAction)backBtnAction:(id)sender;
- (IBAction)addMoreTrusteeBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UIButton * saveBtn;
@property(nonatomic,weak)IBOutlet UIView * headerView;



@end
