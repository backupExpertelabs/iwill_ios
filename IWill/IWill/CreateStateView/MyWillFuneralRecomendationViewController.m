//
//  MyWillFuneralRecomendationViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/29/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillFuneralRecomendationViewController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "Constant.h"

@interface MyWillFuneralRecomendationViewController ()<UITextViewDelegate,UITextFieldDelegate,ConnectionManager_Delegate>


{
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    NSString *contentMessage ;
    
    ConnectionManager * connectionManager;
    
    
}
@end

@implementation MyWillFuneralRecomendationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    
    visiblePopTipViews = [[NSMutableArray alloc] init];
    _servicedByTextF.layer.cornerRadius = 3;
    _servicedByTextF.layer.borderWidth = 0.5;
    _servicedByTextF.layer.borderColor = [UIColor whiteColor].CGColor;
    _servicedByTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _arrangementsTxtV.layer.cornerRadius = 3;
    _arrangementsTxtV.layer.borderWidth = 0.5;
    _arrangementsTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    _arrangementsTxtV.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _obituaryTxtV.layer.cornerRadius = 3;
    _obituaryTxtV.layer.borderWidth = 0.5;
    _obituaryTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    _obituaryTxtV.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _specialRequestTxtV.layer.cornerRadius = 3;
    _specialRequestTxtV.layer.borderWidth = 0.5;
    _specialRequestTxtV.layer.borderColor = [UIColor whiteColor].CGColor;
    _specialRequestTxtV.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    [self registerForKeyboardNotifications];
    [self getAllFuneralInfo];
    
}
-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)hintCloseBtnAction:(id)sender
{
    _hintBackView.hidden = YES;
}

- (IBAction)servicedHintBtnAction:(id)sender
{
//    _hintBackView.hidden = NO;
    
    contentMessage = nil;
    contentMessage = @"Name and location of funeral home.";
    [self hintBtnAction:sender];
    
}
- (IBAction)arrangementsHintBtnAction:(id)sender
{
    //_hintBackView.hidden = NO;
    
    contentMessage = nil;
    contentMessage = @"Songs, colors, who will speak, people you don't want there, cremation etc.";
     [self hintBtnAction:sender];
}
- (IBAction)obituaryHintBtnAction:(id)sender
{
    //_hintBackView.hidden = NO;
    
   contentMessage = nil;
    contentMessage = @"Obituary wording if any.";
     [self hintBtnAction:sender];
}
- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        
        CMPopTipView *popTipView;
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}


-(void)getAllFuneralInfo
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSString * requestURl = [NSString stringWithFormat:@"%@/%@",@"getFuneralInfo",[userData valueForKey:@"id"]];
    
    NSLog(@"hdsghj %@",requestURl);
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:requestURl];
}




- (IBAction)saveBtnAction:(id)sender
{
    [self callAPIForUpdateFuneral];
}


-(void)callAPIForUpdateFuneral
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    
    NSMutableDictionary * funeral_infoDict = [[NSMutableDictionary alloc]init];

    [funeral_infoDict setValue:_servicedByTextF.text forKey:@"service"];
    [funeral_infoDict setValue:_arrangementsTxtV.text forKey:@"arrangement"];
    [funeral_infoDict setValue:_obituaryTxtV.text forKey:@"obituary_words"];
    [funeral_infoDict setValue:_specialRequestTxtV.text forKey:@"special_request"];
    [funeral_infoDict setValue:@"" forKey:@"comments"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:funeral_infoDict forKey:@"funeral_info"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"setFuneralInfo"];
    
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}

-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1)
        {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getFuneralInfoC"])
            {
                NSDictionary *dataDict = [aResponceDic valueForKey:@"Result"];
           
                _servicedByTextF.text = [dataDict valueForKey:@"service"];
                _arrangementsTxtV.text = [dataDict valueForKey:@"arrangement"];
                _obituaryTxtV.text = [dataDict valueForKey:@"obituary_words"];
                _specialRequestTxtV.text = [dataDict valueForKey:@"special_request"];
                //.text = [dataDict valueForKey:@"comments"];

                
            }
            
            else{
            
           [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                
            }
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}
@end
