//
//  MyChildDetailsViewController.m
//  IWill
//
//  Created by A1AUHAIG on 8/10/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyChildDetailsViewController.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "Utils.h"
@interface MyChildDetailsViewController ()<ConnectionManager_Delegate,UITextViewDelegate,UITextFieldDelegate>

{
    ConnectionManager * connectionManager;
    UITextField * activeField;
}

@end

@implementation MyChildDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    
    // Do any additional setup after loading the view.
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    
    _nameTxtF.layer.cornerRadius = 2;
    _nameTxtF.layer.borderWidth = 0.3;
    _nameTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _dobTxtF.layer.cornerRadius = 2;
    _dobTxtF.layer.borderWidth = 0.3;
    _dobTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _dobTxtF.userInteractionEnabled = NO;
    
    _dobTxtF.textColor = [UIColor whiteColor];
    _submitBtn.layer.cornerRadius = 3;
    
    _nameTxtF.text = [_dataDict valueForKey:@"child_name"];
    _dobTxtF.text = [_dataDict valueForKey:@"child_dob"];
    
    
    _dobTxtF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _nameTxtF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)submitBtnAction:(id)sender
{
    [self callAPIForUpdateTrustry];
}

-(void)callAPIForUpdateTrustry
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:[_dataDict valueForKey:@"id"] forKey:@"child_id"];
    
    
    NSMutableDictionary * trusty_infoDict = [[NSMutableDictionary alloc]init];
    [trusty_infoDict setValue:_nameTxtF.text forKey:@"child_name"];
    [trusty_infoDict setValue:_dobTxtF.text forKey:@"child_dob"];

    
    [dataDict setValue:trusty_infoDict forKey:@"child_info"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateChild"];
    
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}

-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1)
        {
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}




-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    
    
    
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag == 100)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    return YES;
}

- (IBAction)dateSelectedBtnAction:(id)sender {
    
    activeField.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    [textView resignFirstResponder];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}



@end
