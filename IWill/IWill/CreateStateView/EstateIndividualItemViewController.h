//
//  EstateIndividualItemViewController.h
//  IWill
//
//  Created by A1AUHAIG on 9/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"

@interface EstateIndividualItemViewController : UIViewController<CMPopTipViewDelegate>

@property(nonatomic,retain) IBOutlet UITableView * individualItemsTableView;


-(IBAction)backBtnAction:(id)sender;

-(IBAction)adMoreItemsBtnAction:(id)sender;

- (IBAction)saveBtnAction:(id)sender;


@property(nonatomic,strong)NSMutableDictionary * individualItemDict;
@property(nonatomic,strong)NSString * estateId;
@property(nonatomic,strong)NSString * estateItemId;

@property(nonatomic,retain) IBOutlet UIButton * saveBtn;


@end
