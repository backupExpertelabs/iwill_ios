//
//  MyTrustyDetailsViewController.m
//  IWill
//
//  Created by A1AUHAIG on 8/9/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyTrustyDetailsViewController.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "Utils.h"


@interface MyTrustyDetailsViewController ()<ConnectionManager_Delegate,UITextViewDelegate,UITextFieldDelegate>

{
    ConnectionManager * connectionManager;
}
@end

@implementation MyTrustyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    
    // Do any additional setup after loading the view.
     connectionManager = [[ConnectionManager alloc] init];
     connectionManager.delegate = self;

    
    _nameTxtF.layer.cornerRadius = 3;
    _nameTxtF.layer.borderWidth = 0.3;
    _nameTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _additionalInfoTxtf.layer.cornerRadius = 3;
    _additionalInfoTxtf.layer.borderWidth = 0.3;
    _additionalInfoTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _contactInfoTxtF.layer.cornerRadius = 3;
    _contactInfoTxtF.layer.borderWidth = 0.3;
    _contactInfoTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _submitBtn.layer.cornerRadius = 3;
    
    _nameTxtF.text = [_dataDict valueForKey:@"name"];
    _contactInfoTxtF.text = [_dataDict valueForKey:@"contact_info"];
    _additionalInfoTxtf.text = [_dataDict valueForKey:@"additional_info"];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)submitBtnAction:(id)sender
{
    [self callAPIForUpdateTrustry];
}




//http://localhost/iwill/index.php/addTrusty            Add the particular addTrusty     POST
//http://localhost/iwill/index.php/getTrusties/$testator_id       get All trusty          GET
//http://localhost/iwill/index.php/getTrusty/$testator_id/$trusty_id      get single trusty  info        GET
//http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id    delete a single trusty        GET
//http://localhost/iwill/index.php/updateTrusty          for update trusty         POST


//"testator_id": 1,
//"trusty_info": [
//                {
//                    "name": "trusty1",
//                    "contact_info": "123",
//                    "additional_info": "verma1"
//                },



-(void)callAPIForUpdateTrustry
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:[_dataDict valueForKey:@"trusty_id"] forKey:@"trusty_id"];
    NSMutableDictionary * trusty_infoDict = [[NSMutableDictionary alloc]init];
    [trusty_infoDict setValue:_nameTxtF.text forKey:@"name"];
    [trusty_infoDict setValue:_contactInfoTxtF.text forKey:@"contact_info"];
    [trusty_infoDict setValue:_additionalInfoTxtf.text forKey:@"additional_info"];
    [dataDict setValue:trusty_infoDict forKey:@"trusty_info"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateTrusty"];
    
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}

-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1)
        {
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}


//-(void)deleteWitnessAction:(UIButton*)sender
//{
//
//
//    NSDictionary* dataDict = [allExecuterArray objectAtIndex:sender.tag];
//    executerID = [dataDict valueForKey:@"executor_id"];
//    [Utils showAlertViewWithTag:200 title:alertTitle message:executerDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
//
//}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}



-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    
    
    
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    
    
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    [textView resignFirstResponder];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}





@end
