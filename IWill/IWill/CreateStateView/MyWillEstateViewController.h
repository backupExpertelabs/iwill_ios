//
//  MyWillEstateViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/7/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"

@interface MyWillEstateViewController : UIViewController<CMPopTipViewDelegate>

@property(nonatomic,retain) IBOutlet UITableView * myEstateTableView;

@property(nonatomic,retain) IBOutlet UIImageView * myEstateImageView;



@property(nonatomic,retain) IBOutlet UITextField * estateNameTxtF;
@property(nonatomic,retain) IBOutlet UITextField * estateValueTxtF;
@property(nonatomic,retain) IBOutlet UIButton * addEstateItemBtn;
@property(nonatomic,retain) IBOutlet UIButton * addEstateRecipientBtn;

-(IBAction)backBtnAction:(id)sender;
-(IBAction)addEstateRecipienr:(id)sender;
-(IBAction)addIndivedualItems:(id)sender;

-(IBAction)addStateItems:(id)sender;

- (IBAction)hintBtnAction:(id)sender;

- (IBAction)saveBtnAction:(id)sender;
@property(nonatomic,retain) IBOutlet UIButton * saveBtn;


@end
