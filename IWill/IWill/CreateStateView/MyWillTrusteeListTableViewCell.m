//
//  MyWillTrusteeListTableViewCell.m
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillTrusteeListTableViewCell.h"
#import "Utils.h"

@implementation MyWillTrusteeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameTextF.layer.borderWidth = 0.5;
    self.nameTextF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _backImage.layer.cornerRadius = 3;

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
