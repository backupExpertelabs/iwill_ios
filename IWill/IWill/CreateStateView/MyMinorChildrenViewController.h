//
//  MyMinorChildrenViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"
#import "JVFloatLabeledTextField.h"
#import "CMPopTipView.h"
@interface MyMinorChildrenViewController : UIViewController<CMPopTipViewDelegate>
- (IBAction)backBtnAction:(id)sender;
- (IBAction)saveGaurdianBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UITableView * minorChildTableView;

@property(nonatomic,retain)IBOutlet JVFloatLabeledTextField * nameTextF;
@property(nonatomic,retain)IBOutlet JVFloatLabeledTextField * dobTextF;
@property(nonatomic,retain)IBOutlet JVFloatLabeledTextField * relationShipTextF;
@property(nonatomic,retain)IBOutlet JVFloatLabeledTextView * commentTextf;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstant;

@property(nonatomic,weak)IBOutlet UIView * addGaurdianView;

- (IBAction)hintBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UITableView * gaurdianTableView;
@property(nonatomic,weak)IBOutlet UIView * gaurdianView;
@property(nonatomic,weak)IBOutlet UIView * gaurdianInnerViewView;

@property(nonatomic,weak)IBOutlet UIButton * existingGaurdianBtn;
@property(nonatomic,weak)IBOutlet UIButton * saveGaurdianBtn;

- (IBAction)existingGaurdianBtnAction:(id)sender;

- (IBAction)hideGuadianView:(id)sender;

 ///////////  Date Picker /////////

@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;




@end
