//
//  GardianDetailsViewController.m
//  IWill
//
//  Created by A1AUHAIG on 8/14/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "GardianDetailsViewController.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "Utils.h"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface GardianDetailsViewController ()<ConnectionManager_Delegate,UITextFieldDelegate,UITextViewDelegate>

{
    ConnectionManager * connectionManager;
    UITextField * activeField;
}
@end

@implementation GardianDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    //
    //    {
    //        "guradian_id": "1",
    //        "name": "Nnbcxvbbnbnvcxbcvnxvcbbvcxcv",
    //        "conatct_info": "23234",
    //        "relation": "22223"
    //    }
    
    _nameTextF.layer.borderWidth = 0.5;
    _nameTextF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _nameTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _dobTextF.layer.borderWidth = 0.5;
    _dobTextF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _dobTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    
    _commentTextf.layer.borderWidth = 0.5;
    _commentTextf.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _commentTextf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    
    _relationShipTextF.layer.borderWidth = 0.5;
    _relationShipTextF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _relationShipTextF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    _nameTextF.layer.cornerRadius = 3;
    _dobTextF.layer.cornerRadius = 3;
    _commentTextf.layer.cornerRadius = 3;
    _relationShipTextF.layer.cornerRadius = 3;

    _deleteBtn.layer.cornerRadius = 3;
    _saveBtn.layer.cornerRadius = 3;
    
    _dateSelectedBtn.layer.cornerRadius =3;
    
    
    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    
    if([_isNewGaurdianAdded isEqualToString:@"No"])
    {
        
        _nameTextF.text = [_dataDict valueForKey:@"name"];
        _dobTextF.text = [_dataDict valueForKey:@"dob"];
        _commentTextf.text = [[_dataDict valueForKey:@"conatct_info"] isKindOfClass:[NSNull class]]?@"":[_dataDict valueForKey:@"conatct_info"];
        _relationShipTextF.text = [_dataDict valueForKey:@"relation"];
       
        [_saveBtn setTitle:@"SAVE" forState:UIControlStateNormal];
        
    }
    else{
         _deleteBtn.hidden = YES;
    }
    
    
    // Do any additional setup after loading the view.
}


-(BOOL)checkGaurdianFieldValidation
{
    
    if(_nameTextF.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Gaurdian Name" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_dobTextF.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Gaurdian DOB" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_relationShipTextF.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Gaurdian Relationship" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_commentTextf.text.length ==0)
    {
        
        [Utils showAlertView:alertTitle message:@"Please Enter Some Comment" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    return YES;
}




-(void)addGaurdianOnServer
{
    
    if([self checkGaurdianFieldValidation])
    {
        
        
        
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        NSString * urlStr = [NSString stringWithFormat:@"%@",@"assignGuardian"];
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        [dataDict setValue:@"" forKey:@"guardian_id"];
        
        //selectedChildArray
        NSMutableDictionary * gardianInfoDict = [[NSMutableDictionary alloc]init];
        [gardianInfoDict setValue:_nameTextF.text forKey:@"name"];
        [gardianInfoDict setValue:_relationShipTextF.text forKey:@"relation"];
        [gardianInfoDict setValue:_commentTextf.text forKey:@"contact_info"];
        [gardianInfoDict setValue:_dobTextF.text forKey:@"dob"];
        
        [dataDict setValue:gardianInfoDict forKey:@"guardian_info"];
        [dataDict setValue:_selectedChildArray forKey:@"child_info"];
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:urlStr];
        
    }
}



#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"deleteGuardianC"])
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            else if([[aResponceDic valueForKey:@"method"] isEqualToString:@"assignGuardianC"])
            {
              [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            }
        }
        
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(void)selectChild:(UIButton*)sender
{
    
}

-(void)viewChild:(UIButton*)sender
{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)saveGaurdianBtnAction:(id)sender
{
    
    if([self checkGaurdianFieldValidation])
    {
        

    
    
    
    if([_isNewGaurdianAdded isEqualToString:@"Yes"])
    {
        
        [self addNewGaurdianOnServer];
    }
    else
    {
        
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        NSString * urlStr = [NSString stringWithFormat:@"%@",@"updateGuardian"];
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        [dataDict setValue:[_dataDict valueForKey:@"guradian_id"] forKey:@"guardian_id"];
        
        
        NSMutableDictionary * gardianInfoDict = [[NSMutableDictionary alloc]init];
        
        [gardianInfoDict setValue:_nameTextF.text forKey:@"name"];
        [gardianInfoDict setValue:_relationShipTextF.text forKey:@"relation"];
        [gardianInfoDict setValue:_commentTextf.text forKey:@"contact_info"];
        [gardianInfoDict setValue:_dobTextF.text forKey:@"dob"];

        
        [dataDict setValue:gardianInfoDict forKey:@"guardian_info"];
        //[dataDict setValue:selectedChildArray forKey:@"child_info"];
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:urlStr];
    }
        
    }
}
- (IBAction)deleteBtnAction:(id)sender
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    
    
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@/%@",@"deleteGuardian",[userData valueForKey:@"id"],[_dataDict valueForKey:@"guradian_id"]];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}





-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    
    
    
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    activeField = textField;
    
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    [textView resignFirstResponder];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == _dobTextF)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
         return NO;
    }
    return YES;
}


- (IBAction)dateSelectedBtnAction:(id)sender {
    
    _dobTextF.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}



-(void)addNewGaurdianOnServer
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSString * urlStr = [NSString stringWithFormat:@"%@",@"assignGuardian"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:@"" forKey:@"guardian_id"];
    
    NSMutableDictionary * gardianInfoDict = [[NSMutableDictionary alloc]init];
    [gardianInfoDict setValue:_nameTextF.text forKey:@"name"];
    [gardianInfoDict setValue:_relationShipTextF.text forKey:@"relation"];
    [gardianInfoDict setValue:_commentTextf.text forKey:@"contact_info"];
    [gardianInfoDict setValue:_dobTextF.text forKey:@"dob"];

    
    [dataDict setValue:gardianInfoDict forKey:@"guardian_info"];
    [dataDict setValue:_selectedChildArray forKey:@"child_info"];
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:urlStr];
    
}



@end
