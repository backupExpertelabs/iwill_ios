//
//  AddAdoptedChildViewController.m
//  IWill
//
//  Created by A1AUHAIG on 9/5/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AddAdoptedChildViewController.h"
#import "ChildTableViewCell.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"

@interface AddAdoptedChildViewController ()<ConnectionManager_Delegate,UITextFieldDelegate>


{
    //int numberOfLegalChildren;
    //int numberOfAdoptedChildren;
    ConnectionManager * connectionManager;
    
//    NSMutableArray * legalChildArry;
    NSMutableArray * adoptedChildArry;
    UITextField * activeField;
    
    
}

@end

@implementation AddAdoptedChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    adoptedChildArry = [[NSMutableArray alloc]init];
    NSMutableDictionary * adoptedChildDict = [[NSMutableDictionary alloc]init];
    [adoptedChildDict setValue:@"" forKey:@"child_name"];
    [adoptedChildDict setValue:@"" forKey:@"child_dob"];
    [adoptedChildArry addObject:adoptedChildDict];
    
    connectionManager = [[ConnectionManager alloc]init];
    _adoptedChildrenTableView.tag =2;
    _adoptedChildrenTableView.scrollEnabled = YES;
//    _adoptedChildrenTableView.layer.borderWidth = 0.5;
//    _adoptedChildrenTableView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    
    
//    _numberOfadoptedChildrenTxtf.layer.borderWidth = 0.5;
//    _numberOfadoptedChildrenTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //_numberOfadoptedChildrenTxtf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    

    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    self.iWilldatePicker.maximumDate = [NSDate date];
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    
    
}


-(void)addMoreAdoptedChildren:(UIButton*)sender
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        return adoptedChildArry.count;
   
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    ChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ChildTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:MyIdentifier];
    }
    
   
        NSMutableDictionary * legalDict = [adoptedChildArry objectAtIndex:indexPath.row];
        
        cell.childNameTxtF.tag =1;
        cell.childDobTxtF.tag = 2;
        
        cell.childNameTxtF.text = [legalDict valueForKey:@"child_name"];
        cell.childDobTxtF.text = [legalDict valueForKey:@"child_dob"];
        
        
    cell.childNameLbl.text = [NSString stringWithFormat:@"Name of child %ld",indexPath.row+1];
    cell.childDobLbl.text = [NSString stringWithFormat:@"DOB of child %ld",indexPath.row+1];
    
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _adoptedChildrenTableView.frame.size.width, 40)];
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
//    [button addTarget:self
//               action:@selector(addMoreAdoptedChildren:)
//     forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:@"Add More" forState:UIControlStateNormal];
//    button.frame = CGRectMake(80.0, 0.0, 160.0, 40.0);
//    [footerView addSubview:button];
//    return footerView;
//}

- (IBAction)legalChildAddMoreBtnAction:(id)sender {
    
//    if(_numberOfLegalChildrenTxtf.text.integerValue == legalChildArry.count)
//    {
//        return;
//    }
//
//    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
//    [legalChildDict setValue:@"" forKey:@"child_name"];
//    [legalChildDict setValue:@"" forKey:@"child_dob"];
//    [legalChildArry addObject:legalChildDict];
//
//    //numberOfLegalChildren = numberOfLegalChildren+1;
//    _legalChildrenViewHeightConstant.constant = 225+ (120*legalChildArry.count);
//    _mainViewHeightConstant.constant = 748 + (120*legalChildArry.count)+(120*adoptedChildArry.count);
//
//    [_legalChildrenTableView reloadData];
}


- (IBAction)adoptedChildAddMoreBtnAction:(id)sender
{
    
    if(_numberOfadoptedChildrenTxtf.text.integerValue == adoptedChildArry.count )
    {
        return;
    }
    
    
    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
    [legalChildDict setValue:@"" forKey:@"child_name"];
    [legalChildDict setValue:@"" forKey:@"child_dob"];
    [adoptedChildArry addObject:legalChildDict];
    
    // numberOfAdoptedChildren = numberOfAdoptedChildren+1;
    _adoptedChildrenViewHeightConstant.constant = 225+ (120*adoptedChildArry.count);
    _mainViewHeightConstant.constant = 748 + (120*adoptedChildArry.count);
    
    [_adoptedChildrenTableView reloadData];
}


- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)checkTextValidation
{
    //legalChildArry
    //adoptedChildArry
    
    //[adoptedChildDict setValue:@"" forKey:@"child_name"];
    //[adoptedChildDict setValue:@"" forKey:@"child_dob"];
    
    
//    for (NSDictionary * ledalDict in legalChildArry) {
//
//        if([[ledalDict valueForKey:@"child_name"] length]==0)
//        {
//            [Utils showAlertView:@"Alert" message:@"Please enter legal Child name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            return NO;
//        }
//
//    }
    
    
    
    
    return YES;
}

- (IBAction)saveBtnAction:(id)sender

{
    [self addAllChieldInfoOnServer];
}

-(void)addAllChieldInfoOnServer
{
    
    if([self checkTextValidation])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        
       // [dataDict setValue:legalChildArry forKey:@"Legal_child"];
        [dataDict setValue:adoptedChildArry forKey:@"Adopted_child"];
        
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"saveChilds"];
    }
    else{
        
    }
    
    
    
    
    
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}





-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;

        CGPoint point = [textField.superview convertPoint:origin toView:_adoptedChildrenTableView];
        NSIndexPath * indexPath = [_adoptedChildrenTableView indexPathForRowAtPoint:point];
        
        
        NSMutableDictionary * legalChildDict = [adoptedChildArry objectAtIndex:indexPath.row];
        
        if(textField.tag ==1 )
        {
            [legalChildDict setValue:textField.text forKey:@"child_name"];
            
            [adoptedChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
        }
        else
        {
            [legalChildDict setValue:textField.text forKey:@"child_dob"];
            [adoptedChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
        }
        
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //CGPoint origin = textField.frame.origin;
    
 
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag == 100)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    return YES;
}


- (IBAction)dateSelectedBtnAction:(id)sender {
    
    activeField.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}

-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _adoptedChildrenTableView.contentInset = contentInsets;
    _adoptedChildrenTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _adoptedChildrenTableView.contentInset = contentInsets;
    _adoptedChildrenTableView.scrollIndicatorInsets = contentInsets;
}

@end
