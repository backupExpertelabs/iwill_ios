//
//  PersonalItemsRecipientPageViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/12/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "PersonalItemsRecipientPageViewController.h"

@interface PersonalItemsRecipientPageViewController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource,UIViewControllerTransitioningDelegate>

{
    NSArray *myViewControllers;
}

@property (assign, nonatomic) NSInteger index;
@end

@implementation PersonalItemsRecipientPageViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.dataSource = self;
    [self didMoveToParentViewController:self];
    
//    UIViewController *tuto1 = [self.storyboard instantiateViewControllerWithIdentifier:@"MyChildRecipientViewController"];
//    UIViewController *tuto2 = [self.storyboard instantiateViewControllerWithIdentifier:@"AddOtherRecipientViewController"];
//    
//     UIViewController *tuto3 = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPrimaryRecipientViewController"];
//     UIViewController *tuto4 = [self.storyboard instantiateViewControllerWithIdentifier:@"AddSecondryRecipientViewController"];
//    
//    myViewControllers = @[tuto1, tuto2,tuto3,tuto4];
//    self.index = 0;
//    
//    [self setViewControllers:@[tuto1] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    //self.transitionStyle = UIPageViewControllerTransitionStyleScroll;
    //[self setModalTransitionStyle:UIPageViewControllerTransitionStyleScroll];
    
    //[self.transitionStyle:UIPageViewControllerTransitionStyleScroll];
}




- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    return myViewControllers[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
     viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
    // get the index of the current view controller on display
    
    if (currentIndex > 0)
    {
        return [myViewControllers objectAtIndex:currentIndex-1];
        // return the previous viewcontroller
    } else
    {
        return nil;
        // do nothing
    }
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
    // get the index of the current view controller on display
    // check if we are at the end and decide if we need to present
    // the next viewcontroller
    if (currentIndex < [myViewControllers count]-1)
    {
        return [myViewControllers objectAtIndex:currentIndex+1];
        // return the next view controller
    } else
    {
        return nil;
        // do nothing
    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [myViewControllers count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
