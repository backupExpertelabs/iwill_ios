//
//  MyWillTrusteeListTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWillTrusteeListTableViewCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel * name;
@property(nonatomic,weak) IBOutlet UILabel * aditionalInfo;
@property(nonatomic,weak) IBOutlet UILabel * contactInfo;
@property(nonatomic,weak) IBOutlet UITextField * nameTextF;
@property(nonatomic,weak) IBOutlet UITextView * aditionalInfoTxtV;
@property(nonatomic,weak) IBOutlet UITextView * contactInfoTxtV;
@property(nonatomic,weak) IBOutlet UIButton * deleteBtn;
@property(nonatomic,weak) IBOutlet UIButton * editBtn;
@property(nonatomic,weak) IBOutlet UIView * backView;

/////// for child //
@property(nonatomic,weak) IBOutlet UILabel * childName;
@property(nonatomic,weak) IBOutlet UILabel * childDOB;
@property(nonatomic,weak) IBOutlet UILabel * childType;
@property(nonatomic,weak) IBOutlet UIButton * selectChild;
@property(nonatomic,weak) IBOutlet UIButton * childView;

@property(nonatomic,weak) IBOutlet UIImageView *backImage;

@end
