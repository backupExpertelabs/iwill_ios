//
//  MyChildListViewController.m
//  IWill
//
//  Created by A1AUHAIG on 8/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyChildListViewController.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "Utils.h"
#import "ChildTableViewCell.h"
#import "MyChildDetailsViewController.h"

@interface MyChildListViewController ()<ConnectionManager_Delegate>

{
    ConnectionManager* connectionManager;
    NSMutableArray * childLegalDataArray;
    NSMutableArray * childAdoptedDataArray;
    NSString * childId;

}
@end

@implementation MyChildListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    childLegalDataArray = [[NSMutableArray alloc]init];
    childAdoptedDataArray = [[NSMutableArray alloc]init];

    
    _addBtn.layer.cornerRadius = 4;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self getAllChieldInfoOnServer];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
    {
        return childLegalDataArray.count;
        
    }
    else{
        return childAdoptedDataArray.count;
    }
    
    return 0;
    
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    ChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ChildTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:MyIdentifier];
    }
    
    if(indexPath.section == 0)
    {
        
        NSMutableDictionary * legalDict = [childLegalDataArray objectAtIndex:indexPath.row];
        
        cell.childNameTxtF.tag =1;
        cell.childDobTxtF.tag = 2;
        if(![[legalDict valueForKey:@"child_name"] isKindOfClass:[NSNull class]])
        {
            cell.childNameTxtF.text = [legalDict valueForKey:@"child_name"];

        }
        else
        {
            cell.childNameTxtF.text = @"";

        }
        
        if(![[legalDict valueForKey:@"child_dob"] isKindOfClass:[NSNull class]])
        {
            cell.childDobTxtF.text = [legalDict valueForKey:@"child_dob"];

        }
        else
        {
            cell.childDobTxtF.text = @"";
            
        }
        
        //cell.childNameTxtF.text = [legalDict valueForKey:@"child_name"];
        //cell.childDobTxtF.text = [legalDict valueForKey:@"child_dob"];
        cell.deleteBtn.tag = [[legalDict valueForKey:@"id"] intValue];
        
        
        
    }
    if(indexPath.section == 1)
    {
        NSMutableDictionary * legalDict = [childAdoptedDataArray objectAtIndex:indexPath.row];
        
        cell.childNameTxtF.tag =1;
        cell.childDobTxtF.tag = 2;
        
        cell.childNameTxtF.text = [legalDict valueForKey:@"child_name"];
        cell.childDobTxtF.text = [legalDict valueForKey:@"child_dob"];
        cell.deleteBtn.tag = [[legalDict valueForKey:@"id"] intValue];
    }
    
    
    cell.childNameTxtF.layer.borderColor = [UIColor clearColor].CGColor;
    cell.childDobTxtF.layer.borderColor = [UIColor clearColor].CGColor;
    
    
    [cell.deleteBtn addTarget:self action:@selector(deleteChild:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    if(section == 0)
//        return @"Legal Child";
//    else
//        return @"Adopted Child";
//}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width-0, 50)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, view.frame.size.width-20 , 45)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.layer.cornerRadius =3;
    label.backgroundColor = [Utils getBorderLineColor];
    NSString *string;
    if(section ==0)
    {
        string =@"Legal Child Information";
    }
    else{
        string =@"Adopted Child Information";
    }
    [label setText:string];
    [view addSubview:label];
    label.layer.cornerRadius =3;
    view.backgroundColor = [UIColor clearColor];
    //[view setBackgroundColor:[UIColor clearColor]]; //your background color...
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyChildDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyChildDetailsViewController"];
    
    NSMutableDictionary * childDic;
    if(indexPath.section == 0)
    {
        
        childDic = [childLegalDataArray objectAtIndex:indexPath.row];
    }
    if(indexPath.section == 1)
    {
        childDic = [childAdoptedDataArray objectAtIndex:indexPath.row];
    }
    
    
    vc.dataDict = childDic;
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)getAllChieldInfoOnServer
{
    
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@",@"getChilds",[userData valueForKey:@"id"]];
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
    }




#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getChildsC"])
            {
            
            
//                childLegalDataArray = [[NSMutableArray alloc]init];
//                childAdoptedDataArray = [[NSMutableArray alloc]init];

            
            childLegalDataArray = [[aResponceDic valueForKey:@"Result"] valueForKey:@"Legal_child"];
            childAdoptedDataArray = [[aResponceDic valueForKey:@"Result"] valueForKey:@"Adopted_child"];
            
            [_legalChildrenTableView reloadData];
            
            }
            else{
                [self getAllChieldInfoOnServer];

            }
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


- (IBAction)addChildBtnAction:(id)sender {
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNewChildViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)deleteChild:(UIButton*)sender
{
    childId = [NSString stringWithFormat:@"%ld", (long)sender.tag];
    [Utils showAlertViewWithTag:200 title:alertTitle message:childDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        
    }
    
    
    if(alertView.tag == 200 && buttonIndex ==1)
    {
        [self deleteChildAPI];
    }
    
}



-(void)deleteChildAPI
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    //    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"executor_id"];
    //
    //    NSLog(@"hdsghj %@",dataDict);
    
    
    //trusrtyId
    NSString * childUrl = [NSString stringWithFormat:@"%@/%@/%@",@"deleteChild",[userData valueForKey:@"id"],childId];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:childUrl];
    
}



@end
