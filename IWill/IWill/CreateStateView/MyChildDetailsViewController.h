//
//  MyChildDetailsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 8/10/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyChildDetailsViewController : UIViewController
@property(nonatomic,weak)IBOutlet UITextField * nameTxtF;
@property(nonatomic,weak)IBOutlet UITextField * dobTxtF;
@property(nonatomic,weak)IBOutlet UIButton * submitBtn;
- (IBAction)submitBtnAction:(id)sender;


@property(nonatomic,weak)IBOutlet UIScrollView * mainScrollView;

@property(nonatomic,strong)NSMutableDictionary * dataDict;

@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;




@end
