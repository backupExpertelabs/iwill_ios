//
//  GaudianTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 8/14/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GaudianTableViewCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel * name;
@property(nonatomic,weak) IBOutlet UILabel * aditionalInfo;
@property(nonatomic,weak) IBOutlet UILabel * contactInfo;
@property(nonatomic,weak) IBOutlet UIButton * selectBtn;
@property(nonatomic,weak) IBOutlet UIView * backView;

@property(nonatomic,weak) IBOutlet UIImageView * backImage;

@end
