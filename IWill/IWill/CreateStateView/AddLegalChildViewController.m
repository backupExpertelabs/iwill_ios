//
//  AddLegalChildViewController.m
//  IWill
//
//  Created by A1AUHAIG on 9/4/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AddLegalChildViewController.h"
#import "ChildTableViewCell.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"

@interface AddLegalChildViewController ()<ConnectionManager_Delegate,UITextFieldDelegate>


{
    ConnectionManager * connectionManager;
    NSMutableArray * legalChildArry;
}

@end

@implementation AddLegalChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerForKeyboardNotifications];

    legalChildArry = [[NSMutableArray alloc]init];
    
    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
    [legalChildDict setValue:@"" forKey:@"child_name"];
    [legalChildDict setValue:@"" forKey:@"child_dob"];
    [legalChildArry addObject:legalChildDict];
    
    connectionManager = [[ConnectionManager alloc]init];
    
    _legalChildrenTableView.scrollEnabled = YES;
    _legalChildrenTableView.tag =1;
    _numberOfLegalChildrenTxtf.layer.borderWidth = 0.5;
    _numberOfLegalChildrenTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
    _numberOfLegalChildrenTxtf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _numberOfLegalChildrenTxtf.layer.cornerRadius = 3;
    _saveBtn.layer.cornerRadius = 3;
    
    
    
    
    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    
    
    
    
    
}


-(void)addMoreAdoptedChildren:(UIButton*)sender
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _legalChildrenTableView)
    {
        return legalChildArry.count;
        
    }
    
    
    return 0;
    
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    ChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ChildTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:MyIdentifier];
    }
    
    if(tableView == _legalChildrenTableView)
    {
        
        NSMutableDictionary * legalDict = [legalChildArry objectAtIndex:indexPath.row];
        
        cell.childNameTxtF.tag =1;
        cell.childDobTxtF.tag = 2;
        cell.childNameTxtF.text = [legalDict valueForKey:@"child_name"];
        cell.childDobTxtF.text = [legalDict valueForKey:@"child_dob"];
        
        cell.childNameLbl.text = [NSString stringWithFormat:@"Name of child %ld",indexPath.row+1];
        cell.childDobLbl.text = [NSString stringWithFormat:@"DOB of child %ld",indexPath.row+1];
        
        cell. childNameTxtFFloat  = [Utils setTextFieldBottumBorder: cell. childNameTxtFFloat andController:self];
        cell. childDobTxtF  = [Utils setTextFieldBottumBorder: cell. childDobTxtF andController:self];
        
    }
    
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _legalChildrenTableView.frame.size.width, 40)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button addTarget:self
               action:@selector(addMoreAdoptedChildren:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Add More" forState:UIControlStateNormal];
    button.frame = CGRectMake(80.0, 0.0, 160.0, 40.0);
    [footerView addSubview:button];
    return footerView;
}

- (IBAction)legalChildAddMoreBtnAction:(id)sender {
    
    if(_numberOfLegalChildrenTxtf.text.integerValue == legalChildArry.count)
    {
        return;
    }
    
    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
    [legalChildDict setValue:@"" forKey:@"child_name"];
    [legalChildDict setValue:@"" forKey:@"child_dob"];
    [legalChildArry addObject:legalChildDict];
    
    //numberOfLegalChildren = numberOfLegalChildren+1;
    _legalChildrenViewHeightConstant.constant = 225+ (120*legalChildArry.count);
    _mainViewHeightConstant.constant = 748 + (120*legalChildArry.count);
    
    [_legalChildrenTableView reloadData];
}





- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)checkTextValidation
{
    
    for (NSDictionary * ledalDict in legalChildArry) {
        
        if([[ledalDict valueForKey:@"child_name"] length]==0)
        {
            [Utils showAlertView:@"Alert" message:@"Please enter legal Child name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return NO;
        }
        
    }
    
    return YES;
}

- (IBAction)saveBtnAction:(id)sender

{
    [self addAllChieldInfoOnServer];
}

-(void)addAllChieldInfoOnServer
{
    
    if([self checkTextValidation])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        NSArray * adoptedArr = [[NSArray alloc]init];
        [dataDict setValue:legalChildArry forKey:@"Legal_child"];
        [dataDict setValue:adoptedArr forKey:@"Adopted_child"];
        
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"saveChilds"];
    }
    else{
        
    }

}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    return YES;
}



-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    
        
        CGPoint point = [textField.superview convertPoint:origin toView:_legalChildrenTableView];
        NSIndexPath * indexPath = [_legalChildrenTableView indexPathForRowAtPoint:point];
        
        
        NSMutableDictionary * legalChildDict = [legalChildArry objectAtIndex:indexPath.row];
        
        if(textField.tag ==1 )
        {
            [legalChildDict setValue:textField.text forKey:@"child_name"];
            
            [legalChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
        }
        else
        {
            [legalChildDict setValue:textField.text forKey:@"child_dob"];
            [legalChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
        }
        
    

}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _legalChildrenTableView.contentInset = contentInsets;
    _legalChildrenTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _legalChildrenTableView.contentInset = contentInsets;
    _legalChildrenTableView.scrollIndicatorInsets = contentInsets;
}

- (IBAction)dateSelectedBtnAction:(id)sender
{
    
}

@end
