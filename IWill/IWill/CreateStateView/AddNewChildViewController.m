//
//  AddNewChildViewController.m
//  IWill
//
//  Created by A1AUHAIG on 9/18/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AddNewChildViewController.h"
#import "ChildTableViewCell.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"
@interface AddNewChildViewController ()<ConnectionManager_Delegate,UITextFieldDelegate>

{
    ConnectionManager * connectionManager;
    NSMutableArray * legalChildArry;
    NSMutableArray * adoptedChildArry;
    UITextField * tempTextF;
    
    BOOL isLegalChileAdded;
    
}


@end

@implementation AddNewChildViewController
//@synthesize tabBar1;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    isLegalChileAdded = YES;
    
    [self registerForKeyboardNotifications];
    
    legalChildArry = [[NSMutableArray alloc]init];
    adoptedChildArry = [[NSMutableArray alloc]init];
    _adoptedChildView.hidden = YES;
    
    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
    [legalChildDict setValue:@"" forKey:@"child_name"];
    [legalChildDict setValue:@"" forKey:@"child_dob"];
    [legalChildArry addObject:legalChildDict];
    
    
    
    NSMutableDictionary * adoptedChildDict = [[NSMutableDictionary alloc]init];
    [adoptedChildDict setValue:@"" forKey:@"child_name"];
    [adoptedChildDict setValue:@"" forKey:@"child_dob"];
    [adoptedChildArry addObject:adoptedChildDict];
    
    
    
    connectionManager = [[ConnectionManager alloc]init];
    
    _legalChildrenTableView.scrollEnabled = YES;
    _legalChildrenTableView.tag =1;
    
    _numberOfLegalChildrenTxtf.layer.borderWidth = 0.5;
    _numberOfLegalChildrenTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
    _numberOfLegalChildrenTxtf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _numberOfLegalChildrenTxtf.layer.cornerRadius = 3;
    
    _numberOfadoptedChildrenTxtf.layer.borderWidth = 0.5;
    _numberOfadoptedChildrenTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
    _numberOfadoptedChildrenTxtf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _numberOfadoptedChildrenTxtf.layer.cornerRadius = 3;
    
    
    _saveLegalBtn.layer.cornerRadius = 3;
    _saveAdoptedBtn.layer.cornerRadius = 3;
    
    
    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
}


- (IBAction)saveLegalBtnAction:(id)sender
{
    [self addAllChieldInfoOnServer];
}
- (IBAction)saveAdoptedBtnAction:(id)sender
{
    [self addAllChieldInfoOnServer];
}



-(void)addMoreAdoptedChildren:(UIButton*)sender
{
    
    if(isLegalChileAdded)
    {
        
        
        NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
        [legalChildDict setValue:@"" forKey:@"child_name"];
        [legalChildDict setValue:@"" forKey:@"child_dob"];
        [legalChildArry addObject:legalChildDict];
        
        [_legalChildrenTableView reloadData];
        
    }
    
    else
    {
        NSMutableDictionary * adoptedChildDict = [[NSMutableDictionary alloc]init];
        [adoptedChildDict setValue:@"" forKey:@"child_name"];
        [adoptedChildDict setValue:@"" forKey:@"child_dob"];
        [adoptedChildArry addObject:adoptedChildDict];
        
        [_adoptedChildrenTableView reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _legalChildrenTableView)
    {
        return legalChildArry.count;
        
    }
    if(tableView == _adoptedChildrenTableView)
    {
        return adoptedChildArry.count;
        
    }
    
    return 0;
    
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    ChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ChildTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:MyIdentifier];
    }
    
    if(tableView == _legalChildrenTableView)
    {
        
        NSMutableDictionary * legalDict = [legalChildArry objectAtIndex:indexPath.row];
        
        cell.childNameTxtFFloat.tag =1;
        cell.childDobTxtFFloat.tag = 2;
        cell.childNameTxtFFloat.text = [legalDict valueForKey:@"child_name"];
        cell.childDobTxtFFloat.text = [legalDict valueForKey:@"child_dob"];
        
        cell.childNameLbl.text = [NSString stringWithFormat:@"Name of child %d",indexPath.row+1];
        cell.childDobLbl.text = [NSString stringWithFormat:@"DOB of child %d",indexPath.row+1];
        
        cell. childNameTxtFFloat  = [Utils setTextFieldBottumBorder: cell. childNameTxtFFloat andController:self];
        cell. childDobTxtFFloat  = [Utils setTextFieldBottumBorder: cell. childDobTxtFFloat andController:self];
        
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(deletetCell:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        {
            
            NSMutableDictionary * legalDict = [adoptedChildArry objectAtIndex:indexPath.row];
            
            cell.childNameTxtFFloat.tag =1;
            cell.childDobTxtFFloat.tag = 2;
            cell.childNameTxtFFloat.text = [legalDict valueForKey:@"child_name"];
            cell.childDobTxtFFloat.text = [legalDict valueForKey:@"child_dob"];
            
            cell.childNameLbl.text = [NSString stringWithFormat:@"Name of child %d",indexPath.row+1];
            cell.childDobLbl.text = [NSString stringWithFormat:@"DOB of child %d",indexPath.row+1];
            
            cell. childNameTxtFFloat  = [Utils setTextFieldBottumBorder: cell. childNameTxtFFloat andController:self];
            cell. childDobTxtFFloat  = [Utils setTextFieldBottumBorder: cell. childDobTxtFFloat andController:self];
            
            
            cell.deleteBtn.tag = indexPath.row;
            [cell.deleteBtn addTarget:self action:@selector(deletetCell:) forControlEvents:UIControlEventTouchUpInside];
            
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _legalChildrenTableView.frame.size.width, 60)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button addTarget:self
               action:@selector(addMoreAdoptedChildren:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Add More" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[Utils getBorderLineColor]];
    button.frame = CGRectMake(10.0, 10.0, _legalChildrenTableView.frame.size.width-20, 40.0);
    button.layer.cornerRadius = 3;
    [footerView addSubview:button];
    return footerView;
}

- (IBAction)legalChildAddMoreBtnAction:(id)sender {
    
    if(_numberOfLegalChildrenTxtf.text.integerValue == legalChildArry.count)
    {
        return;
    }
    
    
    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
    [legalChildDict setValue:@"" forKey:@"child_name"];
    [legalChildDict setValue:@"" forKey:@"child_dob"];
    [legalChildArry addObject:legalChildDict];
    
    [_legalChildrenTableView reloadData];
}

-(IBAction)adoptedChildAddMoreBtnAction:(id)sender
{
    if(_numberOfadoptedChildrenTxtf.text.integerValue == adoptedChildArry.count)
    {
        return;
    }
    
    NSMutableDictionary * legalChildDict = [[NSMutableDictionary alloc]init];
    [legalChildDict setValue:@"" forKey:@"child_name"];
    [legalChildDict setValue:@"" forKey:@"child_dob"];
    [adoptedChildArry addObject:legalChildDict];
    
    [_adoptedChildrenTableView reloadData];
}

-(BOOL)checkTextValidation
{
    if(isLegalChileAdded)
    {
        for (NSDictionary * ledalDict in legalChildArry) {
            
            if([[ledalDict valueForKey:@"child_name"] length]==0)
            {
                [Utils showAlertView:@"Alert" message:@"Please enter legal Child name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                return NO;
            }
            
            if([[ledalDict valueForKey:@"child_dob"] length]==0)
            {
                [Utils showAlertView:@"Alert" message:@"Please select child dob" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                return NO;
            }
            
        }
    }
    else
    {
        for (NSDictionary * ledalDict in adoptedChildArry) {
            
            if([[ledalDict valueForKey:@"child_name"] length]==0)
            {
                [Utils showAlertView:@"Alert" message:@"Please enter adopted Child name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                return NO;
            }
            
            if([[ledalDict valueForKey:@"child_dob"] length]==0)
            {
                [Utils showAlertView:@"Alert" message:@"Please select child dob" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                return NO;
            }
            
        }
    }
    
    return YES;
}

- (IBAction)saveBtnAction:(id)sender

{
    [self addAllChieldInfoOnServer];
}

-(void)addAllChieldInfoOnServer
{
    
    if([self checkTextValidation])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        NSArray * childArr = [[NSArray alloc]init];
        
        if(isLegalChileAdded)
        {
            [dataDict setValue:legalChildArry forKey:@"Legal_child"];
            [dataDict setValue:childArr forKey:@"Adopted_child"];
        }
        else
        {
            [dataDict setValue:childArr forKey:@"Legal_child"];
            [dataDict setValue:adoptedChildArry forKey:@"Adopted_child"];
        }
        
        //        [dataDict setValue:legalChildArry forKey:@"Legal_child"];
        //        [dataDict setValue:adoptedChildArry forKey:@"Adopted_child"];
        
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"saveChilds"];
    }
    else{
        
    }
    
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tempTextF = textField;
    
    if(textField == _numberOfadoptedChildrenTxtf || textField == _numberOfLegalChildrenTxtf)
    {
        [self addToolBarForDoneButton:textField];
    }
    
    if(textField.tag ==2)
    {
        tempTextF = textField;
        _datebackgroundView.hidden = NO;
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if(textField == _numberOfadoptedChildrenTxtf || textField == _numberOfLegalChildrenTxtf)
    {
        return;
    }
    
    
    CGPoint origin = textField.frame.origin;

    
    if(isLegalChileAdded)
    {
        
        
        CGPoint point = [textField.superview convertPoint:origin toView:_legalChildrenTableView];
        NSIndexPath * indexPath = [_legalChildrenTableView indexPathForRowAtPoint:point];
        NSMutableDictionary * legalChildDict = [legalChildArry objectAtIndex:indexPath.row];
        if(textField.tag ==1 )
        {
            [legalChildDict setValue:textField.text forKey:@"child_name"];
            
            [legalChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
        }
    }
    else{
        CGPoint point = [textField.superview convertPoint:origin toView:_adoptedChildrenTableView];
        NSIndexPath * indexPath = [_adoptedChildrenTableView indexPathForRowAtPoint:point];
        NSMutableDictionary * legalChildDict = [adoptedChildArry objectAtIndex:indexPath.row];
        if(textField.tag ==1 )
        {
        [legalChildDict setValue:textField.text forKey:@"child_name"];
        [adoptedChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
        }
    }
    
    
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}



- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    if(isLegalChileAdded)
    {
        _legalChildrenTableView.contentInset = contentInsets;
        _legalChildrenTableView.scrollIndicatorInsets = contentInsets;
    }
    else{
        _adoptedChildrenTableView.contentInset = contentInsets;
        _adoptedChildrenTableView.scrollIndicatorInsets = contentInsets;
    }
    
}


- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    
    if(isLegalChileAdded)
    {
        _legalChildrenTableView.contentInset = contentInsets;
        _legalChildrenTableView.scrollIndicatorInsets = contentInsets;
    }
    else{
        _adoptedChildrenTableView.contentInset = contentInsets;
        _adoptedChildrenTableView.scrollIndicatorInsets = contentInsets;
    }
    
    //    _legalChildrenTableView.contentInset = contentInsets;
    //    _legalChildrenTableView.scrollIndicatorInsets = contentInsets;
}


- (IBAction)dateSelectedBtnAction:(id)sender
{
    tempTextF.text =[Utils getDateFromDate:_iWilldatePicker.date];  _datebackgroundView.hidden = YES;
    
    CGPoint origin = tempTextF.frame.origin;
    
    if(isLegalChileAdded)
    {
        
        
        CGPoint point = [tempTextF.superview convertPoint:origin toView:_legalChildrenTableView];
        NSIndexPath * indexPath = [_legalChildrenTableView indexPathForRowAtPoint:point];
        NSMutableDictionary * legalChildDict = [legalChildArry objectAtIndex:indexPath.row];
        [legalChildDict setValue:tempTextF.text forKey:@"child_dob"];
        [legalChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
    }
    else{
        CGPoint point = [tempTextF.superview convertPoint:origin toView:_adoptedChildrenTableView];
        NSIndexPath * indexPath = [_adoptedChildrenTableView indexPathForRowAtPoint:point];
        NSMutableDictionary * legalChildDict = [adoptedChildArry objectAtIndex:indexPath.row];
        [legalChildDict setValue:tempTextF.text forKey:@"child_dob"];
        [adoptedChildArry replaceObjectAtIndex:(NSUInteger)indexPath.row withObject:legalChildDict];
    }
    
}


-(IBAction)legalBtnAction:(UIButton*)sender
{
    
    isLegalChileAdded = YES;
    _legalChildView.hidden = NO;
    _adoptedChildView.hidden = YES;
    _buttonHilightLbl.frame = CGRectMake(0, 49, self.view.frame.size.width/2, 1);
    [_legalChildrenTableView reloadData];
    
    
}

-(IBAction)adoptedBtnAction:(UIButton*)sender
{
    
    
    isLegalChileAdded = NO;
    
    _legalChildView.hidden = YES;
    _adoptedChildView.hidden = NO;
    _buttonHilightLbl.frame = CGRectMake(self.view.frame.size.width/2, 49, self.view.frame.size.width/2, 1);
    
    [_adoptedChildrenTableView reloadData];
    
}


-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)deletetCell:(UIButton*)sender
{
    if(isLegalChileAdded)
    {
        [legalChildArry removeObjectAtIndex:sender.tag];
        [_legalChildrenTableView reloadData];
    }
   else
   {
        [adoptedChildArry removeObjectAtIndex:sender.tag];
       [_adoptedChildrenTableView reloadData];
    }
}



-(void)addToolBarForDoneButton:(UITextField*)textF
{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    textF.inputAccessoryView = keyboardToolbar;
}

-(void)yourTextViewDoneButtonPressed
{
    [tempTextF resignFirstResponder];
}


@end
