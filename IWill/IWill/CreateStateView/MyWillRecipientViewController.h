//
//  MyWillRecipientViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/16/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"
@interface MyWillRecipientViewController : UIViewController<CMPopTipViewDelegate>
- (IBAction)addChildBtnAction:(id)sender;
- (IBAction)otherBtnAction:(id)sender;
- (IBAction)primaryBtnAction:(id)sender;
- (IBAction)secondaryBtnAction:(id)sender;
- (IBAction)hintBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;

@property(nonatomic,strong) NSMutableDictionary * benificiaryDataDict;
@property(nonatomic,strong) NSString * benificiaryType;
@property(nonatomic,weak)IBOutlet UIButton * addChildBtn;
@property(nonatomic,weak)IBOutlet UIButton * addOthertn;
@property(nonatomic,weak)IBOutlet UIButton * addPrimaryBtn;
@property(nonatomic,weak)IBOutlet UIButton * addSecondaryBtn;

@end
