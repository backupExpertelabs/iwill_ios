//
//  MyWillRecipientViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/16/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillRecipientViewController.h"
#import "AddPrimaryRecipientViewController.h"
#import "AddSecondryRecipientViewController.h"
#import "AddOtherRecipientViewController.h"
#import "MyChildRecipientViewController.h"

@interface MyWillRecipientViewController ()
{
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
}
@end

@implementation MyWillRecipientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    visiblePopTipViews = [[NSMutableArray alloc]init];
    
    
    _addChildBtn.layer.cornerRadius =3;
    _addOthertn.layer.cornerRadius =3;
    _addPrimaryBtn.layer.cornerRadius =3;
     _addSecondaryBtn.layer.cornerRadius =3;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addChildBtnAction:(id)sender {
    
    MyChildRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyChildRecipientViewController"];
    
    vc.recipientChild = _benificiaryDataDict;
    vc.banificiaryType = _benificiaryType;
    [self.navigationController pushViewController:vc animated:YES];
}



- (IBAction)otherBtnAction:(id)sender {
    
    AddOtherRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddOtherRecipientViewController"];
    vc.recipientOther = _benificiaryDataDict;
    vc.banificiaryType = _benificiaryType;

    [self.navigationController pushViewController:vc animated:YES];
}



- (IBAction)primaryBtnAction:(id)sender {
    
    AddPrimaryRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPrimaryRecipientViewController"];
    vc.recipientPrimary = _benificiaryDataDict;
    vc.banificiaryType = _benificiaryType;

    [self.navigationController pushViewController:vc animated:YES];
}



- (IBAction)secondaryBtnAction:(id)sender {
    
    AddSecondryRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddSecondryRecipientViewController"];
    vc.recipientSecondry = _benificiaryDataDict;
    vc.banificiaryType = _benificiaryType;

    [self.navigationController pushViewController:vc animated:YES];
}



////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        contentMessage = @"the person(s), group or charity you wish to leave the items you own.";
        
        
        CMPopTipView *popTipView;
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        /* Some options to try.
         */
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        //popTipView.hasGradientBackground = NO;
        //popTipView.cornerRadius = 2.0;
        //popTipView.sidePadding = 30.0f;
        //popTipView.topMargin = 20.0f;
        //popTipView.pointerSize = 50.0f;
        //popTipView.hasShadow = NO;
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}



- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
