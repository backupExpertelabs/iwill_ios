//
//  MyWillPersonalItemsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/31/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CMPopTipView.h"

@interface MyWillPersonalItemsViewController : UIViewController<CMPopTipViewDelegate>

@property(nonatomic,weak)IBOutlet UITableView * personalItemsTableView;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addMorePersonalItemsBtn;
- (IBAction)addMorePersonalItemsBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *personalItemsViewHeightConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstant;

- (IBAction)hintBtnAction:(id)sender;
- (IBAction)savePIBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *totalAmountTxtF;
@property (weak, nonatomic) IBOutlet UIButton *savePIBtn;


@end
