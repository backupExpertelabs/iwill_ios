//
//  MyWillEstateViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/7/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillEstateViewController.h"
#import "TrustryTableViewCell.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "Utils.h"
#import "MyWillRecipientViewController.h"
#import "EstateIndividualItemViewController.h"

@interface MyWillEstateViewController ()<ConnectionManager_Delegate,UITextFieldDelegate>

{
    int numberOfItems;
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    ConnectionManager * connectionManager;
    NSMutableDictionary * estateDict;
    
    NSMutableArray * estateItemsArray;
    NSMutableArray * cellArray;
    UITextField * tempTxtF;
    
}
@end

@implementation MyWillEstateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerForKeyboardNotifications];

    estateItemsArray = [[NSMutableArray alloc]init];
    
    visiblePopTipViews = [[NSMutableArray alloc]init];
    numberOfItems = 1;
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    
    
    _myEstateImageView.layer.cornerRadius = 3;
    _estateNameTxtF.layer.cornerRadius = 2;
    _estateNameTxtF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    _estateNameTxtF.layer.borderColor = [Utils getBorderLineColor].CGColor;
    _estateNameTxtF.layer.borderWidth = 0.5;
    
    _estateValueTxtF.layer.borderColor = [Utils getBorderLineColor].CGColor;
    _estateValueTxtF.layer.borderWidth = 0.5;
    
    _estateValueTxtF.layer.cornerRadius = 2;
    _addEstateItemBtn.layer.cornerRadius = 3;
    _estateValueTxtF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    _addEstateRecipientBtn.layer.cornerRadius = 3;
    _saveBtn.layer.cornerRadius = 3;

    _estateValueTxtF.delegate = self;
    _estateNameTxtF.delegate = self;
    
    

    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self getAllState];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backBtnAction:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
    
}



-(IBAction)addEstateRecipienr:(id)sender
{
    
    if(_estateNameTxtF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:@"Please enter estate name" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return;
    }
    
    if(_estateValueTxtF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:@"Please enter estate value" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return;
    }
    
    MyWillRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillRecipientViewController"];
    vc.benificiaryType = @"estate";
    vc.benificiaryDataDict = estateDict;
    [self.navigationController pushViewController:vc animated:YES];
}



-(IBAction)addIndivedualItems:(id)sender
{
    
}


-(IBAction)addStateItems:(id)sender
{
    
    
    NSMutableArray * users_info = [[NSMutableArray alloc] init];
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:[estateDict valueForKey:@"estate_id"] forKey:@"estate_id"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addStateItem"];
}


//    numberOfItems = numberOfItems+1;
//    [_myEstateTableView reloadData];


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return estateItemsArray.count;
    
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    TrustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[TrustryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    
    NSDictionary * stateItemDict = [estateItemsArray objectAtIndex:indexPath.row];
    
    cell.nameTextF.text = [stateItemDict valueForKey:@"name"];
    cell.valueTextF.text = [stateItemDict valueForKey:@"value"];
    cell.locationTextF.text = [stateItemDict valueForKey:@"locality"];
    
    cell.deleteBtn.tag = [[stateItemDict valueForKey:@"estate_items_id"] intValue];
    [cell.deleteBtn addTarget:self action:@selector(deleteEstateItems:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.addBanificiaryBtn.tag = indexPath.row;
    [cell.addBanificiaryBtn addTarget:self action:@selector(addBanificiaryOnStateItems:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.addIndividualItemsBtn.tag = indexPath.row;
    [cell.addIndividualItemsBtn addTarget:self action:@selector(addIndividualItemOnStateItem:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)deleteEstateItems:(UIButton*)sender
{
    NSString * estateItemId = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    NSString * estateId =   [estateDict valueForKey:@"estate_id"] ;

    NSString * url = [NSString stringWithFormat:@"%@/%@/%@/%@",@"deleteStateItem",[userData valueForKey:@"id"],estateId,estateItemId];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:url];
    
    // deleteStateItem/{testator_id}/{ estate_id}/{estate_Item_id}
    
}


////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"A person or firm appointed by you separate from the will that holds and administers property or assets for the benefit of a third party.";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}



-(void)getAllState
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    NSString * childUrl = [NSString stringWithFormat:@"%@/%@",@"getEstate",[userData valueForKey:@"id"]];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:childUrl];
    
}



#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getEstateC"])
            {
                
                [estateItemsArray removeAllObjects];
                
                estateDict = [[NSMutableDictionary alloc] init];
                estateDict = [aResponceDic valueForKey:@"Result"];
                
                for (NSDictionary * indi in [[aResponceDic valueForKey:@"Result"] valueForKey:@"estate_items"])
                    
                {
                    [estateItemsArray addObject:indi];
                }
                
                
                //str1 != nil ? true : false
                _estateNameTxtF.text = ![[estateDict valueForKey:@"name"] isKindOfClass:[NSNull class]] ? [estateDict valueForKey:@"name"] :@"";
                _estateValueTxtF.text = ![[estateDict valueForKey:@"value"] isKindOfClass:[NSNull class]] ? [estateDict valueForKey:@"value"] :@"";;
                [_myEstateTableView reloadData];
            }
            
            else if ([[aResponceDic valueForKey:@"method"] isEqualToString:@"addStateItemC"])
            {
                [self getAllState];
//                [estateItemsArray addObject:[aResponceDic valueForKey:@"Result"]];
//                [_myEstateTableView reloadData];
                
            }
            
            
            else if ([[aResponceDic valueForKey:@"method"] isEqualToString:@"deleteStateItemC"])
            {
                [self getAllState];
            }
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



- (IBAction)saveBtnAction:(id)sender
{
    
    
    NSMutableArray * estate_items_info = [[NSMutableArray alloc] init];
    
    
    for (int i=0; i<estateItemsArray.count;i++)
    {
        
        
        NSDictionary * estateDict = [estateItemsArray objectAtIndex:i];
        
        
        
//        NSIndexPath  *indexPaths = [NSIndexPath indexPathForRow:i inSection:0];
//
//        TrustryTableViewCell * selecCell = [_myEstateTableView cellForRowAtIndexPath:indexPaths];
//
//
//        //TrustryTableViewCell * newCell = [_myEstateTableView cellForRowAtIndexPath:indexPaths];
        
        if([[estateDict valueForKey:@"name"] length] ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter estate item name and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        
        
        if([[estateDict valueForKey:@"name"] length] ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter estate item value save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        if([[estateDict valueForKey:@"locality"] length] ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter estate item location and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        NSMutableDictionary * estateDict1 = [[NSMutableDictionary alloc] init];

        [estateDict1 setValue:[estateDict valueForKey:@"estate_items_id"] forKey:@"estate_items_id"];
        [estateDict1 setValue:[estateDict valueForKey:@"name"] forKey:@"name"];
        [estateDict1 setValue:[estateDict valueForKey:@"value"]  forKey:@"value"];
        [estateDict1 setValue:[estateDict valueForKey:@"locality"] forKey:@"locality"];
        [estate_items_info addObject:estateDict1];
        
        
    }
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    [dataDict setValue:[estateDict valueForKey:@"estate_id"] forKey:@"estate_id"];
    [dataDict setValue:_estateValueTxtF.text forKey:@"value"];
    [dataDict setValue:_estateNameTxtF.text forKey:@"name"];
    [dataDict setValue:estate_items_info forKey:@"estate_items_info"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateEstate"];
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _myEstateTableView.contentInset = contentInsets;
    _myEstateTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _myEstateTableView.contentInset = contentInsets;
    _myEstateTableView.scrollIndicatorInsets = contentInsets;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.myEstateTableView];
    NSIndexPath * indexPath = [self.myEstateTableView indexPathForRowAtPoint:point];
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    
    //[piDict valueForKey:@"pi_info"]
    
    dataDict = [[estateItemsArray objectAtIndex:indexPath.row] mutableCopy];
    
    
    if(textField.tag ==100)
    {
        [dataDict setValue:textField.text forKeyPath:@"name" ];
    }
    if(textField.tag ==200)
    {
        [dataDict setValue:textField.text forKeyPath:@"value"];
        
    }
    if(textField.tag ==300)
    {
        [dataDict setValue:textField.text forKeyPath:@"locality"];
        
    }
    
    [estateItemsArray replaceObjectAtIndex:indexPath.row withObject:dataDict];
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tempTxtF = textField;
    
    
    if(textField == _estateValueTxtF || textField.tag == 200)
    {
        [self addToolBarForDoneButton:textField];
    }
    
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
    
    
}

-(void)addBanificiaryOnStateItems:(UIButton*)sender
{
    MyWillRecipientViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillRecipientViewController"];
    vc.benificiaryType = @"estate_items";
    vc.benificiaryDataDict = [estateItemsArray objectAtIndex:sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)addIndividualItemOnStateItem:(UIButton*)sender
{
    
    EstateIndividualItemViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EstateIndividualItemViewController"];
    //vc.benificiaryType = @"estate_items";
    vc.individualItemDict = [estateItemsArray objectAtIndex:sender.tag];
    vc.estateId = [estateDict valueForKey:@"estate_id"];
    vc.estateItemId = [[estateItemsArray objectAtIndex:sender.tag] valueForKey:@"estate_items_id"];
    [self.navigationController pushViewController:vc animated:YES];
    
}




-(void)addToolBarForDoneButton:(UITextField*)textF
{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    textF.inputAccessoryView = keyboardToolbar;
}

-(void)yourTextViewDoneButtonPressed
{
    [tempTxtF resignFirstResponder];
}




@end
