//
//  WillShowWithAllDetailsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WillShowWithAllDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@property(nonatomic,weak) IBOutlet UITableView * willDetailsTableView;

- (IBAction)backBtnAction:(id)sender;


@end
