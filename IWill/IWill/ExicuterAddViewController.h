//
//  ExicuterAddViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/15/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomeTextField.h"

@interface ExicuterAddViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;


@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;


@property (weak, nonatomic) IBOutlet UIButton *profilePicBtn;
- (IBAction)profilePicBtnAction:(id)sender;



@property (weak, nonatomic) IBOutlet CustomeTextField *nameTxt;
@property (weak, nonatomic) IBOutlet CustomeTextField *dobTxt;
@property (weak, nonatomic) IBOutlet CustomeTextField *ageTxt;
@property (weak, nonatomic) IBOutlet CustomeTextField *nameOfOldestChildTxt;
@property (weak, nonatomic) IBOutlet CustomeTextField *relationshipToTestatorText;
@property (weak, nonatomic) IBOutlet CustomeTextField *ssnLast4DigitText;
@property (weak, nonatomic) IBOutlet CustomeTextField *addressText;
@property (weak, nonatomic) IBOutlet CustomeTextField *knowBodyMarkText;

@property (weak, nonatomic) IBOutlet CustomeTextField *emailTextF;
@property (weak, nonatomic) IBOutlet CustomeTextField *mobileNumberTxtF;
@property (weak, nonatomic) IBOutlet CustomeTextField *lastName;

@property (weak, nonatomic) IBOutlet UIButton *authorizationBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
- (IBAction)enterBtnAction:(id)sender;
- (IBAction)authorizationBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *authorizationLinkBtn;

- (IBAction)authorizationLinkBtnAction:(id)sender;


@end
