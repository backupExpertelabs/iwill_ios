//
//  MyWillPersonalInformationViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWillPersonalInformationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *currentAddressTextView;
@property (weak, nonatomic) IBOutlet UITextView *formerAddress1;
@property (weak, nonatomic) IBOutlet UITextView *formerAddress2;

@end
