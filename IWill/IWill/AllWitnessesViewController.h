//
//  AllWitnessesViewController.h
//  IWill
//
//  Created by A1AUHAIG on 4/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllWitnessesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *witnessTable;
@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
@property (weak, nonatomic) IBOutlet UIButton *addNewWitness;

- (IBAction)enterBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;
- (IBAction)addNewWitnessBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addNewWitnessBtn;

@end
