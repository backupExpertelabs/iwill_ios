//
//  AddWitnessesViewController.h
//  IWill
//
//  Created by A1AUHAIG on 4/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface AddWitnessesViewController : UIViewController
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *firstNameText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lastNameText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *emailText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *addressText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dobText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneNumberText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *permanentMarkText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *socialSecurityNumberText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *relationshitToTestatorText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nameOfChieldText;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nameOfSpouseText;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
- (IBAction)enterBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *witnessBtnView;
@property (weak, nonatomic) IBOutlet UIButton *addOneWitBtn;
- (IBAction)addOneWitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addTwoWitBtn;
- (IBAction)addTwoWitBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *addThreeWitBtn;
- (IBAction)addThreeWitBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic)  NSString *witnessAdded;


@end
