//
//  ConnectionManager.m
//
//


#import "ConnectionManager.h"
#import "AppDelegate.h"
#import "Utils.h"

//#define kBASE_URL  @"http://devpixapi.inclusosol.com/"

#define kBASE_URL  @"http://iwill.digvijayjain.com/index.php/"

//#define kURL  @"http://axad.digvijayjain.com/"
#define kURL  @"http://iwill.digvijayjain.com/index.php/"

#define kRequestTimeOut    120


@implementation ConnectionManager

@synthesize delegate,sessionManager,currentTask;

-(BOOL)getDataFromServerWithSessionTastmanager:(NSString * )functionName withInput:(NSMutableDictionary*)aDict andDelegate:(id)inputDelegate andURL:(NSString *)urlString
{
    
    self.delegate = inputDelegate;
    urlString = [kURL stringByAppendingString:urlString];
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"json string %@",myString);
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[myString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; ////
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];////
    [request setHTTPBody:jsonData];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
            {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
                NSLog(@"Response = %@\n\n",json);
                NSString *responseeStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"Response String %@",responseeStr);
                [self.delegate performSelector:@selector(responseReceived:) withObject:json];
            }
        }
        
        else{
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
            {
                [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
            }
        }
        
        
        
    }] resume];
    
    return YES;
}


-(BOOL)getDataFromServerWithSessionTastmanagerWithGET:(NSString * )functionName withInput:(NSMutableDictionary*)aDict andDelegate:(id)inputDelegate andURL:(NSString *)urlString
{
    
    self.delegate = inputDelegate;
    urlString = [kURL stringByAppendingString:urlString];
    NSError * err;
    //    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    //    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //
    //    NSLog(@"json string %@",myString);
    //
    //    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[myString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    //    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; ////
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];////
    //    [request setHTTPBody:jsonData];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
            {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
                
                NSLog(@"Response = %@\n\n",json);
                
                NSString *responseeStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"Response String %@",responseeStr);
                
                
                [self.delegate performSelector:@selector(responseReceived:) withObject:json];
            }
        }
        
        else{
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
            {
                [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
            }
        }
        
        
        
    }] resume];
    
    return YES;
}







-(BOOL)getDataFromServerWithOtherResponseSessionTastmanager:(NSString * )functionName withInput:(NSMutableDictionary*)aDict andDelegate:(id)inputDelegate andURL:(NSString *)urlString
{
    
    self.delegate = inputDelegate;
    urlString = [kURL stringByAppendingString:urlString];
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"json string %@",myString);
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[myString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; ////
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];////
    [request setHTTPBody:jsonData];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceivedWithSecondMethod:)])
            {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
                
                NSLog(@"Response = %@\n\n",json);
                [self.delegate performSelector:@selector(responseReceivedWithSecondMethod:) withObject:json];
            }
        }
        
        else{
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
            {
                [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
            }
        }
        
        
        
    }] resume];
    
    return YES;
}



-(BOOL)getDataFromServerretrivePasswordTastmanager:(NSString * )functionName  andDelegate:(id)inputDelegate andURL:(NSString *)urlString
{
    urlString = [kURL stringByAppendingString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; ////
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];////
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
            {
                NSError* error;
                
                
                // NSString * jsonStr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                
                
                // NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                
                id json = [NSJSONSerialization JSONObjectWithData:data
                                                          options:0
                                                            error:&error];
                
                NSLog(@"Response = %@\n\n",json);
                [self.delegate performSelector:@selector(responseReceived:) withObject:json];
            }
        }
        
        else{
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
            {
                [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
            }
        }
        
        
        
    }] resume];
    
    return YES;
}






-(BOOL) getDataForFunction : (NSString *) functionName withInput: (NSMutableDictionary *) aDict withCurrentTask : (NSString *) inputTask andDelegate : (id)inputDelegate{
    NSLog(@"Input Parameters = %@ \n\n",aDict);
    
    self.delegate = inputDelegate;
    self.currentTask = inputTask;
    
    // functionName = [kURL stringByAppendingString:functionName];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Input Parameters json string = %@ \n\n",myString);
    
    
    NSURL *baseURL = [NSURL URLWithString:kURL];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:kRequestTimeOut];
    [manager POST:functionName parameters:aDict
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
         {
             NSLog(@"Response = %@\n\n",responseObject);
             [self.delegate performSelector:@selector(responseReceived:) withObject:responseObject];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"Error = %@\n\n",error);
         
         
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
         {
             [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
         }
         
     }];
    return YES;
}


-(BOOL) getDataForFunctionWithGET : (NSString *) functionName withInput: (NSMutableDictionary *) aDict withCurrentTask : (NSString *) inputTask andDelegate : (id)inputDelegate{
    NSLog(@"Input Parameters = %@ \n\n",aDict);
    
    self.delegate = inputDelegate;
    self.currentTask = inputTask;
    
    // functionName = [kURL stringByAppendingString:functionName];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Input Parameters json string = %@ \n\n",myString);
    
    
    NSURL *baseURL = [NSURL URLWithString:kURL];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:kRequestTimeOut];
    
    [manager GET:functionName parameters:aDict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
         {
             NSLog(@"Response = %@\n\n",responseObject);
             [self.delegate performSelector:@selector(responseReceived:) withObject:responseObject];
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"Error = %@\n\n",error);
         
         
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
         {
             [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
         }
         
     }];
    return YES;
}







-(BOOL) getDataForFunction:(NSString *)functionName withInput:(NSMutableDictionary *)aDict withCurrentTask:(CURRENT_TASK)inputTask Delegate:(id)inputDelegate andMultipartData:(NSData *)data
{
    
    self.delegate = inputDelegate;
    self.enCurrentTask = inputTask;
    
    AFHTTPSessionManager *manager = [self InitSetUpForWebService];
    [manager.requestSerializer setTimeoutInterval:kRequestTimeOut];
    [manager POST:functionName parameters:aDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if (data) {
             [formData appendPartWithFileData:data name:@"image" fileName:@"profileImage.jpg" mimeType:@"image/jpg"];
             
         }
     }
     
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"%@",responseObject);
         //[AppManager stopStatusbarActivityIndicator];
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
         {
             [self.delegate performSelector:@selector(responseReceived:) withObject:responseObject];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         //[AppManager stopStatusbarActivityIndicator];
         
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
         {
             [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
         }
         
     }];
    
    
    return YES;
}


-(BOOL)updateImage:(UIImage *)inImage supportingParametes:(NSMutableDictionary *)inDic andFunctionName:(NSString *)inFunctionName
{
    //    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:kBASE_URL]];
    //    NSData *imageData = UIImageJPEGRepresentation(inImage, 1.0);
    //
    //    AFHTTPRequestOperation *op = [manager POST:inFunctionName parameters:inDic constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    //        //do not put image inside parameters dictionary as I did, but append it!
    //        [formData appendPartWithFileData:imageData name:kprofileImage fileName:@"profileImage.jpg" mimeType:@"image/jpeg"];
    //
    //
    //    } success:^(AFHTTPRequestOperation *operation, id responseObject)
    //                                  {
    //                                      NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    //                                      NSLog(@"%@",responseObject);
    //                                      [AppManager stopStatusbarActivityIndicator];
    //                                      if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
    //                                      {
    //                                          [self.delegate performSelector:@selector(responseReceived:) withObject:responseObject];
    //                                      }
    //
    //
    //
    //                                  } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    //                                  {
    //                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    //                                      [AppManager stopStatusbarActivityIndicator];
    //
    //                                      if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
    //                                      {
    //                                          [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
    //                                      }
    //                                  }];
    //
    //
    //    [op start];
    
    return YES;
}

-(void)failureBlockCalled:(NSError *)error
{
    if ([self isRequestTimeOut:error])
    {
        //[Utils showAlertView:kAlertTitle message:kRequestTimeOutMessage delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        //    else
        //        [Utils showAlertView:kAlertTitle message:kAlertServiceFailed delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
    }
}
#pragma mark
#pragma Check Request Timeout---

-(BOOL)isRequestTimeOut:(NSError *)error
{
    BOOL isTimeOut = NO;
    NSString *str = @"NSLocalizedDescription=The request timed out.";
    
    if ([error.description rangeOfString:str].location!=NSNotFound)
    {
        isTimeOut=YES;
    }
    return isTimeOut;
}
-(BOOL) getDataForFunction:(NSString *)functionName withInput:(NSMutableDictionary *)aDict withCurrentTask:(CURRENT_TASK)inputTask Delegate:(id)inputDelegate andMultipartData:(NSData *)data withMediaKey:(NSString *)imageKey
{
    
    self.delegate = inputDelegate;
    //    self.currentTask = inputTask;
    
    NSURL *baseURL = [NSURL URLWithString:kBASE_URL];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Input Parameters json string = %@ \n\n",myString);
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setTimeoutInterval:60];
    [manager POST:functionName parameters:aDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if (data)
         {
             
             if([[aDict valueForKey:@"mediaType"] isEqualToString:@"2"] )
             {
                 
                 [formData appendPartWithFileData:data name:imageKey fileName:@"video.mov" mimeType:@"video/quicktime"];
             }
             else if([[aDict valueForKey:@"mediaType"] isEqualToString:@"1"] )
                 
             {
                 [formData appendPartWithFileData:data name:imageKey fileName:@"profileImage.jpg" mimeType:@"image/jpg"];
             }
             
             else if([[aDict valueForKey:@"mediaType"] isEqualToString:@"3"] )
             {
                 [formData appendPartWithFileData:data name:imageKey fileName:@"audio.mp3" mimeType:@"audio/mp3"];
             }
             
             else{
                 [formData appendPartWithFileData:data name:imageKey fileName:@"profileImage.jpg" mimeType:@"image/jpg"];
             }
         }
     }
     
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
         {
             [self.delegate performSelector:@selector(responseReceived:) withObject:responseObject];
         }
     }
          failure:^(NSURLSessionDataTask* task, NSError *error)
     {
         
         
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
         {
             [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
         }
         
     }];
    
    
    return YES;
}



-(BOOL) getDataForFunction:(NSString *)functionName withInput:(NSMutableDictionary *)aDict withCurrentTask:(CURRENT_TASK)inputTask Delegate:(id)inputDelegate andMultipartData:(NSData *)data withMediaKey:(NSString *)imageKey andMineType:(NSString*)mineType andFileName:(NSString*)fileName
{
    
    NSURL *baseURL = [NSURL URLWithString:kURL];
    
    self.delegate = inputDelegate;
    //    self.currentTask = inputTask;
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request = [requestSerializer multipartFormRequestWithMethod:@"POST" URLString:[kURL stringByAppendingString:functionName] parameters:aDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:imageKey fileName:fileName mimeType:mineType];
        
    } error:nil];
    [request setTimeoutInterval:20000];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES; // not recommended for production
    manager.securityPolicy.validatesDomainName = NO;
    [requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"content-type"];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      
                      // This is not called back on the main queue.
                      
                      // You are responsible for dispatching to the main queue for UI updates
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          //Update the progress view
                          
                          NSLog(@"Running….");
                          
                      });
                      
                  }
                  
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
                          {
                              [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
                          }
                          
                          
                      } else {
                          NSString *responseMessage = [NSString stringWithUTF8String:[responseObject bytes]];
                          NSLog(@"responseMessage: %@", responseMessage);
                          NSDictionary *dicData = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                          if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
                          {
                              [self.delegate performSelector:@selector(responseReceived:) withObject:dicData];
                          }
                          
                      }
                      
                  }];
    
    
    
    [uploadTask resume];
    return YES;
}



//    //NSURL *baseURL = [NSURL URLWithString:kURL];
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];
//
//     AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
//    [manager.requestSerializer setTimeoutInterval:60];
//   // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
//
//    [serializer setValue:@"application/x-www-form-urlencoded; charset=UTF8" forHTTPHeaderField:@"Content-Type"];
//
//    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//
//   // manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//
//
//    //[serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    //[serializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//
//
//
//    [manager POST:functionName parameters:aDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//     {
//         if (data)
//         {
//
//             [formData appendPartWithFileData:data name:imageKey fileName:fileName mimeType:mineType];
//         }
//     }
//
//          success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//
//         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseReceived:)])
//         {
//             [self.delegate performSelector:@selector(responseReceived:) withObject:responseObject];
//         }
//     }
//          failure:^(NSURLSessionDataTask* task, NSError *error)
//     {
//
//
//         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
//         {
//             [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
//         }
//
//     }];
//





//[formData appendPartWithFileData:data name:imageKey fileName:fileName mimeType:mineType];

// AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];

//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithBaseURL:baseURL];
//
//    [manager POST:functionName parameters:aDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileData:data
//                                    name:imageKey
//                                fileName:fileName mimeType:mineType];
//
//
//
//
//        // etc.
//    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Response: %@", responseObject);
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
//
//
//    return YES;
//}


-(AFHTTPSessionManager *)InitSetUpForWebService
{
    NSURL *baseURL = [NSURL URLWithString:kBASE_URL];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setTimeoutInterval:60];
    
    return manager;
}




-(BOOL) getDataForCart : (NSString *) functionName withInput: (NSMutableDictionary *) aDict withCurrentTask : (NSString *) inputTask andDelegate : (id)inputDelegate{
    NSLog(@"Input Parameters = %@ \n\n",aDict);
    
    self.delegate = inputDelegate;
    self.currentTask = inputTask;
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:aDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Input Parameters json string = %@ \n\n",myString);
    
    
    NSURL *baseURL = [NSURL URLWithString:kBASE_URL];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:configuration];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:kRequestTimeOut];
    [manager POST:functionName parameters:aDict
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseCart:)])
         {
             NSLog(@"Response = %@\n\n",responseObject);
             [self.delegate performSelector:@selector(responseCart:) withObject:responseObject];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"Error = %@\n\n",error);
         
         
         if(self.delegate != nil && [self.delegate respondsToSelector:@selector(didFailWithError:)])
         {
             [self.delegate performSelector:@selector(didFailWithError:) withObject:error];
         }
         
     }];
    return YES;
}

@end
