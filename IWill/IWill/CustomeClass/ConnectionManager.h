//
//  ConnectionManager.h
//  
//



#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef enum
{
    eCurrentTaskNone = 0,
    //    TASK_TO_GET_LIST
}CURRENT_TASK;

@protocol ConnectionManager_Delegate <NSObject>

@optional
-(void) didFailWithError:(NSError *)error;
-(void) responseReceived:(id)responseObject;
-(void) responseReceivedWithSecondMethod:(id)responseObject;
-(void) responseCart:(id)responseObject;


@end

@interface ConnectionManager : NSObject

@property(nonatomic,strong) AFHTTPSessionManager *sessionManager;
@property(nonatomic,weak) id<ConnectionManager_Delegate> delegate;
@property(nonatomic,strong) NSString *currentTask;
@property(nonatomic, assign) CURRENT_TASK enCurrentTask;

-(BOOL)getDataFromServerretrivePasswordTastmanager:(NSString * )functionName  andDelegate:(id)inputDelegate andURL:(NSString *)urlString;



-(BOOL)getDataFromServerWithSessionTastmanager:(NSString * )functionName withInput:(NSMutableDictionary*)aDict andDelegate:(id)inputDelegate andURL:(NSString *)urlString;

-(BOOL)getDataFromServerWithSessionTastmanagerWithGET:(NSString * )functionName withInput:(NSMutableDictionary*)aDict andDelegate:(id)inputDelegate andURL:(NSString *)urlString;

-(BOOL) getDataForFunction : (NSString *) functionName withInput: (NSMutableDictionary *) aDict withCurrentTask : (NSString *) inputTask andDelegate : (id)inputDelegate;

-(BOOL) getDataForFunctionWithGET : (NSString *) functionName withInput: (NSMutableDictionary *) aDict withCurrentTask : (NSString *) inputTask andDelegate : (id)inputDelegate;







-(BOOL) getDataForFunction:(NSString *)functionName withInput:(NSMutableDictionary *)aDict withCurrentTask:(CURRENT_TASK )inputTask Delegate:(id)inputDelegate andMultipartData:(NSData *)data;

-(BOOL)updateImage:(UIImage *)inImage supportingParametes:(NSMutableDictionary *)inDic andFunctionName:(NSString *)inFunctionName;

-(BOOL) getDataForFunction:(NSString *)functionName withInput:(NSMutableDictionary *)aDict withCurrentTask:(CURRENT_TASK)inputTask Delegate:(id)inputDelegate andMultipartData:(NSData *)data withMediaKey:(NSString *)imageKey;
-(void)failureBlockCalled:(NSError *)error;
-(BOOL)isRequestTimeOut:(NSError *)error;

-(BOOL) getDataForFunction:(NSString *)functionName withInput:(NSMutableDictionary *)aDict withCurrentTask:(CURRENT_TASK)inputTask Delegate:(id)inputDelegate andMultipartData:(NSData *)data withMediaKey:(NSString *)imageKey andMineType:(NSString*)mineType andFileName:(NSString*)fileName;


-(BOOL) getDataForCart : (NSString *) functionName withInput: (NSMutableDictionary *) aDict withCurrentTask : (NSString *) inputTask andDelegate : (id)inputDelegate;

-(BOOL)getDataFromServerWithOtherResponseSessionTastmanager:(NSString * )functionName withInput:(NSMutableDictionary*)aDict andDelegate:(id)inputDelegate andURL:(NSString *)urlString;

@end
