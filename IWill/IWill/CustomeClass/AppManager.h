//
//  AppManager.h
//  Helius
//
//  Created by Phoenix on 08/08/16.
//  Copyright © 2016 Incluso Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVFloatLabeledTextField.h"

@interface AppManager : NSObject
+ (id)sharedManager;

+(void)startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:(BOOL)status;
+(void)stopStatusbarActivityIndicator;
+(BOOL)checkEmailValidation:(NSString *)email;

+(JVFloatLabeledTextField*)setBottumBorder:(JVFloatLabeledTextField*)textField;




@end
