
//
//  Utils.m
//  Pix
//

#import "Utils.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "JVFloatLabeledTextField.h"

@implementation Utils
    
    
#pragma mark -
    //----- show a alert massage
+ (void) showAlertView :(NSString*)title message:(NSString*)msg delegate:(id)delegate
      cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
    {
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate
                               
                               
                                               cancelButtonTitle:CbtnTitle otherButtonTitles:otherBtnTitles, nil];
        
        
        
        
        [alert1 show];
        //    [alert release];
    }
    
    
+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate
            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:CbtnTitle otherButtonTitles:otherBtnTitles, nil];
        alert.tag = tag;
        alert.tintColor =  [UIColor colorWithRed:255.0/255.0 green:143.0/255.0 blue:56.0/255.0 alpha:1];
        [alert show];
        //    [alert release];
    }
    
#pragma mark - validate phone
+(BOOL)validatePhone:(NSString *)phoneNumber
    {
        NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
        NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
        
        return [phoneTest evaluateWithObject:phoneNumber];
    }
#pragma mark - Activity Indicator
+(void) startActivityIndicatorInView:(UIView*)aView withMessage:(NSString*)aMessage
    {
        MBProgressHUD *_hud = [MBProgressHUD showHUDAddedTo:aView animated:YES];
        _hud.dimBackground  = YES;
        _hud.labelText      = aMessage;
    }
    
+(void) stopActivityIndicatorInView:(UIView*)aView
    {
        [MBProgressHUD hideHUDForView:aView animated:YES];
    }
#pragma Mark Validate Email
+(BOOL)validateEmail:(NSString*)email
    {
        //    BOOL isValidEmail =  NO;
        
        //    NSString *regex1 = @"\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z";
        //    NSString *regex2 = @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*";
        //    NSPredicate *test1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
        //    NSPredicate *test2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
        
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [emailTest evaluateWithObject:email];
        
        //    if ([test1 evaluateWithObject:email] && [test2 evaluateWithObject:email])
        //        isValidEmail =YES;
        //
        //    return isValidEmail;
    }
+(void)CustomePlaceHolder:(UITextField*) textField  withPlaceholder:(NSString *)PlaceHolder
    {
        UIColor *color=[UIColor colorWithRed:222.0f/255.0f green:222.0f/255.0f blue:222.0f/255.0f alpha:1.0];
        UIFont *font=[UIFont fontWithName:@"Myriad pro" size:15.0];
        
        
        textField.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:PlaceHolder
                                        attributes:@{
                                                     NSForegroundColorAttributeName: color,
                                                     NSFontAttributeName : font
                                                     }
         ];
        
        
        
    }
    
    
#pragma mark- Network reachabiliyt
+(BOOL)isInternetAvailable
    {
        BOOL isInternetAvailable = false;
        Reachability *internetReach = [Reachability reachabilityForInternetConnection];
        [internetReach startNotifier];
        NetworkStatus netStatus = [internetReach currentReachabilityStatus];
        switch (netStatus)
        {
            case NotReachable:
            isInternetAvailable = FALSE;
            break;
            case ReachableViaWWAN:
            isInternetAvailable = TRUE;
            break;
            case ReachableViaWiFi:
            isInternetAvailable = TRUE;
            break;
        }
        [internetReach  stopNotifier];
        return isInternetAvailable;
    }
    
    
+ (NSString *) getDateFromUnixFormat:(NSString *)unixFormat
    {
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[unixFormat intValue]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        //NSDate *date = [dateFormatter dateFromString:publicationDate];
        NSString *dte=[dateFormatter stringFromDate:date];
        return dte;
        
    }


+ (NSString *) getDateFromDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //NSDate *date = [dateFormatter dateFromString:publicationDate];
    NSString *dte=[dateFormatter stringFromDate:date];
    return dte;
    
}


+(NSString*) getDateFromDateForAPI:(NSDate*)date

{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    //NSDate *date = [dateFormatter dateFromString:publicationDate];
    NSString *dte=[dateFormatter stringFromDate:date];
    return dte;
}




+ (NSString *)currentTimeStamp {
    return [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
}
    
+(void)sessionExpireAndGotoLogin
    {
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"isLogout"];
        
    }
    
+(void)openRedirectPage:(NSDictionary *)dataDict
    {
        NSString * redirectType = [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"redirectType"]];
        if([redirectType isEqualToString:@"1"])
        {
            NSString * urlMail  = @"mailto:sb@sw.com?subject=title&body=content";
            NSString *url = [urlMail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
            [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://"]];
        }
        
        if([redirectType isEqualToString:@"2"])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
        }
        if([redirectType isEqualToString:@"3"])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:12125551212"]];
        }
        if([redirectType isEqualToString:@"4"])
        {
            NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
        }
        
       
        
    }
    
    
+(UIView*)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
    
    return view;
}



+(UIButton*)buttonWithSaddowAndradius:(UIButton*)sender
{
    sender.layer.cornerRadius = 2;
    sender.layer.masksToBounds = NO;
    sender.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    sender.layer.shadowOpacity = 0.3;
    sender.layer.shadowRadius = 4;
    sender.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    return sender;
}


+ (NSString *)encodeToBase64String:(UIImage *)image
{
//    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    return [UIImageJPEGRepresentation(image, 0.25) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
}
+(UITextField*)setBottumBorder:(UITextField*)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    return textField;
}

+(JVFloatLabeledTextField*)setBottumBorderOnJVFloatLabeledTextField:(JVFloatLabeledTextField*)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    return textField;
}


+(UITextView*)setBottumBorderOnTextView:(UITextView*)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    return textField;
}

+(UIColor*)getBorderLineColor
{
    
    return [UIColor colorWithRed:85/255.f green:47/255.f blue:139/255.f alpha:1];
}

+(JVFloatLabeledTextField*)setTextFieldBottumBorder:(JVFloatLabeledTextField*)textField andController:(UIViewController*)vc
{
    
    CGFloat borderWidth = 0.5;
//    textField.layer.borderColor = [UIColor whiteColor].CGColor;
//    textField.layer.borderWidth = borderWidth;
//    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
//    textField.layer.cornerRadius = 3;
//    textField.layer.masksToBounds = YES;
//
//
//
//
    
    float xAsix = textField.frame.origin.x;
    
    
    
    CALayer *border = [CALayer layer];
   // border.borderColor = [UIColor whiteColor].CGColor;
    border.borderColor = [UIColor colorWithRed:85/255.f green:47/255.f blue:139/255.f alpha:1].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, vc.view.frame.size.width-(xAsix+xAsix), textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
    
    return textField;
}

    @end
