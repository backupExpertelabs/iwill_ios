//
//  CitySugectionDropDown.m
//  BankMix
//
//  Created by A1AUHAIG on 9/5/17.
//  Copyright © 2017 Yatharth Singh. All rights reserved.
//

#import "CitySugectionDropDown.h"

@implementation CitySugectionDropDown

-(void)addTableViewOnView:(NSMutableArray*)dataArray andFrame:(CGRect)frame andType:(NSString *)type
{
    self.frame = frame;
    typeStr = type;
    tableDataArray = dataArray;
//    if(typeStr.length>0 && [typeStr isEqualToString:@"zip"])
//    {
      cityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    }
//    else
//    {
//       cityTableView = [[UITableView alloc] initWithFrame:self.frame];
//    }
    //cityTableView = [[UITableView alloc] initWithFrame:self.frame];
    [cityTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"newCell"];
    cityTableView.delegate = self;
    cityTableView.dataSource = self;
    cityTableView.layer.borderWidth = 1;
    cityTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
   // self.backgroundColor = [UIColor redColor];
   // cityTableView.backgroundColor = [UIColor greenColor];
    [self addSubview:cityTableView];
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"newCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    
    cell.textLabel.text = [tableDataArray objectAtIndex:indexPath.row];
//    if(typeStr.length>0 && [typeStr isEqualToString:@"zip"])
//    {
//        cell.textLabel.text = [tableDataArray objectAtIndex:indexPath.row];
// 
//    }
//    else
//    {
//        cell.textLabel.text = [[tableDataArray objectAtIndex:indexPath.row] valueForKey:@"name"];
// 
//    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(typeStr.length>0 && [typeStr isEqualToString:@"zip"])
//    {
//       [_delegate tableviewSelectedRow:[tableDataArray objectAtIndex:indexPath.row]];
//        
//    }
//    else
//    {
//       [_delegate tableviewSelectedRow:[[tableDataArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
//        
//    }
    
    [_delegate tableviewSelectedRow:[tableDataArray objectAtIndex:indexPath.row]];

   // [_delegate tableviewSelectedRow:[[tableDataArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
}

@end
