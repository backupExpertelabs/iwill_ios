//
//  AlertConstant.h
//  Pix
//
//  Created by A1AUHAIG on 3/30/17.
//  Copyright © 2017 Incluso Solutions. All rights reserved.
//

#ifndef AlertConstant_h
#define AlertConstant_h



#define kAlertCheckInternetConnection           @"Sorry,No Internet, please try again !"

#define kAlertTitle                             @"Prizpix"
#define kAlertBtnOK                             @"OK"
#define kAlertBtnCancel                         @"Cancel"
#define kAlertBtnYes                            @"YES"
#define kAlertBtnNo                             @"No"
#define kAlertServiceFailed                     @"Failed. Try again"
#define kAlertError                             @"Error"
#define kAlertTextFieldNil                      @"Please fill mandatory fields"
#define kAlertEmailNotValid                     @"Please fill correct email"
#define kPlayerFileNotFound                     @"File Not Found"
#define kErrorFromServer                        @""
#define kIssuesReportAudioFile                  @"Please hold to record !"




#define kAlertEnterEmail              @"Please enter your email!"
#define kAlertInvalidEmail            @"Please enter valid email!"
#define kAlertEnterPassword           @"Please enter your password!"
#define kAlertEnterConfirmPassword     @"Please confirm password!"
#define kAlertPasswordNotMatch       @"Your passwords do not match."
#define kAlertPasswordMinSize      @"Password should be 6 characters"
#define kAlertEnterFirstName       @"Please enter first name"
#define kAlertEnterLastName    @"Please enter last name"
#define kAlertEnterAddress1     @"Please enter address"
#define kAlertEnterAddress2     @"Please enter address2"
#define kAlertEnterCity          @"Please enter city"
#define kAlertEnterState        @"Please enter state/province."
#define kAlertEnterPostalCode   @"Please enter postal code/Zip"
#define kAlertEnterCountry  @"Please select country"
#define kAlertProfileImage  @"Please select profile image"
#define kAlertEnterSecurityPin  @"Please enter your security pin"
#define kAlertEnterSecurityVerifyPin  @"Please verify security pin."
#define kAlertEnterSecurityVerifyPinMinSize      @"Please enter 6 digit security pin"
#define kAlertEnterSecondaryEmail       @"Please enter your secondary email"
#define kAlertEnterSecurityQuestion1    @"Please select your security question 1"
#define kAlertEnterSecurityQuestion2      @"Please select your security question 2"
#define kAlertEnterSecurityQuestion1Answer @"Please enter your security answer 1"
#define kAlertEnterSecurityQuestion2Answer  @"Please enter your security answer2 "
#define kAlertSecurityPinNotMatch       @"Pin mismatch"
#define kAlertExceedLimit     @"Sorry you have exceeded your limit. Please purchase plan"
#define kAlertSelectSubscriptionPlan    @"Please select subscription plan"
#define kAlertCardHolderName  @"Please enter card holder’s name"
#define kAlertCardNumber   @"Please enter card number"
#define kAlertCardCVVNumber  @"Please enter card CVV number"
#define kAlertCardExpiryMonth      @"Please enter card expiry month"
#define kAlertCardExpiryYear      @"Please enter card expiry year"






#endif /* AlertConstant_h */
