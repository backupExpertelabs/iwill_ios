//
//  AppManager.m
//  Helius
//
//  Created by Phoenix on 08/08/16.
//  Copyright © 2016 Incluso Solutions. All rights reserved.
//


#import "AppManager.h"
#import "JVFloatLabeledTextField.h"

@implementation AppManager

+ (id)sharedManager {
    static AppManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


+(void)startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:(BOOL)status

{
    
}
+(void)stopStatusbarActivityIndicator
{
    
}

+(BOOL)checkEmailValidation:(NSString *)email
{
    NSString *regex1 = @"\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z";
    NSString *regex2 = @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*";
    NSPredicate *test1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    NSPredicate *test2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
    
    if ([test1 evaluateWithObject:email] && [test2 evaluateWithObject:email])
    {
        return YES;
    }
    return NO;
    
}

+(JVFloatLabeledTextField*)setBottumBorder:(JVFloatLabeledTextField*)textField
{
    
    CGFloat borderWidth = 0.5;
    textField.layer.borderColor = [UIColor whiteColor].CGColor;
    textField.layer.borderWidth = borderWidth;
    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    textField.layer.cornerRadius = 3;
    textField.layer.masksToBounds = YES;
    return textField;
}


@end
