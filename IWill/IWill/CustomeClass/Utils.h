//
//  Utils.h
//  Pix
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"


@interface Utils : NSObject
    
    
+ (void) showAlertView :(NSString*)title message:(NSString*)msg delegate:(id)delegate
      cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;
    
    //+ (void) showAlertView :(NSString*)title message:(NSString*)msg delegate:(id)delegate cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;
+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate
            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;
+(BOOL)validatePhone:(NSString *)phoneNumber;
    
+(void) startActivityIndicatorInView:(UIView*)aView withMessage:(NSString*)aMessage;
+(void) stopActivityIndicatorInView:(UIView*)aView;
    
+(BOOL)isInternetAvailable;
+(BOOL)validateEmail:(NSString*)email;
+(NSString *) getDateFromUnixFormat:(NSString *)unixFormat;
    
+(void)sessionExpireAndGotoLogin;
+(void)openRedirectPage:(NSString *)redirectType andRedirectValue:(NSString*)redirectValue;
+(void)openRedirectPage:(NSDictionary *)dataDict;
+ (NSString *)currentTimeStamp;
+(NSArray*)getTotalViewBanner;
+(NSArray*)getCurrentBannerForAds;

+(UIView*)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners;
+ (NSString *) getDateFromDate:(NSDate *)date;

+(UIButton*)buttonWithSaddowAndradius:(UIButton*)sender;

+ (NSString *)encodeToBase64String:(UIImage *)image;

+(NSString*) getDateFromDateForAPI:(NSDate*)date;
+(UITextField*)setBottumBorder:(UITextField*)textField;
+(UITextView*)setBottumBorderOnTextView:(UITextView*)textField;
+(JVFloatLabeledTextField*)setBottumBorderOnJVFloatLabeledTextField:(JVFloatLabeledTextField*)textField;

+(UIColor*)getBorderLineColor;
//+(JVFloatLabeledTextField*)setTextFieldBottumBorder:(JVFloatLabeledTextField*)textField;

+(JVFloatLabeledTextField*)setTextFieldBottumBorder:(JVFloatLabeledTextField*)textField andController:(UIViewController*)vc;

    @end
