//
//  CitySugectionDropDown.h
//  BankMix
//
//  Created by A1AUHAIG on 9/5/17.
//  Copyright © 2017 Yatharth Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol citySuggetionDelegate

@required
-(void)tableviewSelectedRow:(NSString*)name;

@end
@interface CitySugectionDropDown : UIView<UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray * tableDataArray;
    UITableView * cityTableView;
     NSString * typeStr;
}

@property (nonatomic,weak) id<citySuggetionDelegate> delegate;

///-(void)addTableViewOnView:(NSMutableArray*)dataArray andFrame:(CGRect)frame;
-(void)addTableViewOnView:(NSMutableArray*)dataArray andFrame:(CGRect)frame andType:(NSString*)type;
@end
