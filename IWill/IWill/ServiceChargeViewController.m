//
//  ServiceChargeViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ServiceChargeViewController.h"
#import "ConnectionManager.h"
#import "Constant.h"
#import "Utils.h"
#import "ServiceChargeDataModel.h"
#import "WitnessTableViewCell.h"

@interface ServiceChargeViewController ()<ConnectionManager_Delegate>
{
    ConnectionManager * connectionManager;
    NSMutableArray * dataListArray;
}

@end

@implementation ServiceChargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    [self hitApiTogetAllServiceList];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hitApiTogetAllServiceList
{
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    NSLog(@"hdsghj %@",dataDict);
    
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"getServicesChagres"];
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:alertTitle message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    });
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        ServiceChargeDataModel * dataoooo = [[ServiceChargeDataModel alloc] init];
        dataoooo = [dataoooo initWithDataDict:aResponceDic];
        if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==1) {
            
            dataListArray = [dataoooo valueForKey:@"result"];
            [_serviceChargeTable reloadData];
            
            }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==1) {
            UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return dataListArray.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    WitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[WitnessTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    cell.backView.layer.cornerRadius = 2;
    cell.backView.layer.masksToBounds = NO;
    cell.backView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    cell.backView.layer.shadowOpacity = 0.3;
    cell.backView.layer.shadowRadius = 4;
    cell.backView.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    ServiceChargeResultModel * dataDict = [dataListArray objectAtIndex:indexPath.row];
    
    cell.firstNameText.text = [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"service_type"]];
    cell.lastNameText.text = [dataDict valueForKey:@"charge"];
    cell.deleteWitnessBtn.tag = indexPath.row;
    
    [cell.deleteWitnessBtn addTarget:self action:@selector(buySubscription:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
}



-(void)buySubscription:(UIButton*)sender
{
    
    int tag = sender.tag;
    
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    
    ServiceChargeResultModel * serviceChargeDict = [dataListArray objectAtIndex:tag];
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];

     [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
     [dataDict setValue:[serviceChargeDict valueForKey:@"id"] forKey:@"serviceCharges_id"];
     [dataDict setValue:[serviceChargeDict valueForKey:@"service_type"] forKey:@"service_type"];
     [dataDict setValue:[serviceChargeDict valueForKey:@"charge"] forKey:@"amount"];
    [dataDict setValue:@"secdrt123" forKey:@"transaction_id"];
    
    NSLog(@"hdsghj %@",dataDict);
    
    [connectionManager getDataFromServerWithOtherResponseSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"services"];
}

//Url : http://iwill.digvijayjain.com/index.php/services
//
//API NAME : services
//
//REQUEST PARAMETER :
//
//{
//
//    "testator_id" : "6",
//
//    "serviceCharges_id" : "2",
//
//    "service_type" : "yearly",
//
//    "amount" : "29.00$",
//
//    "transaction_id" : "KUTtIGref83211"
//
//}
//
//





//URL:-   http://iwill.digvijayjain.com/index.php/getServicesChagres
//
//REQUEST PARAMETER :
//{
//}
//RESPONSE :
//{
//    "isSuccess": true, "message": "Result display successfully", "Result": [ { "id": "1", "service_type": "monthly", "charge": "2.99$", "date": "2018-05-23" }, { "id": "2", "service_type": "yearly", "charge": "29.00$", "date": "2018-05-23" }
//                                                                            ] }
//

- (IBAction)skipBtnAction:(id)sender
{
    
}
- (IBAction)backBtnAction:(id)sender {
    
}
@end
