//
//  AllWitnessesViewController.m
//  IWill
//
//  Created by A1AUHAIG on 4/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AllWitnessesViewController.h"
#import "WitnessTableViewCell.h"
#import "WitnessDetailsViewController.h"
#import "AddWitnessesViewController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "Constant.h"

@interface AllWitnessesViewController ()<ConnectionManager_Delegate,UIAlertViewDelegate>

{
    ConnectionManager * connectionManager;
    NSMutableArray * allWitnessArray;
    
    NSString * witnessId;
    
    
}
@end

@implementation AllWitnessesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    allWitnessArray = [[NSMutableArray alloc] init];
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    [self callAPIForAllWitness];
    
    _addNewWitness.layer.cornerRadius =3;
    _enterBtn.layer.cornerRadius  =3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (IBAction)enterBtnAction:(id)sender
{
    
}
- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return allWitnessArray.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    WitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[WitnessTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary * dataDict = [allWitnessArray objectAtIndex:indexPath.row];
    
    cell.witnessName.text = [NSString stringWithFormat:@"%@ %@",[dataDict valueForKey:@"first_name"],[dataDict valueForKey:@"last_name"]];
    cell.relationshipNa.text = [dataDict valueForKey:@"reletionship"];
    cell.executerType.text = [dataDict valueForKey:@"email"];

    cell.deleteWitnessBtn.tag = indexPath.row;
    
    [cell.deleteWitnessBtn addTarget:self action:@selector(deleteWitnessAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    WitnessDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WitnessDetailsViewController"];
    
    vc.dataDict = [allWitnessArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)initViewWthLayer

{
    self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
}

-(void)callAPIForAllWitness
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"getWitness"];
    
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}


-(void)responseReceivedWithSecondMethod:(id)responseObject {
   
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            allWitnessArray = [aResponceDic valueForKey:@"Result"];
            [_witnessTable reloadData];
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });


}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            allWitnessArray = [aResponceDic valueForKey:@"Result"];
            [_witnessTable reloadData];
            
            if(allWitnessArray.count ==3)
            {
                _enterBtn.hidden =YES;
            }
            else
            {
                _enterBtn.hidden =NO;
            }
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
//            [Utils showAlertView:alertTitle message:@"You have no witness. Please add atleast two witness." delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:witnessCountZeroORNowitnessAdded delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
            
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddWitnessesViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    if(alertView.tag == 200 && buttonIndex ==1)
    {
        [self deleteWitnessAPI];
    }
    
}


-(void)deleteWitnessAction:(UIButton*)sender
{
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSString * witnessCount = [userData valueForKey:@"countWitnesses"];
    
    if(witnessCount.intValue < 3 )
    {
        
    }
    
    
    else{
        NSDictionary* dataDict = [allWitnessArray objectAtIndex:sender.tag];
        witnessId = [dataDict valueForKey:@"witness_id"];
        [Utils showAlertViewWithTag:200 title:alertTitle message:witnessDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
    }
}


-(void)deleteWitnessAPI
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"witness_id"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithOtherResponseSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"deleteWitness"];
    

}

- (IBAction)addNewWitnessBtnAction:(id)sender
{
    //AddWitnessesViewController
    AddWitnessesViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddWitnessesViewController"];
    
    vc.witnessAdded = @"Yes";
    //vc.dataDict = [allWitnessArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


@end
