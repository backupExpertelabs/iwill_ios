//
//  MyWillViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillViewController.h"
#import "Utils.h"
#import <AVFoundation/AVFoundation.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MyWillViewController ()

{
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
}

@property (nonatomic, strong) IBOutlet AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@end

@implementation MyWillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    visiblePopTipViews = [[NSMutableArray alloc] init];
    [self initViewWthLayer];
//    [self addDescriptionView];
//    [self PlayWillVideo];
    // Do any additional setup after loading the view.
    
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WillCreateDescriptionViewController"];
    
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    
    
}


-(void)addDescriptionView
{
    _willDetailsDescriptionText = [[UITextView alloc]initWithFrame:CGRectMake(0, 30, self.view.frame.size.width-10, (_willCreateDetailsShowView.frame.size.height)/2)];
    [_willDetailsDescriptionText setText:@"jkdsgfhsddjsfhjsdgfjhdsgfjhdsgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjsdhgfjhsdgfjhsdhgfsdhfgsdfhsdhfsdhfhsdhfsdhfhsdfsdfsdfsdf\nsdfsdfsdfsdfdsfsd\ndsfsdfdsf\ndsfsdfds\nsdfdsfds\nsdfsdfs\nsdfds\nsdfdsf\nsdfdsf\nsdfdsf\nsdfds\nsdfds\nueiiuwe\n\n\n\n\n\n\nreeret\n\n\n\n\n\ttertertt\n\n\n\n\reerterteretr\n\n\n\n\terterter\n\n\n\nretertreter"];
    [_willCreateDetailsShowView addSubview:_willDetailsDescriptionText];
    
    
    self.willViewShowView = [[UIView alloc]initWithFrame:CGRectMake(0, (_willCreateDetailsShowView.frame.size.height/2)+30, self.view.frame.size.width-10, ((_willCreateDetailsShowView.frame.size.height)/2)-50)];
    [_willCreateDetailsShowView addSubview:self.willViewShowView];
    _willViewShowView.layer.borderWidth = 2;
    _willViewShowView.layer.borderColor = [UIColor blackColor].CGColor;
    _willDetailsDescriptionText.layer.borderWidth = 2;
    _willDetailsDescriptionText.layer.borderColor = [UIColor blackColor].CGColor;
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initViewWthLayer


{
    
    self.simpleInfoBtn = [Utils buttonWithSaddowAndradius:self.simpleInfoBtn];
    self.jointInfoBtn = [Utils buttonWithSaddowAndradius:self.jointInfoBtn];
    self.testamentaryInfoBtn = [Utils buttonWithSaddowAndradius:self.testamentaryInfoBtn];
    
    //    self.profileInfoBtn.layer.cornerRadius= 75;
    //    self.profileInfoBtn.alpha = 0.7;
    //self.profileInfoBtn.opaque = YES;
    
    self.simpleInfoBtn.layer.cornerRadius = 75;
    self.jointInfoBtn.layer.cornerRadius= 75;
    self.testamentaryInfoBtn.layer.cornerRadius= 75;
    

    
    _myProfileInfoBtn.layer.cornerRadius =3;
    _executerInfoBtn.layer.cornerRadius =3;
    _addChildBtn.layer.cornerRadius =3;
    _myTrusteeBtn.layer.cornerRadius =3;
    _myPersonalItemsBtn.layer.cornerRadius =3;
    _myEstateBtn.layer.cornerRadius =3;
    _minorChildBtn.layer.cornerRadius =3;
    _funeralBtn.layer.cornerRadius =3;

    
}

-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)simpleInfoBtnAction:(id)sender

{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SimpleWillInformationListViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)jointInfoBtnAction:(id)sender
{
    
}
-(IBAction)testamentaryInfoBtnAction:(id)sender
{
    
}
- (IBAction)myProfileInfoBtnAction:(id)sender
{
    //ExecuterListViewController
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)executerInfoBtnAction:(id)sender
{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecuterListViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)myTrusteeBtnAction:(id)sender
{
   //MyWillPersonalInfoViewController.h
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillTrusteeListViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    //MyWillTrusteeListViewController
    
}

- (IBAction)myPersonalItemsBtnAction:(id)sender
{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillPersonalItemsViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (IBAction)myEstateBtnAction:(id)sender
{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillEstateViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (IBAction)addChildBtnAction:(id)sender
{
    //MyChildListViewController
    //MyWillChildrenInformationViewController
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyChildListViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
- (IBAction)minorChildBtnAction:(id)sender

{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyMinorChildrenViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (IBAction)funeralBtnAction:(id)sender

{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillFuneralRecomendationViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}



////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(UIButton*)sender
{
    
    
    
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        if(sender.tag==1)
        {
        contentMessage = @"Person who is creating the will.";
        }
        
        if(sender.tag==2)
        {
            contentMessage = @"A person or institution appointed by a testator to carry out the terms of their will in their absence";
        }
        
        if(sender.tag==3)
        {
            contentMessage = @"the person or persons who will receive benefits from you in the form of cash or property .";
        }
        
        
        if(sender.tag==4)
        {
            contentMessage = @"A person or firm appointed by you separate from the will that holds and administers property or assets for the benefit of a third party.";
        }
        if(sender.tag==5)
        {
            contentMessage = @"the person or persons who will receive benefits from you in the form of cash or property .";
        }
        if(sender.tag==6)
        {
            contentMessage = @"All the money and property owned by a particular person, especially at death.(In his will, he divided his estate between his wife and daughter).";
        }
        
        if(sender.tag==7)
        {
            contentMessage = @"The person who would take care of any younger children if testator is absent";
        }
        
        if(sender.tag==8)
        {
            contentMessage = @"the person or persons who will receive benefits from you in the form of cash or property .";
        }
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        /* Some options to try.
         */
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        //popTipView.hasGradientBackground = NO;
        //popTipView.cornerRadius = 2.0;
        //popTipView.sidePadding = 30.0f;
        //popTipView.topMargin = 20.0f;
        //popTipView.pointerSize = 50.0f;
        //popTipView.hasShadow = NO;
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}


-(void)PlayWillVideo
{
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"xx" ofType:@"mp4"]];
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[self.willViewShowView bounds]];
    [self.willViewShowView.layer addSublayer:avPlayerLayer];
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    [self.avplayer play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
//    //Config dark gradient view
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = [[UIScreen mainScreen] bounds];
//    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
//    [self.gradientView.layer insertSublayer:gradient atIndex:0];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.avplayer pause];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}


- (IBAction)willCreateDetailsShowCloseBtnAction:(id)sender
{
    self.avplayer = nil;
    [_willCreateDetailsShowView removeFromSuperview];
}

- (IBAction)myWillDetailds:(id)sender
{
    
}
- (IBAction)myWillupload:(id)sender
{
    
}
@end
