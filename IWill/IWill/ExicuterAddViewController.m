//
//  ExicuterAddViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/15/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ExicuterAddViewController.h"
#import "Utils.h"
#import "Constant.h"
#import "SlideNavigationController.h"
#import "ConnectionManager.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface ExicuterAddViewController ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ConnectionManager_Delegate>

{
    UITextField* activeField;
    UIImage * selectedImage;
    ConnectionManager * connectionManager;
    
}


@end

@implementation ExicuterAddViewController

- (void)viewDidLoad {
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    
self.datebackgroundView.hidden = YES;

CAGradientLayer *gradient = [CAGradientLayer layer];
gradient.frame = [[UIScreen mainScreen] bounds];
gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
[self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
self.dateMainView.layer.cornerRadius = 4;

self.iWilldatePicker.maximumDate = [NSDate date];

self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
self.submitBtn = [Utils buttonWithSaddowAndradius:self.submitBtn];


[self registerForKeyboardNotifications];
[self initViewWthLayer];


[_backBtn addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];


// Do any additional setup after loading the view.
}

-(void)setValueInField
{
    
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    if(textField == self.dobTxt)
    {
        [self selectDate];
        //self.datebackgroundView.hidden = NO;
        return NO;
    }
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}





-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        if(textField == self.ssnLast4DigitText)
        {
            NSInteger textLenth = self.ssnLast4DigitText.text.length;
    
            if(textLenth == 4 && string.length>0)
            {
                return NO;
            }
    
        }
    
    return YES;
}


- (IBAction)dateSelectedBtnAction:(id)sender {
    
    _dobTxt.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}



- (IBAction)submitBtnAction:(id)sender
{
    if([self checkTextValidation])
    {
                if (![Utils isInternetAvailable])
                {
                    [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    return;
                }
                [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
                NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
                NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
                [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        
                [dataDict setValue:_nameTxt.text forKey:@"first_name"];
                [dataDict setValue:_dobTxt.text forKey:@"last_name"];
                [dataDict setValue:_dobTxt.text forKey:@"dob"];
                [dataDict setValue:self.nameOfOldestChildTxt forKey:@"email"];
                [dataDict setValue:self.relationshipToTestatorText forKey:@"relationship"];
                [dataDict setValue:self.ssnLast4DigitText.text forKey:@"age"];
                [dataDict setValue:self.knowBodyMarkText.text forKey:@"mobile"];
                [dataDict setValue:self.addressText.text forKey:@"address"];
        
                [dataDict setValue:@"" forKey:@"city"];
                [dataDict setValue:@"" forKey:@"state"];
                [dataDict setValue:@"" forKey:@"zip_code"];
                [dataDict setValue:@"" forKey:@"country"];
        
                [dataDict setValue:_nameOfOldestChildTxt.text forKey:@"oldest_child_name"];
                [dataDict setValue:_ssnLast4DigitText.text forKey:@"security_no"];
                [dataDict setValue:_knowBodyMarkText.text forKey:@"permanent_mark"];
                [dataDict setValue:@"1" forKey:@"authentication"];
        
                [dataDict setValue:[Utils encodeToBase64String:selectedImage] forKey:@"image"];
                
                NSLog(@"hdsghj %@",dataDict);
                [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addExecutor"];
    }
    else{
        
    }

}

- (IBAction)enterBtnAction:(id)sender
{
    
    
    if([self checkTextValidation])
    {
//        if (![Utils isInternetAvailable])
//        {
//            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            return;
//        }
//        [Utils startActivityIndicatorInView:self.view withMessage:@""];
//        
//        
//        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
//        
//        
//        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
//        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
//        
//        [dataDict setValue:_nameTxt.text forKey:@"first_name"];
//        [dataDict setValue:_dobTxt.text forKey:@"last_name"];
//        [dataDict setValue:_dobTxt.text forKey:@"dob"];
//        [dataDict setValue:self.nameOfOldestChildTxt forKey:@"email"];
//        [dataDict setValue:self.relationshipToTestatorText forKey:@"relationship"];
//        [dataDict setValue:self.ssnLast4DigitText.text forKey:@"age"];
//        [dataDict setValue:self.knowBodyMarkText.text forKey:@"mobile"];
//        [dataDict setValue:self.addressText.text forKey:@"address"];
//        
//        [dataDict setValue:@"" forKey:@"city"];
//        [dataDict setValue:@"" forKey:@"state"];
//        [dataDict setValue:@"" forKey:@"zip_code"];
//        [dataDict setValue:@"" forKey:@"country"];
//        
//        [dataDict setValue:_nameOfOldestChildTxt.text forKey:@"oldest_child_name"];
//        [dataDict setValue:_ssnLast4DigitText.text forKey:@"security_no"];
//        [dataDict setValue:_knowBodyMarkText.text forKey:@"permanent_mark"];
//        [dataDict setValue:@"1" forKey:@"authentication"];
//
//        [dataDict setValue:[Utils encodeToBase64String:selectedImage] forKey:@"image"];
//        
//        NSLog(@"hdsghj %@",dataDict);
//        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addWitness"];
    }
    else{
        
    }
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(BOOL)checkTextValidation
{
    if(self.nameTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:exicuterName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
        
    }
    
    if(self.lastName.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerAge delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
        
    }
    
    
    if(self.emailTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:exicuterEmail delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    
    if(self.addressText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerAddress delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    
    if(self.dobTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerDOB delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    
    if(self.ageTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerAge delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }

    
    
    if(self.ssnLast4DigitText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerSSN delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    if(self.nameOfOldestChildTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerChildName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }

    if(self.knowBodyMarkText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerBodyMark delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    if(self.relationshipToTestatorText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerRelationship delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    if(selectedImage == nil)
    {
        [Utils showAlertView:alertTitle message:@"Please select executer profile image" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    
    if(![self.authorizationBtn isSelected])
    {
        [Utils showAlertView:alertTitle message:@"Please Accept term and condition." delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
         return NO;
    }
    
    return YES;
}



- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+30, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    
    //}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}



-(void)initViewWthLayer


{
    
    self.submitBtn = [Utils buttonWithSaddowAndradius:self.submitBtn];
    
    self.profilePicBtn.layer.cornerRadius = 50;
    self.profilePicBtn.layer.borderWidth=1;
    self.profilePicBtn.layer.borderColor = [UIColor purpleColor].CGColor;
    self.profilePicBtn.layer.masksToBounds = YES;
    self.profilePicBtn.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.profilePicBtn.layer.shadowOpacity = 0.3;
    self.profilePicBtn.layer.shadowRadius = 4;
    self.profilePicBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
}


- (IBAction)profilePicBtnAction:(id)sender {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Take Image" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take image From Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self chooseFromGalary ];
        //[self dismissViewControllerAnimated:YES completion:^{
        //}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take image From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self chooseFromCamera];
        
        // [self dismissViewControllerAnimated:YES completion:^{
        //}];
    }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
  
    
}




-(void)chooseFromCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
}




-(void)chooseFromGalary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}



- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    selectedImage = chosenImage;
    [self.profilePicBtn setImage:chosenImage forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (IBAction)authorizationBtnAction:(UIButton*)sender
{
    if([sender isSelected])
    {
        [sender setSelected: NO];
    }
    else
    {
        [sender setSelected: YES];
    }
    
    
    
}



-(void)selectDate
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeDate];
    [alertController.view addSubview:picker];
   
    
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"OK");
        NSLog(@"%@",picker.date);
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"OK");
        NSLog(@"%@",picker.date);
    }];
    
    [alertController addAction:action];
    [alertController addAction:action1];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)authorizationLinkBtnAction:(id)sender {
}
@end
