//
//  ExecuterAddNewViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/16/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"
#import "JVFloatLabeledTextField.h"

#import "CMPopTipView.h"

@interface ExecuterAddNewViewController : UIViewController<CMPopTipViewDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *executerSegment;
- (IBAction)executerSegmentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fullName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *dob;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UITextField *relationship;
@property(nonatomic,weak)IBOutlet UIScrollView * mainScrollView;
@property (strong, nonatomic)  NSString *executerCount;
@property (strong, nonatomic)  NSMutableArray *executerList;


@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

- (IBAction)saveBtnAction:(id)sender;
- (IBAction)backBrnAction:(id)sender;
- (IBAction)hintBtnAction:(id)sender;




@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;

@end
