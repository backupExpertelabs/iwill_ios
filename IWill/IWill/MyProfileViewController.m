//
//  MyProfileViewController.m
//  IWill
//
//  Created by A1AUHAIG on 4/27/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyProfileViewController.h"
#import "CitySugectionDropDown.h"
#import "Utils.h"
#import "Constant.h"
#import "SlideNavigationController.h"
#import "UIImage+AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "ConnectionManager.h"
#import "UIButton+AFNetworking.h"
#import "AppManager.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MyProfileViewController ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,citySuggetionDelegate,ConnectionManager_Delegate>

{
    UITextField* activeField;
    UIImage * selectedImage;
    CitySugectionDropDown * citySugectionDropDown;
    ConnectionManager * connectionManager;
    BOOL isTestatorProfileAdded;
}
@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isTestatorProfileAdded = NO;
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    
    citySugectionDropDown = [[CitySugectionDropDown alloc] init];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    //self.submitBtn = [Utils buttonWithSaddowAndradius:self.submitBtn];
    
    self.submitBtn.layer.cornerRadius = 4;
    
    self. nameTextF.delegate = self;
    self. genderTextF.delegate = self;
    self. phoneTextF.delegate = self;
    self. childrenTextF.delegate = self;
    self.countryTextF.delegate = self;
    self. stateTextF.delegate = self;
    self.cityTextF.delegate = self;;
    self.zipTextF.delegate = self;
    self.placeOfBirthTextF.delegate = self;
    self.motherTextF.delegate = self;
    self.fatherTextF.delegate = self;
    self. maritalStatusTextF.delegate = self;
    self.addressTextF.delegate = self;
    [self registerForKeyboardNotifications];
    [self initViewWthLayer];
    
    
    [_backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // backBtnAction
    
//    [_backBtn addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    
    [self getTestatorProfile];
    // Do any additional setup after loading the view.
}

-(void)setValueInField
{
    
}

-(void)getTestatorProfile
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:alertTitle message:alertNoInternetConection delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSLog(@"input data %@",dataDict);
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@",@"getTesttatorUsingId",[userData valueForKey:@"id"]];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:Nil andDelegate:self andURL:urlStr];
    
}


#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //activeField = textField;
    if(textField == self.dobTextF)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    
    if(textField == self.spouseDOBTxtF)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    
    
    
    
    if(textField == self.maritalStatusTextF)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        NSMutableArray * maritalStatusArr = [[NSMutableArray alloc] initWithObjects:@"Married",@"Single",@"Divorce", nil];
        [self maritalStatusWithText:textField andDataArray:maritalStatusArr];
        return NO;
    }
    activeField = textField;
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //    if(textField == self.socialSecurityNumberText)
    //    {
    //        NSInteger textLenth = self.socialSecurityNumberText.text.length;
    //
    //        if(textLenth == 4 && string.length>0)
    //        {
    //            return NO;
    //        }
    //
    //    }
    
    return YES;
}


- (IBAction)dateSelectedBtnAction:(id)sender {
    
    activeField.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}



- (IBAction)submitBtnAction:(id)sender
{
    
//    if(isTestatorProfileAdded == YES)
//    {
        if([self checkTextValidation])
        {
            if (![Utils isInternetAvailable])
            {
                [Utils showAlertView:alertTitle message:alertNoInternetConection delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                return;
            }
            
            [Utils startActivityIndicatorInView:self.view withMessage:@""];
            NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
            NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
            [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
            
            NSMutableDictionary * profileDict = [[NSMutableDictionary alloc]init];

            [profileDict setValue:_nameTextF.text forKey:@"first_name"];
            [profileDict setValue:_lastNameTxtF.text forKey:@"last_name"];
            [profileDict setValue:_dobTextF.text forKey:@"dob"];
            [profileDict setValue:_genderTextF.text forKey:@"gender"];
            [profileDict setValue:_spouseNameTextF.text forKey:@"spouse_name"];
            [profileDict setValue:_spouseDOBTxtF.text forKey:@"spouse_dob"];
            [profileDict setValue:_placeOfBirthTextF.text forKey:@"address"];
            [profileDict setValue:_motherTextF.text forKey:@"mother_name"];
            [profileDict setValue:_fatherTextF.text forKey:@"father_name"];
            [profileDict setValue:_bodyMarkingTextF.text forKey:@"body_mark_location"];
            
            [dataDict setValue:profileDict forKey:@"testator_info"];

            
            NSLog(@"input data %@",dataDict);
            [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"update_testator_profile"];
        }
        else
        {
            
        }
//    }
//    else
//    {
//
//        if([self checkTextValidation])
//        {
//            if (![Utils isInternetAvailable])
//            {
//                [Utils showAlertView:alertTitle message:alertNoInternetConection delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
//                return;
//            }
//
//            [Utils startActivityIndicatorInView:self.view withMessage:@""];
//            NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
//
//            NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
//            [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
//            [dataDict setValue:_nameTextF.text forKey:@"first_name"];
//            [dataDict setValue:_lastNameTxtF.text forKey:@"last_name"];
//            [dataDict setValue:@"" forKey:@"gender"];
//            [dataDict setValue:@"" forKey:@"mobile"];
//            [dataDict setValue:_dobTextF.text forKey:@"dob"];
//            [dataDict setValue:_childrenTextF.text forKey:@"no_of_children"];
//            [dataDict setValue:_addressTextF.text forKey:@"address"];
//            [dataDict setValue:_cityTextF.text forKey:@"city"];
//            [dataDict setValue:_stateTextF.text forKey:@"state"];
//            [dataDict setValue:_zipTextF.text forKey:@"zip_code"];
//            [dataDict setValue:_countryTextF.text forKey:@"country"];
//            [dataDict setValue:_placeOfBirthTextF.text forKey:@"place_of_birth"];
//            [dataDict setValue:_motherTextF.text forKey:@"mother_name"];
//            [dataDict setValue:_fatherTextF.text forKey:@"father_name"];
//            [dataDict setValue:_maritalStatusTextF.text forKey:@"relationship_status"];
//            [dataDict setValue:_firstChildNameTextF.text forKey:@"child_name"];
//            [dataDict setValue:_ssnTextF.text forKey:@"security_no"];
//            [dataDict setValue:[Utils encodeToBase64String:selectedImage] forKey:@"image"];
//            NSLog(@"input data %@",dataDict);
//            [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"testator_profile"];
//        }
//        else{
//
//        }
        
    //}
}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:alertTitle message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        
        
        //        UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
        //        [self.navigationController pushViewController:vc animated:YES];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"Result"] isKindOfClass:[NSNull  class]])
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                return ;
            }
            
            if([aResponceDic objectForKey:@"Result"] != nil)
            {
                
                isTestatorProfileAdded = YES;
                [self setDataOnView:[aResponceDic valueForKey:@"Result"]];
            }
            else{
                //            UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                //            [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+30, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    
    //}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}



-(void)initViewWthLayer


{
    
//    self.submitBtn = [Utils buttonWithSaddowAndradius:self.submitBtn];
//    self.profilePicBtn.layer.cornerRadius = 50;
//    self.profilePicBtn.layer.borderWidth=1;
//    self.profilePicBtn.layer.borderColor = [UIColor purpleColor].CGColor;
//    self.profilePicBtn.layer.masksToBounds = YES;
//    self.profilePicBtn.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    self.profilePicBtn.layer.shadowOpacity = 0.3;
//    self.profilePicBtn.layer.shadowRadius = 4;
//    self.profilePicBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    
    
    
    
    
    self. nameTextF  = [Utils setTextFieldBottumBorder: self. nameTextF andController:self];
    self. genderTextF  = [Utils setTextFieldBottumBorder: self. genderTextF andController:self];
    self. motherTextF  = [Utils setTextFieldBottumBorder: self. motherTextF andController:self];
    self. fatherTextF  = [Utils setTextFieldBottumBorder: self. fatherTextF andController:self];
    self. addressTextF  = [Utils setTextFieldBottumBorder: self. addressTextF andController:self];
    self. placeOfBirthTextF  = [Utils setTextFieldBottumBorder: self. placeOfBirthTextF andController:self];
    
    self. ssnTextF  = [Utils setTextFieldBottumBorder: self. ssnTextF andController:self];
    self. lastNameTxtF  = [Utils setTextFieldBottumBorder: self. lastNameTxtF andController:self];
    self. spouseDOBTxtF  = [Utils setTextFieldBottumBorder: self. spouseDOBTxtF andController:self];
    self. spouseNameTextF  = [Utils setTextFieldBottumBorder: self. spouseNameTextF andController:self];
    self. bodyMarkingTextF  = [Utils setTextFieldBottumBorder: self. bodyMarkingTextF andController:self];
    self. dobTextF  = [Utils setTextFieldBottumBorder: self. dobTextF andController:self];


    
}


- (IBAction)profilePicBtnAction:(id)sender {
    
    
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Take Image" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take image From Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self chooseFromGalary ];
        //[self dismissViewControllerAnimated:YES completion:^{
        //}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take image From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self chooseFromCamera];
        
        // [self dismissViewControllerAnimated:YES completion:^{
        //}];
    }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
    
    
}




-(void)chooseFromCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
}




-(void)chooseFromGalary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}



- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    selectedImage = chosenImage;
    [self.profilePicBtn setImage:chosenImage forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)maritalStatusWithText:(UITextField*)teField andDataArray:(NSMutableArray*)dataArray
{
    [teField resignFirstResponder];
    
    [citySugectionDropDown removeFromSuperview];
    [citySugectionDropDown addTableViewOnView:dataArray andFrame:CGRectMake(teField.frame.origin.x, teField.frame.origin.y+40, teField.frame.size.width, 150) andType:@""];
    citySugectionDropDown.delegate = self;
    [teField.superview addSubview:citySugectionDropDown];
    
}


-(void)tableviewSelectedRow:(NSString*)name;

{
    _maritalStatusTextF.text = name;
    [citySugectionDropDown removeFromSuperview];
}


-(BOOL)checkTextValidation
{
    if(self.nameTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:firstNameMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(self.lastNameTxtF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:lastNameMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    
    
    if(self.placeOfBirthTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:profileAddress delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
   
    
    
    if(self.dobTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:profileDOB delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(self.motherTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:profileMotherName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if(self.fatherTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:profileFatherName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    if(self.ssnTextF.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:profileSSN delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(self.ssnTextF.text.length <4)
    {
        [Utils showAlertView:alertTitle message:profileSSNValidation delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    
    
    
    return YES;
}

-(void)setDataOnView:(NSDictionary*)responseDataDict
{
    _nameTextF.text = [responseDataDict valueForKey:@"first_name"];
    _lastNameTxtF.text = [responseDataDict valueForKey:@"last_name"];
    _placeOfBirthTextF.text = [responseDataDict valueForKey:@"address"];
    _dobTextF.text = [responseDataDict valueForKey:@"dob"];
    _genderTextF.text = [responseDataDict valueForKey:@"gender"];
    _spouseNameTextF.text = [responseDataDict valueForKey:@"spouse_name"];
    _spouseDOBTxtF.text = [responseDataDict valueForKey:@"spouse_dob"];
    _motherTextF.text = [responseDataDict valueForKey:@"mother_name"];
    _fatherTextF.text = [responseDataDict valueForKey:@"father_name"];
    _bodyMarkingTextF.text = [responseDataDict valueForKey:@"body_mark_location"];
    _ssnTextF.text = [responseDataDict valueForKey:@"security_no"];

    //_dobTextF.enabled = NO;
    //_fatherTextF.enabled = NO;
   // _genderTextF.enabled = NO;
   // _motherTextF.enabled = NO;
    
    
}

@end
