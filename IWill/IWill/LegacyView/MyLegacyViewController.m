//
//  MyLegacyViewController.m
//  IWill
//
//  Created by A1AUHAIG on 7/5/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyLegacyViewController.h"
#import "Utils.h"
#import <MobileCoreServices/MobileCoreServices.h> // needed for video types
#import <AVFoundation/AVFoundation.h>
#import "LegacyTableViewCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Constant.h"
#import "ConnectionManager.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "MyLegacyFolderViewController.h"

@interface MyLegacyViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate,ConnectionManager_Delegate>

{
    NSMutableArray * dataArray;
    NSString * uploadType;
    UIImage * selectedImage;
    NSURL *videoURL;
    NSURL * audioURL;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    int audioFileCount;
    
    NSTimer * timer;
    NSTimer * audioRecordingtimer;
    UITextField * audioRecordingTextF;
    
    ConnectionManager * connectionManager;
    
    int audioRecordingTime;
    AppDelegate * appD;
    
    BOOL isNewUpload;
    
}
@property (strong, nonatomic) MPMoviePlayerController *videoController;
@property (nonatomic, strong) AVPlayer *avplayer;

@end
//a22bcef3-fe58-4e88-8d78-509daf8d4610





@implementation MyLegacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    isNewUpload = YES;
    
    appD =  (AppDelegate*)[UIApplication sharedApplication].delegate;
    appD.folderId = @"";
    appD.folderName = @"";
    
    
    audioRecordingTime = 0;
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    dataArray = [[NSMutableArray alloc]init];
    [self setLayerOnView];
    self.mainView.hidden = YES;
    
    _audioPlayerView.backgroundColor = [UIColor grayColor];
    [self registerForKeyboardNotifications];
    
    
    _videoBtn.layer.cornerRadius = 3;
    _imageUploadBtn.layer.cornerRadius = 3;
    _audioUploadBtn.layer.cornerRadius = 3;
    
    _cancelBtn.layer.cornerRadius = 3;
    _submitBtn.layer.cornerRadius = 3;
    
    [self getAllLegacyFile];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setLayerOnView
{
    _dateTimeTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_dateTimeTxtF];
    _titleTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_titleTxtF];
    _whoForIntendedForTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_whoForIntendedForTxtF];
    _releaseOnTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_releaseOnTxtF];
    
    _locationTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_locationTxtF];
    _descriptionTxtF = (JVFloatLabeledTextView*)[Utils setBottumBorderOnJVFloatLabeledTextField:_descriptionTxtF];
    
    self.playerView.layer.borderWidth = 3;
    self.playerView.layer.borderColor = [UIColor grayColor].CGColor;
}


- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)uploadVideoBtnAction:(id)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Take Video" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take From Galary" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self takeVideoFromGalary];
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self takeVideoFromCamera];
        // Distructive button tapped.
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (IBAction)uploadImageBtnAction:(id)sender
{
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Take Image" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Image From Galary" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        
        [self takeImageFromGalary];
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Image From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        
        [self takeImageFromCamera];
        
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
}
- (IBAction)uploadAudioBtnAction:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Record Audio" message:@"Do you want record audio for legancy" preferredStyle:UIAlertControllerStyleAlert];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self setUpTheAudioFile];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(void)takeVideoFromCamera
{
    uploadType = @"video";
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:picker animated:YES completion:nil];
}


-(void)takeVideoFromGalary
{
    uploadType = @"video";
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self;
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:videoPicker animated:YES completion:nil];
}


-(void)takeImageFromGalary
{
    
    uploadType = @"image";
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];}


-(void)takeImageFromCamera
{
    uploadType = @"image";
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    selectedImage = info[UIImagePickerControllerEditedImage];
    
    // This is the NSURL of the video object
    videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    
    NSLog(@"VideoURL = %@", videoURL);
    //    if([uploadType isEqualToString:@"image"])
    //    {
    //        _selectedImageView.hidden = NO;
    //        _selectedImageView.image = selectedImage;
    //        self.mainView.hidden = NO;
    //        self.mainView.backgroundColor = [UIColor grayColor];
    //        _videoPlayBtn.hidden = YES;
    //        _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
    //
    //    }
    //    else{
    //        _selectedImageView.hidden = YES;
    //        self.mainView.hidden = NO;
    //        _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
    //        [self videoPlayerView:videoURL];
    //        _videoPlayBtn.hidden = NO;
    //
    //
    //    }
    //    [_mainScrollView setContentOffset:CGPointZero animated:YES];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    MyLegacyFolderViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyLegacyFolderViewController"];
    vc.folderNameArray = dataArray;
    [self presentViewController:vc animated:YES completion:nil];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


-(void)showVideoView:(NSString*)type
{
    self.mainView.hidden = NO;
    _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
    isNewUpload = YES;
}

- (IBAction)viewCancelBtnAction:(id)sender
{
    self.mainView.hidden = YES;
    _dateTimeTxtF.text = @"";
    _titleTxtF.text = @"";
    _whoForIntendedForTxtF.text = @"";
    _releaseOnTxtF.text = @"";
    _descriptionTxtF.text = @"";
    _locationTxtF.text = @"";
    [self.videoController.view removeFromSuperview];
    [player stop];
    player =nil;
    
}

- (IBAction)saveBtnAction:(id)sender
{
    
    if(_titleTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please inter title" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        return;
    }
    
    
    if(_whoForIntendedForTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please inter Who for intended" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        return;
    }
    
    if(_releaseOnTxtF.text.length ==0)
    {
        [Utils showAlertView:@"" message:@"Please inter when you release" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        return;
    }
    
    if(_descriptionTxtF.text.length ==0)
    {
        [Utils showAlertView:@"" message:@"Please inter description" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        return;
    }
    
    if(_locationTxtF.text.length ==0)
    {
        [Utils showAlertView:@"" message:@"Please inter location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        return;
    }
    
    if(isNewUpload)
    {
        
        if([uploadType isEqualToString:@"image"])
        {
            [self UploadImage];
            
        }
        else if ([uploadType isEqualToString:@"video"])
        {
            [self UploadVideo];
        }
        
        else
        {
            [self UploadAudio];
            
        }
    }
    else
    {
        [self updateLagacyFile];
    }
    
}

-(void)videoPlayerView:(NSURL*)path
{
    self.videoController = [[MPMoviePlayerController alloc] init];
    [self.videoController setContentURL:path];
    [self.videoController.view setFrame:CGRectMake (2, 2, self.view.frame.size.width-4, 194)];
    [self.playerView addSubview:self.videoController.view];
    [self.playerView bringSubviewToFront:_videoPlayBtn];
    [self.videoController thumbnailImageAtTime:1 timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
    NSString * folderName=    appD.folderName;
    NSString * folderId=    appD.folderId;
    
    
    if(folderName.length==0 && folderId.length==0)
    {
        return;
    }
    else
    {
        
        if([uploadType isEqualToString:@"image"])
        {
            _selectedImageView.hidden = NO;
            _selectedImageView.image = selectedImage;
            self.mainView.hidden = NO;
            self.mainView.backgroundColor = [UIColor grayColor];
            _videoPlayBtn.hidden = YES;
            _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
            
        }
        else if ([uploadType isEqualToString:@"video"]){
            _selectedImageView.hidden = YES;
            self.mainView.hidden = NO;
            _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
            [self videoPlayerView:videoURL];
            _videoPlayBtn.hidden = NO;
            
            
        }
        
        else{
            self.mainView.hidden = NO;
            _selectedImageView.hidden = YES;
            _videoPlayBtn.hidden = YES;
            _audioPlayerView.hidden = NO;
            player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
            [player setDelegate:self];
            _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
        }
        [_mainScrollView setContentOffset:CGPointZero animated:YES];
        
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    [self unregisterForKeyboardNotifications];
    
}


- (void)playerStartPlaying
{
    
}

- (void)playerItemDidReachEnd:(NSNotification *)notification

{
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataArray.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[[dataArray objectAtIndex:section] valueForKey:@"files"] count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    LegacyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[LegacyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
    }
    
    //[[dataArray objectAtIndex:section] valueForKey:@"files"]
    //NSMutableDictionary * dict =  [dataArray objectAtIndex:indexPath.row];
    NSMutableDictionary * dict =  [[[dataArray objectAtIndex:indexPath.section] valueForKey:@"files"] objectAtIndex:indexPath.row];
    
    //    [cell.imageView setImageWithURL:[NSURL URLWithString:@"http://example.com/image.jpg"]
    //                   placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.titleLbl.text = [dict valueForKey:@"title"];
    cell.whomToLbl.text = [dict valueForKey:@"intended_for"];
    cell.descriptionLbl.text = [dict valueForKey:@"description"];
    
    if([[dict valueForKey:@"type"] isEqualToString:@"image"])
    {
        cell.placeImageView.image = [dict valueForKey:@"image"];
        cell.playBtn.hidden = YES;
        [cell.placeImageView setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"file_name"]] placeholderImage:nil];
    }
    else if([[dict valueForKey:@"type"] isEqualToString:@"video"])
    {
        cell.playBtn.hidden = NO;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //cell.placeImageView.image = [self loadThumbNail:[NSURL URLWithString:[dict valueForKey:@"file_name"]]];
        });
    }
    else
    {
        cell.placeImageView.image = [UIImage imageNamed:@"audio.png"];
        cell.playBtn.hidden = YES;
        
    }
    
    cell.deleteBtn.tag = [[dict valueForKey:@"id"] integerValue];
    [cell.deleteBtn addTarget:self action:@selector(deleteDataFromArray:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, tableView.frame.size.width, 30)];
    label.textColor = [UIColor whiteColor];
    
    //NSMutableDictionary * dict =  [[[dataArray objectAtIndex:indexPath.section] valueForKey:@"files"] objectAtIndex:indexPath.row];
    
    NSString *string =[[dataArray objectAtIndex:section]valueForKey:@"name"];
    [label setText:string];
    [view addSubview:label];
    [label setBackgroundColor: [Utils getBorderLineColor]];
    [view setBackgroundColor: [Utils getBorderLineColor]];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary * dict =  [[[dataArray objectAtIndex:indexPath.section] valueForKey:@"files"] objectAtIndex:indexPath.row];
    
    //NSDictionary * dict = [dataArray objectAtIndex:indexPath.row];
    
    isNewUpload = NO;
    
    
    if([[dict valueForKey:@"type"] isEqualToString:@"image"])
    {
        uploadType = @"image";
        _selectedImageView.hidden = NO;
        
        self.mainView.hidden = NO;
        _videoPlayBtn.hidden = YES;
        _audioPlayerView.hidden = YES;
        
        [_selectedImageView setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"file_name"]] placeholderImage:nil] ;
        
        _dateTimeTxtF.text = [dict valueForKey:@"date"];
        _titleTxtF.text = [dict valueForKey:@"title"];;
        _whoForIntendedForTxtF.text = [dict valueForKey:@"intended_for"];;
        _releaseOnTxtF.text = [dict valueForKey:@"release_on"];;
        _descriptionTxtF.text = [dict valueForKey:@"description"];;
        _locationTxtF.text = [dict valueForKey:@"location"];;
        
    }
    else if ([[dict valueForKey:@"type"] isEqualToString:@"video"])
        
    {
        
        uploadType = @"video";
        _selectedImageView.hidden = YES;
        self.mainView.hidden = NO;
        [self videoPlayerView: [NSURL URLWithString:[dict valueForKey:@"file_name"]]];
        _videoPlayBtn.hidden = NO;
        _audioPlayerView.hidden = YES;
        
        _dateTimeTxtF.text = [dict valueForKey:@"date"];
        _titleTxtF.text = [dict valueForKey:@"title"];;
        _whoForIntendedForTxtF.text = [dict valueForKey:@"intended_for"];;
        _releaseOnTxtF.text = [dict valueForKey:@"release_on"];;
        _descriptionTxtF.text = [dict valueForKey:@"description"];;
        _locationTxtF.text = [dict valueForKey:@"location"];;
        
        
    }
    
    
    else if ([[dict valueForKey:@"type"] isEqualToString:@"audio"])
        
    {
        
        uploadType = @"audio";
        _selectedImageView.hidden = YES;
        self.mainView.hidden = NO;
        _videoPlayBtn.hidden = YES;
        _audioPlayerView.hidden = NO;
        
        _dateTimeTxtF.text = [dict valueForKey:@"date"];
        _titleTxtF.text = [dict valueForKey:@"title"];;
        _whoForIntendedForTxtF.text = [dict valueForKey:@"intended_for"];;
        _releaseOnTxtF.text = [dict valueForKey:@"release_on"];;
        _descriptionTxtF.text = [dict valueForKey:@"description"];;
        _locationTxtF.text = [dict valueForKey:@"location"];;
        
        audioURL = [NSURL URLWithString:[dict valueForKey:@"file_name"]];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
        [player setDelegate:self];
        
        
    }
    
    [_mainScrollView setContentOffset:CGPointZero animated:YES];
    
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //tempTxtF = textField;
    return YES;
}




- (IBAction)videoPlayBtnAction:(id)sender {
    
    [self.videoController play];
}

-(UIImage *)loadThumbNail:(NSURL *)urlVideo
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlVideo options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    return [[UIImage alloc] initWithCGImage:imgRef];
}

-(void)deleteDataFromArray:(UIButton*)sender
{
    
    
    NSString * fileId = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    //    [dataArray removeObjectAtIndex:sender.tag];
    //    [_albumTableView reloadData];
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Message" message:@"Do you want delete this file" preferredStyle:UIAlertControllerStyleAlert];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self deleteLagcyFileFromServerAPI:fileId];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(void)deleteLagcyFileFromServerAPI:(NSString*)fileId
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    ///deletelegacyUpload/(:any)/(:any
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@/%@",@"deletelegacyUpload",[userData valueForKey:@"id"],fileId];
    NSLog(@"skdjjhfjh %@",urlStr);
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
    
    
}


-(void)setUpTheAudioFile
{
    // Set the audio file
    audioFileCount = audioFileCount+1;
    
    NSString * audioPath = [NSString stringWithFormat:@"%@%d.m4a",@"audioFile",audioFileCount];
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               audioPath,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    audioURL = outputFileURL;
    
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    [session setActive:YES error:nil];
    [recorder record];
    [self callWhenAudioRecordindStart];
    
}

- (IBAction)recordPauseTapped:(id)sender {
    // Stop the audio player before recording
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
    } else {
        // Pause recording
        [recorder pause];
    }
}
- (IBAction)stopTapped:(id)sender {
    
    audioRecordingTime =0;
    [recorder stop];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    [timer invalidate];
    [audioRecordingtimer invalidate];
    
    
    //    self.mainView.hidden = NO;
    //    _selectedImageView.hidden = YES;
    //    _videoPlayBtn.hidden = YES;
    //    _audioPlayerView.hidden = NO;
    //    player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
    //    [player setDelegate:self];
    //    _dateTimeTxtF.text = [Utils getDateFromDateForAPI:[NSDate date]];
    
    
    MyLegacyFolderViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyLegacyFolderViewController"];
    vc.folderNameArray = dataArray;
    [self presentViewController:vc animated:YES completion:nil];
    
    
}

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    
}


- (IBAction)playTapped:(UIButton*)sender {
    if (!player.isPlaying){
        //        player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
        //        [player setDelegate:self];
        [player play];
        [self startTime];
    }
    else
    {
        [player pause];
        [timer invalidate];
    }
}


- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    //player = nil;
    //_audioPlayerTimerLbl.text = @"00:00";
    [timer invalidate];
    NSLog(@"audio playing finish");
}



-(void)callWhenAudioRecordindStart
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Audio recording start" preferredStyle:UIAlertControllerStyleAlert];
    
    // alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [actionSheet addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = @"00:00";
        textField.backgroundColor = [UIColor blueColor];
        textField.borderStyle = UITextBorderStyleNone;
        textField.textAlignment = NSTextAlignmentCenter;
        textField.backgroundColor = [UIColor clearColor];
        [textField setUserInteractionEnabled:NO];
        audioRecordingTextF = textField;
        audioRecordingtimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setUpAudioRecordingTimer:) userInfo:nil repeats:YES];
    }];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Please stop and Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self stopTapped:nil];
        
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}


-(void)setUpAudioRecordingTimer:(NSTimer*)timer
{
    int seconds = 0;
    int minutes = 0;
    //    int time = (int)timer;
    
    audioRecordingTime = audioRecordingTime+1;
    seconds =  (audioRecordingTime)%60;   ////Int(time) % 60;
    minutes = ((audioRecordingTime) / 60) % 60;
    audioRecordingTextF.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];;
}


-(void)audioPlayerCurrentTime
{
    int seconds = 0;
    int minutes = 0;
    int time = player.currentTime;
    seconds =  (time)%60;   ////Int(time) % 60;
    minutes = ((time) / 60) % 60;
    
    _audioPlayerTimerLbl.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    //return [NSString stringWithFormat:@"%0.2f:%0.2f",minutes,seconds];
    //return String(format: "%0.2d:%0.2d",minutes,seconds)
}



-(void)setProgresBar

{
    float theCurrentTime = 0.0;
    float theCurrentDuration = 0.0;
    float currentTime = player.currentTime;
    float duration = player.duration ;
    theCurrentTime = currentTime;
    theCurrentDuration = duration;
    //return Float(theCurrentTime / theCurrentDuration)
    NSLog(@"hhhhhhhh %f kkkkkk %f",currentTime,duration);
    _audipPlayProgressBar.progress = (theCurrentTime / theCurrentDuration);
}


-(void)startTime{
    // timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateViewsWithTimer:"), userInfo: nil, repeats: true)
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(updateProgressBar:) userInfo:nil repeats:YES];
    
    
    
}

-(void)updateProgressBar:(NSTimer*)timer
{
    [self setProgresBar];
    [self audioPlayerCurrentTime];
}


- (IBAction)audioPlayBtnAction:(id)sender {
    
    [self playTapped:nil];
    
}

-(void)UploadImage
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:appD.folderName forKey:@"folder_name"];
    [dataDict setValue:appD.folderId forKey:@"folder_id"];
    
    [dataDict setValue:_dateTimeTxtF.text forKey:@"date"];
    [dataDict setValue:@"" forKey:@"time"];
    [dataDict setValue:_titleTxtF.text forKey:@"title"];
    [dataDict setValue:_whoForIntendedForTxtF.text forKey:@"intended_for"];
    [dataDict setValue:_releaseOnTxtF.text forKey:@"release_on"];
    [dataDict setValue:_descriptionTxtF.text forKey:@"description"];
    [dataDict setValue:_locationTxtF.text forKey:@"location"];
    
    
    //NSData *imageData = UIImagePNGRepresentation(_selectedImageView.image);
    NSData *imageData = UIImageJPEGRepresentation(_selectedImageView.image, 1.0);
    NSLog(@"jhfjsdgfhsf   %@",dataDict);
    
    [connectionManager getDataForFunction:@"legacyUploads" withInput:dataDict withCurrentTask:0 Delegate:self andMultipartData:imageData withMediaKey:@"file" andMineType:@"image/jpeg" andFileName:@"iWill_image.jpeg"];
    
   // [connectionManager getDataForFunction:@"legacyUploads" withInput:dataDict withCurrentTask:0 Delegate:self andMultipartData:imageData withMediaKey:@"file" andMineType:@"image/png" andFileName:@"iWill_image.png"];
    
}




-(void)updateLagacyFile
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:@"" forKey:@"flle_id"];
    
    NSMutableDictionary * fileDict = [[NSMutableDictionary alloc]init];
    
    [fileDict setValue:_dateTimeTxtF.text forKey:@"date"];
    [fileDict setValue:@"" forKey:@"time"];
    [fileDict setValue:_titleTxtF.text forKey:@"title"];
    [fileDict setValue:_whoForIntendedForTxtF.text forKey:@"intended_for"];
    [fileDict setValue:_releaseOnTxtF.text forKey:@"release_on"];
    [fileDict setValue:_descriptionTxtF.text forKey:@"description"];
    [fileDict setValue:_locationTxtF.text forKey:@"location"];
    [dataDict setValue:fileDict forKey:@"file_info"];
    
    
    [connectionManager getDataFromServerWithSessionTastmanager:@"updatelegacyUpload" withInput:dataDict andDelegate:self andURL:@"updatelegacyUpload"];
    
    
}



-(void)UploadVideo
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:appD.folderName forKey:@"folder_name"];
    [dataDict setValue:appD.folderId forKey:@"folder_id"];
    
    [dataDict setValue:_dateTimeTxtF.text forKey:@"date"];
    [dataDict setValue:@"" forKey:@"time"];
    [dataDict setValue:_titleTxtF.text forKey:@"title"];
    [dataDict setValue:_whoForIntendedForTxtF.text forKey:@"intended_for"];
    [dataDict setValue:_releaseOnTxtF.text forKey:@"release_on"];
    [dataDict setValue:_descriptionTxtF.text forKey:@"description"];
    [dataDict setValue:_locationTxtF.text forKey:@"location"];
    
    //NSData *imageData = UIImagePNGRepresentation(_selectedImageView.image);
    NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
    NSString * urlStr = [NSString stringWithFormat:@"%@",@"legacyUploads"];
    
    NSUInteger iamgeSize = videoData.length;
    NSLog(@"jhfjsdgfhsf   %f",iamgeSize);
    
    [connectionManager getDataForFunction:@"legacyUploads" withInput:dataDict withCurrentTask:0 Delegate:self andMultipartData:videoData withMediaKey:@"file" andMineType:@"video/quicktime" andFileName:@"iWillVideo.MOV"];
    
}

-(void)UploadAudio
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:appD.folderName forKey:@"folder_name"];
    [dataDict setValue:appD.folderId forKey:@"folder_id"];
    
    [dataDict setValue:_dateTimeTxtF.text forKey:@"date"];
    [dataDict setValue:@"" forKey:@"time"];
    [dataDict setValue:_titleTxtF.text forKey:@"title"];
    [dataDict setValue:_whoForIntendedForTxtF.text forKey:@"intended_for"];
    [dataDict setValue:_releaseOnTxtF.text forKey:@"release_on"];
    [dataDict setValue:_descriptionTxtF.text forKey:@"description"];
    [dataDict setValue:_locationTxtF.text forKey:@"location"];
    
    
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
    //    NSData *imageData = UIImageJPEGRepresentation(_selectedImageView.image, 1.0);
    NSString * urlStr = [NSString stringWithFormat:@"%@",@"legacyUploads"];
    
    [connectionManager getDataForFunction:@"legacyUploads" withInput:dataDict withCurrentTask:0 Delegate:self andMultipartData:audioData withMediaKey:@"file" andMineType:@"audio/m4a" andFileName:@"iWill_audio.m4a"];
    
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            // "method": "getlegacyUploadsC"
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getlegacyUploadsC"])
            {
                
                
                dataArray = [aResponceDic valueForKey:@"Result"];
                
                [_albumTableView reloadData];
                
            }
            
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"deletelegacyUploadC"])
            {
                
                
                [self getAllLegacyFile];
                
            }
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"legacyUploadsC"])
            {
                
                _dateTimeTxtF.text = @"";
                _titleTxtF.text = @"";
                _whoForIntendedForTxtF.text = @"";
                _releaseOnTxtF.text = @"";
                _descriptionTxtF.text = @"";
                _locationTxtF.text = @"";
                self.mainView.hidden = YES;
                [self.videoController.view removeFromSuperview];
                [self getAllLegacyFile];
                
            }
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"updatelegacyUploadC"])
            {
                _dateTimeTxtF.text = @"";
                _titleTxtF.text = @"";
                _whoForIntendedForTxtF.text = @"";
                _releaseOnTxtF.text = @"";
                _descriptionTxtF.text = @"";
                _locationTxtF.text = @"";
                self.mainView.hidden = YES;
                [self.videoController.view removeFromSuperview];
                [self getAllLegacyFile];
            }
            
            
        }
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)getAllLegacyFile
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // getlegacyUploads/(:any)
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@",@"getlegacyUploads",[userData valueForKey:@"id"]];
    
    
    NSLog(@"skdjjhfjh %@",urlStr);
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
}

@end
