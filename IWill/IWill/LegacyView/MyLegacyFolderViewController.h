//
//  MyLegacyFolderViewController.h
//  IWill
//
//  Created by A1AUHAIG on 8/24/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLegacyFolderViewController : UIViewController


- (IBAction)backBtnAction:(id)sender;
- (IBAction)saveBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *folderTableView;
@property (weak, nonatomic) IBOutlet UITextField *folderNameTxtF;
@property (strong, nonatomic)  NSArray *folderNameArray;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;


@end
