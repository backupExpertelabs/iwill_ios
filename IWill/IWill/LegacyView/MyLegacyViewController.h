//
//  MyLegacyViewController.h
//  IWill
//
//  Created by A1AUHAIG on 7/5/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"
#import "JVFloatLabeledTextField.h"

@interface MyLegacyViewController : UIViewController
- (IBAction)backBtnAction:(id)sender;
- (IBAction)uploadVideoBtnAction:(id)sender;
- (IBAction)uploadImageBtnAction:(id)sender;
- (IBAction)uploadAudioBtnAction:(id)sender;
- (IBAction)viewCancelBtnAction:(id)sender;;

- (IBAction)saveBtnAction:(id)sender;;

@property(weak, nonatomic) IBOutlet UIButton * saveBtn;


@property(weak, nonatomic) IBOutlet UIView * mainView;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dateTimeTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *titleTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *whoForIntendedForTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *releaseOnTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *descriptionTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *locationTxtF;

@property (weak, nonatomic) IBOutlet UITableView * albumTableView;
@property (weak, nonatomic) IBOutlet UIButton *videoBtn;
@property (weak, nonatomic) IBOutlet UIButton *imageUploadBtn;
@property (weak, nonatomic) IBOutlet UIButton *audioUploadBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIButton *videoPlayBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;


- (IBAction)videoPlayBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *audioPlayerTimerLbl;

@property (weak, nonatomic) IBOutlet UIView *audioPlayerView;
@property (weak, nonatomic) IBOutlet UIProgressView *audipPlayProgressBar;
- (IBAction)audioPlayBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *audioPlayBtn;

@end
