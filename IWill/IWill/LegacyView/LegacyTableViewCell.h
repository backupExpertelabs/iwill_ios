//
//  LegacyTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 7/6/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegacyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView * placeImageView;

@property (weak, nonatomic) IBOutlet UILabel * titleLbl;
@property (weak, nonatomic) IBOutlet UILabel * descriptionLbl;
@property (weak, nonatomic) IBOutlet UILabel * whomToLbl;
@property (weak, nonatomic) IBOutlet UILabel * releaseOnLbl;
@property (weak, nonatomic) IBOutlet UIButton * playBtn;
@property (weak, nonatomic) IBOutlet UIButton * deleteBtn;

@end
