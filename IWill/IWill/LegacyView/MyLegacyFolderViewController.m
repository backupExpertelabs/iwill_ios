//
//  MyLegacyFolderViewController.m
//  IWill
//
//  Created by A1AUHAIG on 8/24/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyLegacyFolderViewController.h"
#import "Utils.h"
#import "Constant.h"
#import "AppDelegate.h"

@interface MyLegacyFolderViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

{
    NSString * folderName;
    NSString * folderId;
    
    UIButton * tempBtn;
    AppDelegate * appD;
    
}
@end

@implementation MyLegacyFolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     appD =  (AppDelegate*)[UIApplication sharedApplication].delegate;
    folderName = @"";
    folderId = @"";
    
    _folderNameTxtF.delegate = self;
    
    _folderTableView.layer.borderWidth = 0.5;
    _folderTableView.layer.borderColor = [Utils getBorderLineColor].CGColor;
    _folderNameTxtF.layer.borderWidth = 0.5;
    _folderNameTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _folderNameTxtF.layer.cornerRadius =3;
    _folderTableView.layer.cornerRadius =3;

    _saveBtn.layer.cornerRadius =3;
    
    [self registerForKeyboardNotifications];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender
{
    
    appD.folderName = @"";
    appD.folderId = @"";
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)saveBtnAction:(id)sender
{
    
    folderName = _folderNameTxtF.text;
    
    if(folderId.length==0 && folderName.length==0)
    {
        [Utils showAlertView:@"Message" message:@"Please select any folder or Add new folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    else
    {
    
       
        appD.folderName = _folderNameTxtF.text;
        appD.folderId = folderId;
        
       [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _folderNameArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
    }
    
    UIButton * checkBtn = (UIButton*)[cell viewWithTag:100];
    UILabel * nameLbl = (UILabel*)[cell viewWithTag:200];
    nameLbl.text = [[_folderNameArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    checkBtn.accessibilityIdentifier = [[_folderNameArray objectAtIndex:indexPath.row] valueForKey:@"id"];
    [checkBtn addTarget:self action:@selector(selectFolder:) forControlEvents:UIControlEventTouchUpInside];
    
    if(folderId == [[_folderNameArray objectAtIndex:indexPath.row] valueForKey:@"id"])
    {
        [checkBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    }
    else
    {
       [checkBtn setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    }
    
    
    return cell;
    
}




-(void)selectFolder:(UIButton*)sender
{
    
    [tempBtn setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    tempBtn = sender;
    [tempBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    appD.folderId = sender.accessibilityIdentifier;
    folderId = sender.accessibilityIdentifier;
    
    
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

@end
