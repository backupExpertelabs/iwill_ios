//
//  ExecuterDetailsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/15/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface ExecuterDetailsViewController : UIViewController



@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *fullName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lastName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dob;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *address;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *relationship;


@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic)  NSDictionary *dataDict;
@property (weak, nonatomic) IBOutlet UIButton *enterBtn;

- (IBAction)enterBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;



@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;

@end
