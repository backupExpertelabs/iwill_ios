//
//  ServiceChargeDataModel.h
//  IWill
//
//  Created by A1AUHAIG on 5/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceChargeResultModel.h"

@interface ServiceChargeDataModel : NSObject


@property(nonatomic,retain) NSString *isSuccess;
@property(nonatomic,retain) NSString *message;
@property(nonatomic,retain) NSMutableArray<ServiceChargeResultModel *> *result;

-(id)initWithDataDict:(NSDictionary*)dataDict;
@end
