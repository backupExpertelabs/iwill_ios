//
//  ServiceChargeResultModel.m
//  IWill
//
//  Created by A1AUHAIG on 5/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ServiceChargeResultModel.h"

@implementation ServiceChargeResultModel


-(id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(id)initWithDataDict:(NSDictionary*)dataDict
{
    _serviceId = [dataDict valueForKey:@"id"];
    _service_type = [dataDict valueForKey:@"service_type"];
    _charge  = [dataDict valueForKey:@"charge"];
    _date  = [dataDict valueForKey:@"date"];
    
    return self;
}

@end
