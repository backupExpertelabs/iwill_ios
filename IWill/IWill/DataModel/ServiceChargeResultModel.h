//
//  ServiceChargeResultModel.h
//  IWill
//
//  Created by A1AUHAIG on 5/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceChargeResultModel : NSObject

@property(nonatomic,retain) NSString *serviceId;
@property(nonatomic,retain) NSString *service_type;
@property(nonatomic,retain) NSString *charge;
@property(nonatomic,retain) NSString *date;
@end
