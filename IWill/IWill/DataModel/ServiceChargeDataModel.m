//
//  ServiceChargeDataModel.m
//  IWill
//
//  Created by A1AUHAIG on 5/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ServiceChargeDataModel.h"

@implementation ServiceChargeDataModel

-(id)init{
    self = [super init];
    if (self) {
        _isSuccess = @"";
        _message = @"";
        _result = nil;
    }
    return self;
}


-(id)initWithDataDict:(NSDictionary*)dataDict
{
    _message = [dataDict valueForKey:@"message"];
    _isSuccess = [dataDict valueForKey:@"isSuccess"];
    _result  = [dataDict valueForKey:@"Result"];
    
    return self;
}


@end
