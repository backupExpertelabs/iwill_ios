//
//  HomeViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "HomeViewController.h"
#import "SlideNavigationController.h"
#import "Utils.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initViewWthLayer];
    [_backBtn addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
     //backBtnAction
    
    // Do any additional setup after loading the view.
}


-(void)initViewWthLayer


{
    
//    self.profileInfoBtn = [Utils buttonWithSaddowAndradius:self.profileInfoBtn];
//    self.witnessInfoBtn = [Utils buttonWithSaddowAndradius:self.witnessInfoBtn];
//    self.executiveInfoBtn = [Utils buttonWithSaddowAndradius:self.executiveInfoBtn];
    
//    self.profileInfoBtn.layer.cornerRadius= 75;
//    self.profileInfoBtn.alpha = 0.7;
    //self.profileInfoBtn.opaque = YES;
    
    self.profileInfoBtn.layer.cornerRadius = 75;
    
    self.witnessInfoBtn.layer.cornerRadius= 75;

    self.executiveInfoBtn.layer.cornerRadius= 75;

}



#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(IBAction)profileInfoBtnAction:(id)sender
{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
//    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillFuneralRecomendationViewController"];
//    [self.navigationController pushViewController:vc animated:YES];
    
}
-(IBAction)witnessInfoBtnAction:(id)sender
{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyLegacyViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(IBAction)executiveInfoBtnAction:(id)sender
{
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyTestamentViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(IBAction)lagecyBtnAction:(id)sender{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyLegacyViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}


-(IBAction)lagecyNoteBtnAction:(id)sender
{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyTestamentViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
