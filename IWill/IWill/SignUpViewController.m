//
//  SignUpViewController.m
//  IWill
//
//  Created by A1AUHAIG on 4/16/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "SignUpViewController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "Constant.h"

@interface SignUpViewController ()<UITextFieldDelegate,ConnectionManager_Delegate>

{
    ConnectionManager *connectionManager;
    UITextField * tempTxtF;
}
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    _flotingExample.keepBaseline = YES;
    [self initViewWthLayer];
    [self registerForKeyboardNotifications];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tempTxtF = textField;
    return YES;
}


-(void)initViewWthLayer


{
    //self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
    //self.loginBtnBtn = [Utils buttonWithSaddowAndradius:self.loginBtnBtn];
    self.firstNameTxt.delegate = self;
    self.lastNameTxt.delegate = self;
   self. mobileNameTxt.delegate = self;
   self. passwordText.delegate = self;
   self.confirmPasswordText.delegate = self;
   self. emailText.delegate = self;
    
    self.firstNameTxt.layer.borderWidth = 0.5;
    self.firstNameTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.firstNameTxt.layer.cornerRadius = 4;
    
    self.lastNameTxt.layer.borderWidth = 0.5;
    self.lastNameTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.lastNameTxt.layer.cornerRadius = 4;
    
    self.mobileNameTxt.layer.borderWidth = 0.5;
    self.mobileNameTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.mobileNameTxt.layer.cornerRadius = 4;
    
    self.passwordText.layer.borderWidth = 0.5;
    self.passwordText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.passwordText.layer.cornerRadius = 4;
    
    self.confirmPasswordText.layer.borderWidth = 0.5;
    self.confirmPasswordText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.confirmPasswordText.layer.cornerRadius = 4;
    
    self.emailText.layer.borderWidth = 0.5;
    self.emailText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.emailText.layer.cornerRadius = 4;
    
    self.firstNameTxt.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.lastNameTxt.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.mobileNameTxt.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.passwordText.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.confirmPasswordText.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.emailText.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    
    self.enterBtn.layer.cornerRadius = 4;

    
}


- (IBAction)enterBtnAction:(id)sender
{
    
    if([self checkIsValid])
    {
        
        
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:alertTitle message:alertNoInternetConection delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            return;
        }
        
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:_passwordText.text forKey:@"password"];
        [dataDict setValue:_emailText.text forKey:@"email"];
        [dataDict setValue:_userNameTxt.text forKey:@"mobile"];
        [dataDict setValue:_firstNameTxt.text forKey:@"first_name"];
        [dataDict setValue:_lastNameTxt.text forKey:@"last_name"];
        NSLog(@"input data %@",dataDict);
        
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"signup_user_new"];
    }
    else{
        
    }
}



#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:alertTitle message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        
        
        //        UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
        //        [self.navigationController pushViewController:vc animated:YES];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            [[NSUserDefaults standardUserDefaults]setObject:[aResponceDic valueForKey:@"Result"] forKey:@"DicKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceChargeViewController"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}
-(BOOL)checkIsValid
{
    
    if(_firstNameTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:firstNameMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_lastNameTxt.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:lastNameMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_emailText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:emailMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(![Utils validateEmail:_emailText.text])
    {
        [Utils showAlertView:alertTitle message:emailValidationMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    if(_passwordText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:passwordMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if(_confirmPasswordText.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:confirmPasswordMessage delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }
    
    
    if(![_confirmPasswordText.text isEqualToString:_passwordText.text])
    {
        [Utils showAlertView:alertTitle message:passwordShouldNotMatch delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return NO;
    }

    
    return YES;
}

- (IBAction)loginBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}






@end
