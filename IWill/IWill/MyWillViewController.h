//
//  MyWillViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"

@interface MyWillViewController : UIViewController<CMPopTipViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *simpleInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *jointInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *testamentaryInfoBtn;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic)  NSDictionary *dataDict;

-(IBAction)backBtnAction:(id)sender;
-(IBAction)simpleInfoBtnAction:(id)sender;
-(IBAction)jointInfoBtnAction:(id)sender;
-(IBAction)testamentaryInfoBtnAction:(id)sender;

/////////////////////
- (IBAction)myProfileInfoBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *myProfileInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *executerInfoBtn;
- (IBAction)executerInfoBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *myTrusteeBtn;
- (IBAction)myTrusteeBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *myPersonalItemsBtn;

- (IBAction)myPersonalItemsBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *myEstateBtn;

- (IBAction)myEstateBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addChildBtn;
- (IBAction)addChildBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *minorChildBtn;
- (IBAction)minorChildBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *funeralBtn;
- (IBAction)funeralBtnAction:(id)sender;

- (IBAction)hintBtnAction:(id)sender;

- (IBAction)myWillDetailds:(id)sender;
- (IBAction)myWillupload:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *willCreateDetailsShowView;
@property (strong, nonatomic) IBOutlet UIView *willViewShowView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *willViewShowViewHeightConstraint;


@property (strong, nonatomic) IBOutlet UITextView *willDetailsDescriptionText;
- (IBAction)willCreateDetailsShowCloseBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIImageView *profileView;
@property (weak, nonatomic) IBOutlet UIImageView *executerView;
@property (weak, nonatomic) IBOutlet UIImageView *childView;
@property (weak, nonatomic) IBOutlet UIImageView *trustyView;
@property (weak, nonatomic) IBOutlet UIImageView *personalItemView;
@property (weak, nonatomic) IBOutlet UIImageView *estateView;
@property (weak, nonatomic) IBOutlet UIImageView *minorChildView;
@property (weak, nonatomic) IBOutlet UIImageView *funeralRecomendation;




//////




@end
