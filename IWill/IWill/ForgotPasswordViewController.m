//
//  ForgotPasswordViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "ServiceChargeDataModel.h"

@interface ForgotPasswordViewController ()<UITextFieldDelegate,ConnectionManager_Delegate>

{
    ConnectionManager * connectionManager;
}
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    //self.navigationController.navigationBar.hidden = NO;
    
    //self.emailIdTxtBtn.layer.borderWidth = 1;
    self.emailIdTxtBtn.delegate = self;
    //self.submitBtn = [Utils buttonWithSaddowAndradius: self.submitBtn];
    
    self.emailIdTxtBtn.layer.borderWidth = 0.5;
    self.emailIdTxtBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.emailIdTxtBtn.layer.cornerRadius = 4;
    
    self.emailIdTxtBtn.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    self.submitBtn.layer.cornerRadius = 4;

    
   // [self hitApiTogetAllServiceList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}




//-(void)hitApiTogetAllServiceList
//{
//
//    if (![Utils isInternetAvailable])
//    {
//        [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        return;
//    }
//    [Utils startActivityIndicatorInView:self.view withMessage:@""];
//
//    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
//    NSLog(@"hdsghj %@",dataDict);
//
//    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"getServicesChagres"];
//}
//
//
//#pragma mark - Connection Manager Delegates
//
//-(void) didFailWithError:(NSError *)error {
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [Utils stopActivityIndicatorInView:self.view];
//        [Utils showAlertView:alertTitle message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
//    });
//
//
//
//}
//
//-(void)responseReceived:(id)responseObject {
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [Utils stopActivityIndicatorInView:self.view];
//
//
//        NSDictionary *aResponceDic = responseObject;
//
//        ServiceChargeDataModel * dataoooo = [[ServiceChargeDataModel alloc] init];
//
//        dataoooo = [dataoooo initWithDataDict:aResponceDic];
//
//        if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==1) {
//
//
//
//        }
//
//        else if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==0){
//
//            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
//        }
//        else{
//
//            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
//        }
//    });
//}
//
//



- (IBAction)submitBtnAction:(id)sender {


    if(self.emailIdTxtBtn.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:emailMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];

        return;
    }

    else if( ![Utils validateEmail:self.emailIdTxtBtn.text])
    {
        [Utils showAlertView:@"Message" message:emailValidationMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];

        return;
    }


    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:self.emailIdTxtBtn.text forKey:@"email"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"forgetPassword"];


}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {

    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:alertTitle message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    });



}

-(void)responseReceived:(id)responseObject {

    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];

        //        UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
        //        [self.navigationController pushViewController:vc animated:YES];

        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==1) {

            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];

        }

        else if ([[aResponceDic objectForKey:@"isSuccess"] intValue]==0){

            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{

            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}
-(void)hitAPIForForgotPassword:(NSString * )emailId
{

    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }

    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:emailId forKey:@"email"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"forgetPassword"];


}


- (IBAction)backBtnAction:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
