//
//  StartViewController.m
//  IWill
//
//  Created by A1AUHAIG on 4/13/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "StartViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface StartViewController ()<UITextFieldDelegate>

{
    UITextField* activeField;
}
@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datebackgroundView.hidden = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];

    self.dateSelectedBtn.layer.masksToBounds = NO;
    self.dateSelectedBtn.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.dateSelectedBtn.layer.shadowOpacity = 0.3;
    self.dateSelectedBtn.layer.shadowRadius = 4;
    self.dateSelectedBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    self.submitBtn.layer.masksToBounds = NO;
    self.submitBtn.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.submitBtn.layer.shadowOpacity = 0.3;
    self.submitBtn.layer.shadowRadius = 4;
    self.submitBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    
    
    
   self. nameTextF.delegate = self;
   self. genderTextF.delegate = self;
   self. phoneTextF.delegate = self;
   self. childrenTextF.delegate = self;
    self.countryTextF.delegate = self;
   self. stateTextF.delegate = self;
    self.cityTextF.delegate = self;;
    self.zipTextF.delegate = self;
    self.placeOfBirthTextF.delegate = self;
    self.motherTextF.delegate = self;
    self.fatherTextF.delegate = self;
   self. maritalStatusTextF.delegate = self;
    
    
    [self registerForKeyboardNotifications];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    if(textField == self.dobTextF)
    {
     
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}




- (IBAction)dateSelectedBtnAction:(id)sender {
    
    
     self.datebackgroundView.hidden = YES;
}



- (IBAction)submitBtnAction:(id)sender
{
    
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    CGRect frame = self.mainScrollView.frame;
//    frame.size.height -= kbSize.height;
//    CGPoint fOrigin = activeField.frame.origin;
//    fOrigin.y -= _mainScrollView.contentOffset.y;
//    fOrigin.y += activeField.frame.size.height;
//    if (!CGRectContainsPoint(frame, fOrigin) ) {
//        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y + activeField.frame.size.height - frame.size.height);
//        [_mainScrollView setContentOffset:scrollPoint animated:YES];
    
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+50, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    
    //}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}


@end
