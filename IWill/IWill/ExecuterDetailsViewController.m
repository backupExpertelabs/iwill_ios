//
//  ExecuterDetailsViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/15/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ExecuterDetailsViewController.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface ExecuterDetailsViewController ()<ConnectionManager_Delegate,UITextFieldDelegate>

{
    ConnectionManager * connectionManager;
    UITextField * activeField;
}
@end

@implementation ExecuterDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    
    _enterBtn.layer.cornerRadius =3;
    
    
    self.datebackgroundView.hidden = YES;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    
    self.iWilldatePicker.maximumDate = [NSDate date];
    
    
    [self registerForKeyboardNotifications];
    [self initViewWthLayer];
    
    
}
    -(void)initViewWthLayer
    
    
    {
        self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
        self.fullName.text = [self.dataDict valueForKey:@"first_name"];
        self.lastName.text = [self.dataDict valueForKey:@"last_name"];
        self.dob.text = [self.dataDict valueForKey:@"dob"];
        self.address.text = [self.dataDict valueForKey:@"address"];
        self.relationship.text = [self.dataDict valueForKey:@"relationship"];
        NSLog(@"hdsghj %@",self.dataDict);
        
        
        self.fullName = [Utils setTextFieldBottumBorder:self.fullName andController:self];
        self.lastName = [Utils setTextFieldBottumBorder:self.lastName andController:self];
        self.dob = [Utils setTextFieldBottumBorder:self.dob andController:self];
        self.address = [Utils setTextFieldBottumBorder:self.address andController:self];
        self.relationship = [Utils setTextFieldBottumBorder:self.relationship andController:self];

        
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(BOOL)checkTextValidation
{
    if(self.fullName.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:exicuterName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
        
    }
    
    
    if(self.address.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:exicuterEmail delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
    }
    
    
    if(self.relationship.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerAddress delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
    }
    
    
    
    if(self.dob.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerDOB delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
    }
    
    return YES;
}





- (IBAction)enterBtnAction:(id)sender
{
    
    
    if([self checkTextValidation])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
        NSMutableDictionary * excuterInfoDict = [[NSMutableDictionary alloc]init];

        [excuterInfoDict setValue:_fullName.text forKey:@"first_name"];
        [excuterInfoDict setValue:@"" forKey:@"last_name"];
        [excuterInfoDict setValue:_dob.text forKey:@"dob"];
        [excuterInfoDict setValue:_address.text forKey:@"address"];
        [excuterInfoDict setValue:_relationship.text forKey:@"relationship"];

        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        [dataDict setValue:[_dataDict valueForKey:@"executor_id"] forKey:@"executor_id"];
        [dataDict setValue:excuterInfoDict forKey:@"executor_info"];
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateExecutor"];
    }
    
    
    else{
      
        
        
    }
    
    
}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            [Utils showAlertView:alertTitle message:[aResponceDic valueForKey:@"message"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK"];
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    if(textField == self.dob)
    {
        
        _datebackgroundView.hidden = NO;
        return NO;
    }
    
    else
    {
        return YES;
    }
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+50, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    
    //}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}


-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}


- (IBAction)addOneWitBtnAction:(id)sender {
}

- (IBAction)addTwoWitBtnAction:(id)sender {
}
- (IBAction)addThreeWitBtnAction:(id)sender {
}


- (IBAction)dateSelectedBtnAction:(id)sender
{
    _dob.text = [Utils getDateFromDateForAPI:_iWilldatePicker.date];
    _datebackgroundView.hidden = YES;
}

- (IBAction)authorizationBtnAction:(UIButton*)sender
{
    if([sender isSelected])
    {
        [sender setSelected: NO];
    }
    else
    {
        [sender setSelected: YES];
    }
    
    
    
}

-(void)selectDate
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeDate];
    [alertController.view addSubview:picker];
    
    
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"OK");
        NSLog(@"%@",picker.date);
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"OK");
        NSLog(@"%@",picker.date);
    }];
    
    [alertController addAction:action];
    [alertController addAction:action1];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
