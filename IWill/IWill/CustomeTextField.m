//
//  CustomeTextField.m
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "CustomeTextField.h"

@implementation CustomeTextField


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.textColor = [UIColor whiteColor];
    [self setBottumBorder:self];
}


-(UITextField*)setBottumBorder:(UITextField*)textField
{
//    CALayer *border = [CALayer layer];
//    CGFloat borderWidth = 0.5;
//    border.borderColor = [UIColor whiteColor].CGColor;
//    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
//    border.borderWidth = borderWidth;
//    [textField.layer addSublayer:border];
//    textField.layer.masksToBounds = YES;
    
    
    CGFloat borderWidth = 0.5;
    textField.layer.borderColor = [UIColor whiteColor].CGColor;
    textField.layer.borderWidth = borderWidth;
    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    textField.layer.cornerRadius = 3;
    textField.layer.masksToBounds = YES;
    
    return textField;
}

@end
