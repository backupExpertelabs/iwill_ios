//
//  WillCreateDescriptionViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "WillCreateDescriptionViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface WillCreateDescriptionViewController ()

@property (nonatomic, strong) IBOutlet AVPlayer *avplayer;

@end

@implementation WillCreateDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addDescriptionView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)willCreateDetailsShowCloseBtnAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addDescriptionView
{
   UITextView* willDetailsDescriptionText = [[UITextView alloc]initWithFrame:CGRectMake(5, 70, self.view.frame.size.width-10, (self.view.frame.size.height/2)-35)];
    [willDetailsDescriptionText setText:@"jkdsgfhsddjsfhjsdgfjhdsgfjhdsgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjhsdgfjsdhgfjhsdgfjhsdhgfsdhfgsdfhsdhfsdhfhsdhfsdhfhsdfsdfsdfsdf\nsdfsdfsdfsdfdsfsd\ndsfsdfdsf\ndsfsdfds\nsdfdsfds\nsdfsdfs\nsdfds\nsdfdsf\nsdfdsf\nsdfdsf\nsdfds\nsdfds\nueiiuwe\n\n\n\n\n\n\nreeret\n\n\n\n\n\ttertertt\n\n\n\n\reerterteretr\n\n\n\n\terterter\n\n\n\nretertreter"];
    [self.view addSubview:willDetailsDescriptionText];
    willDetailsDescriptionText.layer.borderWidth = 2;
    willDetailsDescriptionText.layer.borderColor = [UIColor blackColor].CGColor;
    
    [self PlayWillVideo];
}



-(void)PlayWillVideo
{
    //Not affecting background music playing
    
    
   UIView* willViewShowView = [[UIView alloc]initWithFrame:CGRectMake(5, (self.view.frame.size.height/2)+35, self.view.frame.size.width-10, ((self.view.frame.size.height)/2)-70)];
    [self.view addSubview:willViewShowView];
    willViewShowView.layer.borderWidth = 2;
    willViewShowView.layer.borderColor = [UIColor blackColor].CGColor;
    

    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"xx" ofType:@"mp4"]];
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[willViewShowView bounds]];
    [willViewShowView.layer addSublayer:avPlayerLayer];
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    [self.avplayer play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.avplayer pause];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}



@end
