//
//  MainViewController.h
//  IWill
//
//  Created by A1AUHAIG on 4/13/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *logInBtn;
- (IBAction)logInBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *startNowBtn;
- (IBAction)startNowBtnAction:(id)sender;

@end
