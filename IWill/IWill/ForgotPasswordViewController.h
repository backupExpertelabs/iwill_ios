//
//  ForgotPasswordViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController
- (IBAction)submitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailIdTxtBtn;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
