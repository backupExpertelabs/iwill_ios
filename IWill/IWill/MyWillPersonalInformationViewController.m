//
//  MyWillPersonalInformationViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyWillPersonalInformationViewController.h"

@interface MyWillPersonalInformationViewController ()

@end

@implementation MyWillPersonalInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor purpleColor].CGColor;
    
    _currentAddressTextView.layer.borderWidth = borderWidth;
    _currentAddressTextView.layer.borderColor =[UIColor purpleColor].CGColor;
    
    _formerAddress1.layer.borderWidth = borderWidth;
     _formerAddress1.layer.borderColor =[UIColor purpleColor].CGColor;
    
     _formerAddress2.layer.borderWidth = borderWidth;
     _formerAddress2.layer.borderColor =[UIColor purpleColor].CGColor;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
