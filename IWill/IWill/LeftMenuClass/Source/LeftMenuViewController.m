//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "UIImage+AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@implementation LeftMenuViewController

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	//self.tableView.separatorColor = [UIColor lightGrayColor];
    
    self.nameLbl.text = @"Testaor";
    
    //NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    _profileImage.layer.cornerRadius = 45;
    _profileImage.clipsToBounds = YES;
    //self.nameLbl.text = [userData valueForKey:@"name"];
   // [_profileImage setImageWithURL:[NSURL URLWithString:@"http://iwill.digvijayjain.com/customerImages/6131526458631.png"] placeholderImage:[UIImage imageNamed:@""]];
    
//    image = "http://iwill.digvijayjain.com/customerImages/6131526458631.png";
//    mobile = "";
//    name = "a a";

	
//	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
//	self.tableView.backgroundView = imageView;
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
	view.backgroundColor = [UIColor clearColor];
	return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell"];
	
    UILabel * nameLbl = (UILabel*)[cell viewWithTag:100];
    
    
	switch (indexPath.row)
	{
		case 0:
			cell.textLabel.text = @"Home";
			break;
			
		case 1:
			cell.textLabel.text = @"Personal Info";
			break;
			
		case 2:
			cell.textLabel.text = @"Witness Info";
			break;
            
        case 3:
            cell.textLabel.text = @"Executive Info";
            break;
			
		case 4:
			cell.textLabel.text = @"Logout";
			break;
           
        case 5:
            cell.textLabel.text = @"My Child";
            break;
//            MyChildListViewController
            
            
	}
    cell.textLabel.textColor = [UIColor whiteColor];
	cell.backgroundColor = [UIColor clearColor];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UIViewController *vc ;
	
	switch (indexPath.row)
	{
		case 0:
			vc = [self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                             andCompletion:nil];
			break;
			
		case 1:
			vc = [self.storyboard instantiateViewControllerWithIdentifier: @"MyProfileViewController"];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                             andCompletion:nil];
			break;
			
		case 2:
			vc = [self.storyboard instantiateViewControllerWithIdentifier: @"AllWitnessesViewController"];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                             andCompletion:nil];
			break;
			
		case 4:
			[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
			[[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
			return;
			break;
        
        case 3:
            vc = [self.storyboard instantiateViewControllerWithIdentifier: @"ExecuterListViewController"];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                             andCompletion:nil];
            break;
            
            
        case 5:
            vc = [self.storyboard instantiateViewControllerWithIdentifier: @"MyChildListViewController"];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                             andCompletion:nil];
            break;
            
            

	}
	
//	[[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
//															 withSlideOutAnimation:self.slideOutAnimationEnabled
//																	 andCompletion:nil];
}

@end
