//
//  AddWitness2ViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AddWitness2222ViewController.h"
#import "ConnectionManager.h"
#import "Utils.h"
#import "Constant.h"
@interface AddWitness2222ViewController ()<UITextFieldDelegate,ConnectionManager_Delegate>
{
    ConnectionManager * connectionManager;
    UITextField * activeField;
}


@end

@implementation AddWitness2222ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    
    [self registerForKeyboardNotifications];
    [self initViewWthLayer];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void)initViewWthLayer


{
    self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
    
}


- (IBAction)enterBtnAction:(id)sender
{
    
    //    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AllWitnessesViewController"];
    //    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    
    if([self checkIsValid])
    {
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        
        [dataDict setValue:_firstNameText.text forKey:@"testator_id"];
        [dataDict setValue:_firstNameText.text forKey:@"first_name"];
        [dataDict setValue:_lastNameText.text forKey:@"last_name"];
        [dataDict setValue:_emailText.text forKey:@"email"];
        [dataDict setValue:_dobText.text forKey:@"dob"];
        [dataDict setValue:_addressText.text forKey:@"address"];
        [dataDict setValue:_phoneNumberText.text forKey:@"mobile"];
        [dataDict setValue:_permanentMarkText.text forKey:@"permanent_mark"];
        [dataDict setValue:_nameOfChieldText.text forKey:@"child_name"];
        [dataDict setValue:_nameOfSpouseText.text forKey:@"spouse_name"];
        [dataDict setValue:_relationshitToTestatorText.text forKey:@"reletionship"];
        [dataDict setValue:_socialSecurityNumberText.text forKey:@"security_code"];
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addWitness"];
    }
    else{
        
    }
}
#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AllWitnessesViewController"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:@"message" message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
        else{
            
            [Utils showAlertView:@"message" message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
    });
}


-(BOOL)checkIsValid
{
    
    if(_firstNameText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your First name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_lastNameText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your Last Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    
    if(_emailText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:emailMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    
    if([Utils validateEmail:_emailText.text])
    {
        [Utils showAlertView:@"Message" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_dobText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please select your DOB" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_phoneNumberText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your phone number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_addressText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your Address number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_permanentMarkText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your Permanent Mark" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    //        if(_nameOfChieldText.text.length ==0)
    //        {
    //            [Utils showAlertView:@"Message" message:@"Please enter your Chield Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            return NO;
    //        }
    //
    //        if(_nameOfSpouseText.text.length ==0)
    //        {
    //            [Utils showAlertView:@"Message" message:@"Please enter your Spouse Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            return NO;
    //        }
    if(_relationshitToTestatorText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please select Relationship" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    if(_socialSecurityNumberText.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your social security number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    
    
    return YES;
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+50, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    
    //}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}


-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}


- (IBAction)addOneWitBtnAction:(id)sender
{
    
}
- (IBAction)addTwoWitBtnAction:(id)sender
{
    
}
- (IBAction)addThreeWitBtnAction:(id)sender
{
    
}

@end
