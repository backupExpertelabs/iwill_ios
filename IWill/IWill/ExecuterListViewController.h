//
//  ExecuterListViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/15/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExecuterListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *executerTable;
@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
@property (weak, nonatomic) IBOutlet UIButton *addNewExecuterBtn;

@property (weak, nonatomic) IBOutlet UIButton *addNewExecuter;

- (IBAction)enterBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;
- (IBAction)addNewExecuterBtnAction:(id)sender;


@end
