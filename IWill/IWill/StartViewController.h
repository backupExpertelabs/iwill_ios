//
//  StartViewController.h
//  IWill
//
//  Created by A1AUHAIG on 4/13/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;


- (IBAction)submitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;


////////////////
@property (weak, nonatomic) IBOutlet UITextField *dobTextF;
@property (weak, nonatomic) IBOutlet UITextField *nameTextF;
@property (weak, nonatomic) IBOutlet UITextField *genderTextF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextF;
@property (weak, nonatomic) IBOutlet UITextField *childrenTextF;
@property (weak, nonatomic) IBOutlet UITextField *countryTextF;
@property (weak, nonatomic) IBOutlet UITextField *stateTextF;
@property (weak, nonatomic) IBOutlet UITextField *cityTextF;
@property (weak, nonatomic) IBOutlet UITextField *zipTextF;
@property (weak, nonatomic) IBOutlet UITextField *placeOfBirthTextF;
@property (weak, nonatomic) IBOutlet UITextField *motherTextF;
@property (weak, nonatomic) IBOutlet UITextField *fatherTextF;
@property (weak, nonatomic) IBOutlet UITextField *maritalStatusTextF;

@end
