//
//  LoginViewController.h
//  IWill
//
//  Created by A1AUHAIG on 4/13/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
- (IBAction)enterBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;

- (IBAction)forgotPasswordBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *userTxt;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *passTxt;
- (IBAction)signupBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmet;
- (IBAction)segmenyAction:(id)sender;

@end
