//
//  MyProfileViewController.h
//  IWill
//
//  Created by A1AUHAIG on 4/27/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomeTextField.h"
#import "JVFloatLabeledTextField.h"

@interface MyProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *datebackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dateMainView;
@property (weak, nonatomic) IBOutlet UIDatePicker *iWilldatePicker;
- (IBAction)dateSelectedBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectedBtn;


- (IBAction)submitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;


////////////////
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dobTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nameTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *genderTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *childrenTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *countryTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *stateTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *cityTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *zipTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *addressTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *placeOfBirthTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *motherTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *fatherTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *maritalStatusTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *firstChildNameTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lastNameTxtF;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *ssnTextF;



@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *spouseNameTextF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *spouseDOBTxtF;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *bodyMarkingTextF;

@property (weak, nonatomic) IBOutlet UIButton *profilePicBtn;
- (IBAction)profilePicBtnAction:(id)sender;

@property(strong, nonatomic)NSDictionary * dataDict;



@end
