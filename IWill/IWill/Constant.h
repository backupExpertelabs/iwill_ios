//
//  Constant.h
//  IWill
//
//  Created by A1AUHAIG on 4/26/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


/////////  Witness Message ////////
#define  emailMessage                            @"Please enter your Email."
#define  emailValidationMessage                  @"Please enter a valid Email."
#define firstNameMessage                         @"Please enter your First Name."
#define lastNameMessage                          @"Please enter your Last Name."
#define dOBMessage                               @"Please enter your Date Of Birth."
#define addressMessage                           @"Please enter your Address."
#define phoneNumberMessage                       @"Please enter your Phone Number."
#define permanentMarkMessage                     @"Please enter your PermanentMark."
#define chieldNameMessage                        @"Please enter your Chield Name."
#define spouseMessage                            @"Please enter your Spouce."
#define relationShipToTestatorMessage            @"Please enter your Relationship."
#define socialSecurityNumberMessage              @"Please enter your last 4 digit Social Security Number."
#define socialSecurityNumberValidationMessage    @"Social Security Number is minium 4 digit."

////////////////////////////////////////////////

//////////// Login Message ///////////////

#define passwordMessage                @"Please enter your password."
#define confirmPasswordMessage         @"Please enter your confirm password."
#define passwordShouldNotMatch         @"Your password should not match."


////////////////////////////////////////////////


//////////// Sign Up Message ///////////////




////////////////////////////////////////////////

 //////////   App Name /////////

#define appName               @"iWill"

 /////////////////   Alert View Constant ///////////


#define alertTitle             @"Message"
#define  alertOkButtonTitle    @"OK"
#define alertCancelButtonTitle @"Cancel"
#define  alertYesButtonTitle   @"YES"
#define alertNOButtonTitle     @"NO"


///////////////////////   No Internet Message /////////

#define alertNoInternetConection @"Sorry,No Internet, please try again !"

////////////////////  MY Profile Message /////////////

#define profileNumberOfchildren @"Please enter your number of children."
#define profileAddress          @"Please enter your address field."
#define profileCity             @"Please enter your city."
#define profileState            @"Please enter your state."
#define profileCountry          @"Please enter your country."
#define profileZipCode          @"please enter your zip code."
#define profilePlaceOfBirth     @"Please enter your place of birth."
#define profileDOB              @"Please enter your date of birth."
#define profileMotherName       @"Please enter your mother name."
#define profileFatherName       @"Please enter your father name."
#define profileMaritalStatus    @"Please select your marital status."
#define profileSSN              @"Please enter your social security number."
#define profileFirstChildName   @"Please enter your first child name."
#define profileSSNValidation    @"Social security number should not less than 4 digit."

#define profilePicCheck              @"Please select profile image"





/////////////////////////   Add Witness Message ////////////////////


#define witnessFirstName        @"Please enter witness first name."
#define witnessLastName         @"Please enter witness last name."
#define witnessDOB              @"Please enter witness date of birth."
#define witnessRelationship     @"Please enter witness relationship."
#define witnessEmail            @"Please enter witness email."
#define witnessPhoneNumber      @"Please enter witness phone number."
#define witnessPermanentMark    @"Please enter witness Tattoo/Permanent mark."
#define witnessSpouseName       @"Please enter witness spouse's name"
#define witnessChildName        @"Please enter witness child name."
#define withnessSSNNumber       @"Please enter withness last 4 digit social security number."
#define witnessSSNNumberValidation @"Witness social security number should not less then 4 digit."
#define witnessDeleteMessage    @"Do you want to delete this witness"
#define witnessCountZeroORNowitnessAdded @"You have no witness. Please add atleast two witness."





///////////////////////    Executer Message Constant    ///////////////////

#define exicuterName            @"Please enter executer name."
#define exicuterLastName        @"Please enter executer last name."
#define exicuterEmail           @"Please enter executer Email."
#define executerDOB             @"Please enter executer date of birth"
#define executerAddress         @"Please enter executer address."
#define executerChildName       @"Please enter executer oldest child name"
#define executerRelationship    @"Please enter executer relationship."
#define exicuterProfilePic      @"Please add executer profile pic."
#define executerBodyMark        @"Please enter executer permanent/tattoo mark."
#define executerSSN             @"Please enter executer 4 digit social security number."
#define executerAge             @"Please enter executer age."
#define executerIsAuthorisation @"Please select authorization."
#define executerDeleteMessage    @"Do you want to delete this executer"
#define executerCountZeroORNowitnessAdded @"You have no executer. Please add atleast one executer."


       //////////////    Trusty  Message ///////////
#define trusteeDeleteMessage    @"Do you want to delete this Trusty"

//////////////    child  Message ///////////
#define childDeleteMessage    @"Do you want to delete this child"



#endif /* Constant_h */
