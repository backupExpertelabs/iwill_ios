//
//  WillCreateDescriptionViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WillCreateDescriptionViewController : UIViewController


- (IBAction)willCreateDetailsShowCloseBtnAction:(id)sender;

@end
