//
//  ServiceChargeViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/25/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceChargeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *serviceChargeTable;

- (IBAction)backBtnAction:(id)sender;
- (IBAction)skipBtnAction:(id)sender;

@end
