//
//  AppDelegate.h
//  IWill
//
//  Created by A1AUHAIG on 4/12/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *folderName;
@property (strong, nonatomic) NSString *folderId;


@end

