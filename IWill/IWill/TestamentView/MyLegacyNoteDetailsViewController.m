//
//  MyLegacyNoteDetailsViewController.m
//  IWill
//
//  Created by A1AUHAIG on 8/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyLegacyNoteDetailsViewController.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"


@interface MyLegacyNoteDetailsViewController ()<UITextFieldDelegate,UITextViewDelegate,ConnectionManager_Delegate>

{
    ConnectionManager *connectionManager;
}
@end

@implementation MyLegacyNoteDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    if(!_isNewFileUploded)
    {
    _noteTxtF.text = [self.lagecyFileData valueForKey:@"note"];
    _releaseToTxtF.text = [self.lagecyFileData valueForKey:@"release_to"];
    _descriptionTxtF.text = [self.lagecyFileData valueForKey:@"message"];
    
    }
    _noteTxtF = [Utils setTextFieldBottumBorder:_noteTxtF andController:self];
    _releaseToTxtF = [Utils setTextFieldBottumBorder:_releaseToTxtF andController:self];
    _descriptionTxtF = (JVFloatLabeledTextView*)[Utils setTextFieldBottumBorder:_descriptionTxtF andController:self] ;

    
    
//    self.descriptionTxtF.layer.borderWidth = 0.5;
//    self.descriptionTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
//
//    self.releaseToTxtF.layer.borderWidth = 0.5;
//    self.releaseToTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
//
//    self.noteTxtF.layer.borderWidth = 0.5;
//    self.noteTxtF.layer.borderColor = [UIColor whiteColor].CGColor;
    _saveBtn.layer.cornerRadius =3;
    _cancelBtn.layer.cornerRadius =3;


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)cancelBtnAction:(id)sender
{
    
    [self  dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)saveBtnAction:(id)sender
{
    if(_isNewFileUploded)
    {
        [self saveNewLegacyFile];
    }
    else
    {
    [self saveLegacyFile];
    }
    
}
- (IBAction)backBtnAction:(id)sender
{
    [self  dismissViewControllerAnimated:YES completion:nil];
    
}




-(BOOL)validateTextF
{
    
    if(_noteTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your note." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    if(_releaseToTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your release note." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_descriptionTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your desctiption" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    
    
    return YES;
}


-(void)saveLegacyFile
{
    
    
    if(![self validateTextF])
    {
        return;
    }
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    
    
    NSMutableDictionary * legacyNoteDict = [[NSMutableDictionary alloc] init];
    
    //    "testator_id": 1,
    //    "legacy_notes": [
    //                     {
    //                         "note": "yogendra",
    //                         "release_to": "7017734526",
    //                         "message": "brotherbrother",
    //                         "comment": "brotherbrother"
    //                     },
    
    
    
    
    
    //    "testator_id": 1,
    //    "note_id": 2,
    //    "legacy_note": {
    //        "note": "yogendra",
    //        "release_to": "7017734526",
    //        "message": "brotherbrother",
    //        "comment": "brotherbrother"
    //    }
    
    
    
    [legacyNoteDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [legacyNoteDict setValue:[_lagecyFileData valueForKey:@"id"] forKey:@"note_id"];
    
    //NSMutableArray * legacyNoteArr = [[NSMutableArray alloc] init];
    NSMutableDictionary * legacyNote = [[NSMutableDictionary alloc] init];
    [legacyNote setValue:_noteTxtF.text forKey:@"note"];
    [legacyNote setValue:_releaseToTxtF.text forKey:@"release_to"];
    [legacyNote setValue:_descriptionTxtF.text forKey:@"message"];
    [legacyNote setValue:@"" forKey:@"comment"];
   // [legacyNoteArr addObject:legacyNote];
    
    [legacyNoteDict setValue:legacyNote forKey:@"legacy_note"];
    
    
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:legacyNoteDict andDelegate:self andURL:@"updateNote"];
}

-(void)deteteBtnAction:(UIButton*)sender
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"updateNoteC"])
            {
                 [self  dismissViewControllerAnimated:YES completion:nil];
            }
            
            else if([[aResponceDic valueForKey:@"method"] isEqualToString:@"saveLegacyNotesC"]) {
                
                [self  dismissViewControllerAnimated:YES completion:nil];

            }
        }
        
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}




//-(BOOL)validateTextF
//{
//    
//    if(_noteTxtF.text.length ==0)
//    {
//        [Utils showAlertView:@"Message" message:@"Please enter your note." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        return NO;
//    }
//    if(_releaseToTxtF.text.length ==0)
//    {
//        [Utils showAlertView:@"Message" message:@"Please enter your release note." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        return NO;
//    }
//    
//    if(_descriptionTxtF.text.length ==0)
//    {
//        [Utils showAlertView:@"Message" message:@"Please enter your desctiption" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        return NO;
//    }
//    
//    return YES;
//}


-(void)saveNewLegacyFile
{
    
    
    if(![self validateTextF])
    {
        return;
    }
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * legacyNoteDict = [[NSMutableDictionary alloc] init];
    [legacyNoteDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSMutableArray * legacyNoteArr = [[NSMutableArray alloc] init];
    NSMutableDictionary * legacyNote = [[NSMutableDictionary alloc] init];
    [legacyNote setValue:_noteTxtF.text forKey:@"note"];
    [legacyNote setValue:_releaseToTxtF.text forKey:@"release_to"];
    [legacyNote setValue:_descriptionTxtF.text forKey:@"message"];
    [legacyNote setValue:@"" forKey:@"comment"];
    [legacyNoteArr addObject:legacyNote];
    [legacyNoteDict setValue:legacyNoteArr forKey:@"legacy_notes"];
    [connectionManager getDataFromServerWithSessionTastmanager:@"saveLegacyNotes" withInput:legacyNoteDict andDelegate:self andURL:@"saveLegacyNotes"];
}




@end
