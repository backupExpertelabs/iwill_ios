//
//  MyTestamentViewController.h
//  IWill
//
//  Created by A1AUHAIG on 7/5/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"
#import "JVFloatLabeledTextField.h"

@interface MyTestamentViewController : UIViewController
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *mytestamentTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
- (IBAction)uploadNewNoteBtnAction:(id)sender;
- (IBAction)searchBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *notesView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *noteTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *releaseToTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *descriptionTxtF;
- (IBAction)cancelBtnAction:(id)sender;
- (IBAction)saveBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *uploadNewNoteBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;








@end
