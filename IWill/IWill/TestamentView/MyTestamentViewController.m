
//  MyTestamentViewController.m
//  IWill
//
//  Created by A1AUHAIG on 7/5/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyTestamentViewController.h"
#import "TestamentTableViewCell.h"
#import "Utils.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "MyLegacyNoteDetailsViewController.h"

@interface MyTestamentViewController ()<UISearchControllerDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,ConnectionManager_Delegate>

{
    NSMutableArray *notesArray;
    NSMutableArray *searchNotestResults;
    BOOL isEdited;
    BOOL isSearch;
    ConnectionManager * connectionManager;
    NSString * lagacyNotesId;
    
}
@end

@implementation MyTestamentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearch = NO;
    notesArray = [[NSMutableArray alloc] init];
    searchNotestResults = [[NSMutableArray alloc] init];
    _mytestamentTableView.delegate = self;
    _mytestamentTableView.dataSource = self;
    _searchBar.delegate = self;
    _searchBar.showsCancelButton = YES;
    _searchBar.backgroundColor =[Utils getBorderLineColor];
    _noteTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_noteTxtF];
    
    //    _releaseToTxtF = [Utils setBottumBorderOnJVFloatLabeledTextField:_releaseToTxtF];
    
    self.descriptionTxtF.delegate = self;
    self.releaseToTxtF.delegate = self;
    self.noteTxtF.delegate = self;
    
    
    
    self.descriptionTxtF.layer.borderWidth = 1;
    self.descriptionTxtF.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.releaseToTxtF.layer.borderWidth = 1;
    self.releaseToTxtF.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.noteTxtF.layer.borderWidth = 1;
    self.noteTxtF.layer.borderColor = [UIColor grayColor].CGColor;
    
    
    _saveBtn.layer.cornerRadius =3;
    _uploadNewNoteBtn.layer.cornerRadius =3;
    //_descriptionTxtF = (JVFloatLabeledTextView*)[Utils setBottumBorderOnJVFloatLabeledTextField:_descriptionTxtF];
    
    // Do any additional setup after loading the view.
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    //[self getAllLegacyFile];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [self getAllLegacyFile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)uploadNewNoteBtnAction:(id)sender
{
//    _notesView.hidden = NO;
//    isEdited = NO;
//    _noteTxtF.text = @"";
//    _releaseToTxtF.text = @"";
//    _descriptionTxtF.text = @"";
    
    MyLegacyNoteDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyLegacyNoteDetailsViewController"];
    //vc.lagecyFileData = (NSMutableDictionary*)dataDict;
    vc.isNewFileUploded = YES;
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    
}

- (IBAction)searchBtnAction:(id)sender
{
    
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    searchNotestResults = (NSMutableArray*)[notesArray filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text

{
    isSearch = YES;
    searchNotestResults = (NSMutableArray*)[notesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(note contains[c] %@)", [NSString stringWithFormat:@"%@%@",searchBar.text,text]]];
    
    //if(searchBar.text)
    
    [_mytestamentTableView reloadData];
    return YES;
}




- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    //    if (![searchText isEqualToString:self.searchText]) {
    //        self.searchText = searchText;
    //        return;
    //    }
    NSLog(@"Clear clicked");
}


-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSLog(@"Clear llllllllllllll");
    [textField resignFirstResponder];
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"sesesesesesese");
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar

{
    NSLog(@"searchBarCancelButtonClicked");
    [searchBar resignFirstResponder];

    isSearch = NO;
    [_mytestamentTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isSearch)
    {
        return searchNotestResults.count;
    }
    else
        
    {
        
        return notesArray.count;
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    TestamentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[TestamentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:MyIdentifier];
    }
    
    
    if(isSearch)
    {
        NSDictionary * dataDic  = [searchNotestResults objectAtIndex:indexPath.row];
        cell.noteName.text = [[dataDic valueForKey:@"note"] isKindOfClass:[NSNull class]] ? @"":[dataDic valueForKey:@"note"];
        cell.noteDescription.text = [[dataDic valueForKey:@"message"] isKindOfClass:[NSNull class]] ? @"":[dataDic valueForKey:@"message"];
        cell.noteReleaseOn.text = [[dataDic valueForKey:@"release_to"] isKindOfClass:[NSNull class]] ? @"":[dataDic valueForKey:@"release_to"];
        cell.deleteBtn.tag = [[dataDic valueForKey:@"id"] integerValue];
    }
    
    else{
        NSDictionary * dataDic  = [notesArray objectAtIndex:indexPath.row];
        cell.noteName.text = [[dataDic valueForKey:@"note"] isKindOfClass:[NSNull class]] ? @"":[dataDic valueForKey:@"note"];
        cell.noteDescription.text = [[dataDic valueForKey:@"message"] isKindOfClass:[NSNull class]] ? @"":[dataDic valueForKey:@"message"];
        cell.noteReleaseOn.text = [[dataDic valueForKey:@"release_to"] isKindOfClass:[NSNull class]] ? @"":[dataDic valueForKey:@"release_to"];
        cell.deleteBtn.tag = [[dataDic valueForKey:@"id"] integerValue];
    }
    
    
    [cell.deleteBtn addTarget:self action:@selector(deleteNotes:) forControlEvents:UIControlEventTouchUpInside];
   // cell.mainBackView = [self setShadow:cell.mainBackView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    return 80;
//
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary * dataDict ;
    if(isSearch)
    {
        dataDict = [searchNotestResults objectAtIndex:indexPath.row];
        
    }
    else{
        dataDict = [notesArray objectAtIndex:indexPath.row];
        
    }
    //dataDict = [notesArray objectAtIndex:indexPath.row];
    
//     isEdited = YES;
//    _notesView.hidden = NO;
//    _noteTxtF.text = [dataDict valueForKey:@"note"];
//    _releaseToTxtF.text = [dataDict valueForKey:@"release_to"];
//    _descriptionTxtF.text = [dataDict valueForKey:@"message"];
    
    MyLegacyNoteDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyLegacyNoteDetailsViewController"];
    vc.lagecyFileData = (NSMutableDictionary*)dataDict;
    vc.isNewFileUploded = NO;
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    
    
    
}




-(void)deleteNotes:(UIButton*)sender
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:@"Are you sure, You want to delete" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             [self deleteNoteData:sender];
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];

}


-(void)deleteNoteData:(UIButton*)sender

{
 
    lagacyNotesId = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    
    //getLegacyNotes/testator_id
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@/%@",@"deleteLegacyNote",[userData valueForKey:@"id"],lagacyNotesId];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
    
    
    
}

- (IBAction)cancelBtnAction:(id)sender {
    
    _notesView.hidden = YES;
    
}

- (IBAction)saveBtnAction:(id)sender
{
//    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
//
//    [dataDict setValue:_noteTxtF.text forKey:@"note"];
//    [dataDict setValue:_releaseToTxtF.text forKey:@"release"];
//    [dataDict setValue:_descriptionTxtF.text forKey:@"description"];
//
//    [notesArray addObject:dataDict];
//
//    [_mytestamentTableView reloadData];
//    _notesView.hidden = YES;
    
    [self saveLegacyFile];
    
}

-(BOOL)validateTextF
{
    
    if(_noteTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your note." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    if(_releaseToTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your release note." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    if(_descriptionTxtF.text.length ==0)
    {
        [Utils showAlertView:@"Message" message:@"Please enter your desctiption" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    
    return YES;
}


-(void)saveLegacyFile
{
    
    
    if(![self validateTextF])
    {
        return;
    }
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    
    
    NSMutableDictionary * legacyNoteDict = [[NSMutableDictionary alloc] init];
    
    //    "testator_id": 1,
    //    "legacy_notes": [
    //                     {
    //                         "note": "yogendra",
    //                         "release_to": "7017734526",
    //                         "message": "brotherbrother",
    //                         "comment": "brotherbrother"
    //                     },
    
    [legacyNoteDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    NSMutableArray * legacyNoteArr = [[NSMutableArray alloc] init];
    NSMutableDictionary * legacyNote = [[NSMutableDictionary alloc] init];
    [legacyNote setValue:_noteTxtF.text forKey:@"note"];
    [legacyNote setValue:_releaseToTxtF.text forKey:@"release_to"];
    [legacyNote setValue:_descriptionTxtF.text forKey:@"message"];
    [legacyNote setValue:@"" forKey:@"comment"];
    [legacyNoteArr addObject:legacyNote];
    
    [legacyNoteDict setValue:legacyNoteArr forKey:@"legacy_notes"];
    
    
    [connectionManager getDataFromServerWithSessionTastmanager:@"saveLegacyNotes" withInput:legacyNoteDict andDelegate:self andURL:@"saveLegacyNotes"];
}


-(UIView*)setShadow:(UIView*)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.borderWidth = 1;
    view.layer.borderColor = [UIColor blackColor].CGColor;
    view.layer.shadowPath = shadowPath.CGPath;
    return view;
}



-(void)deteteBtnAction:(UIButton*)sender
{
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)getAllLegacyFile
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    // http://localhost/iwill/index.php/getChilds/1
    
    //getLegacyNotes/testator_id
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@",@"getLegacyNotes",[userData valueForKey:@"id"]];
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"getLegacyNotesC"])
            {
                 notesArray = [aResponceDic valueForKey:@"Result"];
                [_mytestamentTableView reloadData];
            }
            
            else if([[aResponceDic valueForKey:@"method"] isEqualToString:@"saveLegacyNotesC"]) {
               
                _notesView .hidden = YES;
                
                [self getAllLegacyFile];
                
                
            }
            
            
            else if([[aResponceDic valueForKey:@"method"] isEqualToString:@"deleteLegacyNote"]) {
                
                [self getAllLegacyFile];
                
                
            }
        }
        
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


@end
