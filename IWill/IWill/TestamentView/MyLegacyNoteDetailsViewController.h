//
//  MyLegacyNoteDetailsViewController.h
//  IWill
//
//  Created by A1AUHAIG on 8/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"
#import "JVFloatLabeledTextField.h"

@interface MyLegacyNoteDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *noteTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *releaseToTxtF;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *descriptionTxtF;
- (IBAction)cancelBtnAction:(id)sender;
- (IBAction)saveBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property(nonatomic)BOOL isNewFileUploded;

@property (strong, nonatomic)  NSMutableDictionary *lagecyFileData;

@end
