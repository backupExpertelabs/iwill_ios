//
//  TestamentTableViewCell.h
//  IWill
//
//  Created by A1AUHAIG on 7/9/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestamentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView * mainBackView;
@property (weak, nonatomic) IBOutlet UILabel * noteName;
@property (weak, nonatomic) IBOutlet UILabel * noteReleaseOn;
@property (weak, nonatomic) IBOutlet UILabel * noteDescription;
@property (weak, nonatomic) IBOutlet UIButton * deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView * backImage;



@end
