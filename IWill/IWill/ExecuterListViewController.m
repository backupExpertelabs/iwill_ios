//
//  ExecuterListViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/15/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ExecuterListViewController.h"
#import "ConnectionManager.h"
#import "WitnessTableViewCell.h"
#import "Utils.h"
#import "ExecuterDetailsViewController.h"
#import "Constant.h"
#import "ExecuterAddNewViewController.h"

@interface ExecuterListViewController ()<ConnectionManager_Delegate>

{
    NSMutableArray * allExecuterArray;
    ConnectionManager * connectionManager;
    NSString * executerID;
}
@end

@implementation ExecuterListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    allExecuterArray = [[NSMutableArray alloc] init];
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
//    [self callAPIForAllExecuter];
    _addNewExecuter.layer.cornerRadius =3;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)enterBtnAction:(id)sender
{
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecuterAddNewViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return allExecuterArray.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    WitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[WitnessTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary * dataDict = [allExecuterArray objectAtIndex:indexPath.row];
    
    cell.firstNameText.text = [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"first_name"]];
    cell.relationshipNa.text = [dataDict valueForKey:@"relationship"];
    
    if([[dataDict valueForKey:@"type"] integerValue] ==1)
    {
        cell.executerType.text = @"Primary";
    }
    if([[dataDict valueForKey:@"type"] integerValue] ==2)
    {
        cell.executerType.text = @"Secondary";
    }
    if([[dataDict valueForKey:@"type"] integerValue] ==3)
    {
        cell.executerType.text = @"Backup";
    }
    //cell.executerType.text =[dataDict valueForKey:@"type"];
    //cell.executerType.text = @"";
    
    cell.deleteWitnessBtn.tag = indexPath.row;
    
    [cell.deleteWitnessBtn addTarget:self action:@selector(deleteWitnessAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    ExecuterAddNewViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecuterAddNewViewController"];
    vc.executerList = allExecuterArray;
    vc.executerCount = [NSString stringWithFormat:@"%u",allExecuterArray.count+1];    [self.navigationController pushViewController:vc animated:YES];
    
    
//    ExecuterDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecuterDetailsViewController"];
//    vc.dataDict = [allExecuterArray objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [self callAPIForAllExecuter];
}


-(void)initViewWthLayer

{
    self.enterBtn = [Utils buttonWithSaddowAndradius:self.enterBtn];
    
}

-(void)callAPIForAllExecuter
{
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSLog(@"hdsghj %@",dataDict);
    //[connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"getExecuter"];
     NSString * urlStr = [NSString stringWithFormat:@"%@/%@",@"getExecutors",[userData valueForKey:@"id"]];
    
   // [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"getExecuter"];
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
    
}


#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}


-(void)responseReceivedWithSecondMethod:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            if([[aResponceDic valueForKey:@"method"]isEqualToString:@"getExecutorsC"])
            {
            
            allExecuterArray = [aResponceDic valueForKey:@"Result"];
            [_executerTable reloadData];
            }
            
            else
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                
                [self callAPIForAllExecuter];
            }
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
    
    
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            if([[aResponceDic valueForKey:@"method"]isEqualToString:@"getExecutorsC"])
            {
                
                allExecuterArray = [aResponceDic valueForKey:@"Result"];
                [_executerTable reloadData];
                if(allExecuterArray.count ==3)
                {
                    _addNewExecuter.hidden = YES;
                }
            }
            else
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                
                [self callAPIForAllExecuter];
            }
            
            
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            //            [Utils showAlertView:alertTitle message:@"You have no witness. Please add atleast two witness." delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:executerCountZeroORNowitnessAdded delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        ExecuterAddNewViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecuterAddNewViewController"];
        vc.executerCount = [NSString stringWithFormat:@"%u",allExecuterArray.count+1];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    if(alertView.tag == 200 && buttonIndex ==1)
    {
        [self deleteWitnessAPI];
    }
    
}


-(void)deleteWitnessAction:(UIButton*)sender
{
    
    
//    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
//    
//    NSString * witnessCount = [userData valueForKey:@"countWitnesses"];
//    
//    if(witnessCount.intValue < 3 )
//    {
//        
//    }
//    
//    
//    else{
        NSDictionary* dataDict = [allExecuterArray objectAtIndex:sender.tag];
        executerID = [dataDict valueForKey:@"executor_id"];
        [Utils showAlertViewWithTag:200 title:alertTitle message:executerDeleteMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
    //}
}


-(void)deleteWitnessAPI
{
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    
    NSLog(@"hdsghj %@",dataDict);
    
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@/%@",@"deleteExecutor",[userData valueForKey:@"id"],executerID];
    
    // [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"getExecuter"];
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:nil andDelegate:self andURL:urlStr];
    
    //[connectionManager getDataFromServerWithOtherResponseSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"deleteExecutor"];
    
}


- (IBAction)addNewExecuterBtnAction:(id)sender
{
    ExecuterAddNewViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecuterAddNewViewController"];
     vc.executerList = allExecuterArray;
    vc.executerCount = [NSString stringWithFormat:@"%u",allExecuterArray.count+1];    [self.navigationController pushViewController:vc animated:YES];
}



@end
