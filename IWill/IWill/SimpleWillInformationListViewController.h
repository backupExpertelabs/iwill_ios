//
//  SimpleWillInformationListViewController.h
//  IWill
//
//  Created by A1AUHAIG on 5/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleWillInformationListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)backBtnAction:(id)sender;

@property(nonatomic,retain) IBOutlet UITableView * simpleWillInformationTable;
@end
