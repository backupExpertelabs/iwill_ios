//
//  MainViewController.m
//  IWill
//
//  Created by A1AUHAIG on 4/13/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MainViewController.h"
#import <AVFoundation/AVFoundation.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    [self onCreateMoreButtons];
    
    [self initViewWthLayer];
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"xx" ofType:@"mp4"]];
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.avplayer pause];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}



-(void)initViewWthLayer
{
    self.logInBtn.layer.borderWidth = 1;
    self.logInBtn.layer.borderColor = [UIColor purpleColor].CGColor;
    self.startNowBtn.layer.borderWidth = 1;
    self.startNowBtn.layer.borderColor = [UIColor purpleColor].CGColor;
    self.startNowBtn.titleLabel.textColor = [UIColor whiteColor];
    self.logInBtn.titleLabel.textColor = [UIColor whiteColor];
}


-(IBAction)onCreateMoreButtons
{
    
    _bottomView.backgroundColor = [UIColor purpleColor];
    float width = self.view.frame.size.width/5;
    float height = 64;
    float x = 0;
    NSArray * imageA = [[NSArray alloc]initWithObjects:@"menu_icon.png",@"menu_icon.png",@"menu_icon.png",@"menu_icon.png",@"menu_icon.png", nil];
    int count =0;
    for (NSString* nameTitle in @[@"Menu", @"About", @"Why Us",@"Demo",@"Contact"]) {
        
        UIView * view11 = [[UIView alloc]initWithFrame:CGRectMake(x, 0, width, height)];
        [_bottomView addSubview:view11];
        UIImageView * viewImage = [[UIImageView alloc]initWithFrame:CGRectMake(17, 3, 30, 30)];
        viewImage.image = [UIImage imageNamed:[imageA objectAtIndex:count]];
        [view11 addSubview:viewImage];
        
        viewImage.center = CGPointMake(view11.frame.size.width  / 2,
                                       15);
        UILabel * nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, view11.frame.size.width, 40)];
        nameLbl.text = nameTitle;
        nameLbl.numberOfLines = 0;
        nameLbl.textAlignment = NSTextAlignmentCenter;
        nameLbl.font = [UIFont systemFontOfSize:13 weight:0];
        nameLbl.textColor = [UIColor whiteColor];
        nameLbl.lineBreakMode = NSLineBreakByWordWrapping;
        [view11 addSubview:nameLbl];
        count +=1;
        x = x+width;
    }
}


- (IBAction)logInBtnAction:(id)sender {
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)startNowBtnAction:(id)sender {
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"StartViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
