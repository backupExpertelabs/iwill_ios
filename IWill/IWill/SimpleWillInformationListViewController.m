//
//  SimpleWillInformationListViewController.m
//  IWill
//
//  Created by A1AUHAIG on 5/21/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "SimpleWillInformationListViewController.h"
#import "WitnessTableViewCell.h"
#import "MyWillPersonalInformationViewController.h"


@interface SimpleWillInformationListViewController ()

{
    NSMutableArray * allWillInformationArray;
}
@end

@implementation SimpleWillInformationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    allWillInformationArray = [[NSMutableArray alloc] init];
    
    [allWillInformationArray addObject:@"PERSONAL INFORMATION"];
    [allWillInformationArray addObject:@"BUSINESS OR EMPLOYMENT"];
    [allWillInformationArray addObject:@"FUNERAL REQUESTS"];
    [allWillInformationArray addObject:@"CURRENT LAST WILL AND TESTAMENT OR LIVING TRUST, IF ANY"];
    
    [allWillInformationArray addObject:@"POWER OF ATTORNEY"];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return allWillInformationArray.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    WitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[WitnessTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    cell.backView.layer.cornerRadius = 2;
    cell.backView.layer.masksToBounds = NO;
    cell.backView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    cell.backView.layer.shadowOpacity = 0.3;
    cell.backView.layer.shadowRadius = 4;
    cell.backView.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    
    cell.firstNameText.text = [allWillInformationArray objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyWillPersonalInformationViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyWillPersonalInformationViewController"];
    
   
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
