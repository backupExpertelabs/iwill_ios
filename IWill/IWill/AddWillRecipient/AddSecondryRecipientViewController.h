//
//  AddSecondryRecipientViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/12/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrustryTableViewCell.h"

#import "CMPopTipView.h"

@interface AddSecondryRecipientViewController : UIViewController<CMPopTipViewDelegate>
- (IBAction)backBtnAction:(id)sender;
- (IBAction)addMoreBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UITableView * recipientSecondaryTableView;
@property(nonatomic,strong) NSMutableDictionary * recipientSecondry;
- (IBAction)saveBtnAction:(id)sender;
@property(nonatomic,strong) NSString * banificiaryType;
@property(nonatomic,strong) IBOutlet UIButton * saveBtn;

@end
