//
//  MyChildRecipientViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"

@interface MyChildRecipientViewController : UIViewController<CMPopTipViewDelegate>
- (IBAction)backBtnAction:(id)sender;
- (IBAction)addMoreTrusteeBtnAction:(id)sender;

-(IBAction)updateChildRecientValue:(UIButton*)sender;

@property(nonatomic,weak)IBOutlet UITableView * recipientChildTableView;
@property(nonatomic,strong) NSMutableDictionary * recipientChild;
@property(nonatomic,strong) NSString * banificiaryType;
@property(nonatomic,weak)IBOutlet UIButton * saveBtn;



@end
