//
//  AddOtherRecipientViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/11/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "AddOtherRecipientViewController.h"
#import "TrustryTableViewCell.h"
#import "JVFloatLabeledTextView.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "Constant.h"
@interface AddOtherRecipientViewController ()<ConnectionManager_Delegate,UITextFieldDelegate,UITextViewDelegate>
{
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    ConnectionManager * connectionManager;
    NSString * otherRecipientId;
    NSMutableArray * recipientArray;
    
    int deletedIndexPath;
    
    NSMutableDictionary * otherDict;
    NSString * itemId;
    NSString * itemType;
    UITextField * currentField;
    //JVFloatLabeledTextView * footerCommentTxtf;
}
@end

@implementation AddOtherRecipientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    deletedIndexPath =0;
    
    [self registerForKeyboardNotifications];
    
    otherDict = [[NSMutableDictionary alloc] init];
    _saveBtn.layer.cornerRadius = 3;

    
    visiblePopTipViews = [[NSMutableArray alloc]init];
//    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width-40,100)];
//    [tableFooterView setBackgroundColor:[UIColor clearColor]];
//    
//    footerCommentTxtf = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(10,30,self.view.frame.size.width-20,70)];
//    //footerLabel.text = @"Footer view";
//    footerCommentTxtf.delegate = self;
//    footerCommentTxtf.placeholder = @"Comment";
//    footerCommentTxtf.textColor = [UIColor whiteColor];
//    footerCommentTxtf.backgroundColor = [UIColor clearColor];
//    footerCommentTxtf.layer.borderWidth =0.5;
//    footerCommentTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
//    footerCommentTxtf.layer.cornerRadius =3;
//    footerCommentTxtf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
//    [tableFooterView addSubview:footerCommentTxtf];
//    
//    [self.recipientOtherTableView setTableFooterView:tableFooterView];
    
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    recipientArray = [[NSMutableArray alloc] init];
    
   // recipientArray = [[[[_recipientOther valueForKey:@"pi_info"]valueForKey:@"beneficiary"] valueForKey:@"others"]valueForKey:@"total"];
    
    
    if([_banificiaryType isEqualToString:@"personalItems"])
    {
        
        otherDict = [[[_recipientOther valueForKey:@"pi_info"]valueForKey:@"beneficiary"] mutableCopy];
        
        itemId = [_recipientOther valueForKey:@"pi_id"];
        itemType = @"personal_item";
        
        
    }
    
    else if([_banificiaryType isEqualToString:@"estate"])
    {
        
        otherDict = [[_recipientOther valueForKey:@"recipent"] mutableCopy];
        itemId = [_recipientOther valueForKey:@"estate_id"];
        itemType = @"estate";
        
    }
    
    
    if([_banificiaryType isEqualToString:@"estate_items"])
    {
        
        otherDict = [[_recipientOther valueForKey:@"recipent"] mutableCopy];
        itemId = [_recipientOther valueForKey:@"estate_items_id"];
        itemType = @"estate_items";
        
    }
    
    if([_banificiaryType isEqualToString:@"individual_items"])
    {
        
        
        otherDict = [[_recipientOther valueForKey:@"recipent"] mutableCopy];
        itemId = [_recipientOther valueForKey:@"individual_items_id"];
        itemType = @"individual_items";
        
    }
    
    
    
    
    for (NSMutableDictionary * dict in [[otherDict valueForKey:@"others"]valueForKey:@"total"])
    {
        [recipientArray addObject:dict];
    }
    
    if(recipientArray.count==0)
    {
        [self addMoreBtnAction:nil];
    }
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addMoreTrusteeBtnAction:(id)sender
{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [recipientArray count];
    
    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    //ChildTableViewCell *cell;
    
    TrustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[TrustryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    
    NSDictionary * otherrRecip = [recipientArray objectAtIndex:indexPath.row];
    
    
    
    cell.nameTextF.text = [otherrRecip valueForKey:@"name"];
    cell.contactInfoTxtF.text = [otherrRecip valueForKey:@"contact_info"];
    cell.aditionalInfoTxtV.text = [otherrRecip valueForKey:@"additional_info"];
    
    
//    if(indexPath.row ==0)
//    {
//        cell.deleteBtn.hidden = YES;
//    }
    
    
    cell.deleteBtn.tag = [[otherrRecip valueForKey:@"id"] integerValue];
    cell.deleteBtn.accessibilityIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    [cell.deleteBtn addTarget:self action:@selector(deleteSecondaryRecipient:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"the person or persons who will receive benefits from you in the form of cash or property .";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        /* Some options to try.
         */
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        //popTipView.hasGradientBackground = NO;
        //popTipView.cornerRadius = 2.0;
        //popTipView.sidePadding = 30.0f;
        //popTipView.topMargin = 20.0f;
        //popTipView.pointerSize = 50.0f;
        //popTipView.hasShadow = NO;
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}

- (IBAction)saveBtnAction:(id)sender
{
    

    NSMutableArray * users_info = [[NSMutableArray alloc] init];
    
    
    for (int i=0; i<recipientArray.count;i++)
    {
        NSIndexPath  *indexPaths = [NSIndexPath indexPathForRow:i inSection:0];
        
        TrustryTableViewCell * selecCell = [_recipientOtherTableView cellForRowAtIndexPath:indexPaths];
        
        
        if(selecCell.nameTextF.text.length ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter name and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        if(selecCell.contactInfoTxtF.text.length ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter contact Info and save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        if(selecCell.aditionalInfoTxtV.text.length ==0)
        {
            [Utils showAlertView:@"Message" message:@"Please enter aditional info save" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        
        
        NSMutableDictionary * piInfo = [[NSMutableDictionary alloc] init];
        [piInfo setValue: [[recipientArray objectAtIndex:i]valueForKey:@"id"] forKey:@"id"];
        [piInfo setValue:selecCell.nameTextF.text forKey:@"name"];
        [piInfo setValue:selecCell.contactInfoTxtF.text forKey:@"contact_info"];
        [piInfo setValue:selecCell.aditionalInfoTxtV.text forKey:@"additional_info"];
        [piInfo setValue:@"" forKey:@"comments"];
        
        [users_info addObject:piInfo];
    }
    

    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:itemId forKey:@"item_id"];
    [dataDict setValue:@"others" forKey:@"user_type"];
    [dataDict setValue:itemType forKey:@"item_type"];
    [dataDict setValue:users_info forKey:@"users_info"];
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateBeneficiaryUsers"];
    
}


-(void)deleteSecondaryRecipient:(UIButton*)sender
{
    otherRecipientId = [NSString stringWithFormat:@"%ld", (long)sender.tag];
    deletedIndexPath = sender.accessibilityIdentifier.intValue;
    
    [Utils showAlertViewWithTag:200 title:alertTitle message:@"You want to delete Other recipient" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex ==0)
    {
        
    }
    
    
    if(alertView.tag == 200 && buttonIndex ==1)
    {
        [self deletePI];
    }
    
}


-(void)deletePI
{
    
//GET:   {URL}/ removeBeneficiary/{testator_id}/{ beneficiary_id}/{item_id}/{user_type}
//    Note( item_id may be of (individual_items,estate,estate_items,personal_items  id))
//    And user type may be (child,others,primary,secondary)

    
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    NSString * trustyUrl = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",@"removeBeneficiary",[userData valueForKey:@"id"],otherRecipientId,itemId,@"others"];
    //http://localhost/iwill/index.php/deleteTrusty/$testator_id/$trusty_id
    
    
    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:trustyUrl];
    
}

- (IBAction)addMoreBtnAction:(id)sender

{
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    
//    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
//    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
//    NSString * trustyUrl = [NSString stringWithFormat:@"%@/%@/%@/%@",@"addMoreToPI",[userData valueForKey:@"id"],[_recipientOther valueForKey:@"pi_id"],@"other"];
//
//    [connectionManager getDataFromServerWithSessionTastmanagerWithGET:@"" withInput:dataDict andDelegate:self andURL:trustyUrl];
    
    
    
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    [dataDict setValue:itemId forKey:@"item_id"];
    [dataDict setValue:@"others" forKey:@"user_type"];
    [dataDict setValue:itemType forKey:@"item_type"];
    
    
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addUserToItem"];
    
    
    
    
    
}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
    
}
-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            if([[aResponceDic valueForKey:@"method"]isEqualToString:@"addUserToItemC"])
            {
                
                [recipientArray removeAllObjects];
                
                [recipientArray addObject:[aResponceDic valueForKey:@"Result"]];
                [_recipientOtherTableView reloadData];
            }
            
            
            else if([[aResponceDic valueForKey:@"method"]isEqualToString:@"removeBeneficiaryC"])
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                
                [recipientArray removeObjectAtIndex:deletedIndexPath];
                [_recipientOtherTableView reloadData];
                
            }
            
            
            else
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
                
            }
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
        
            
            [Utils showAlertViewWithTag:100 title:alertTitle message:executerCountZeroORNowitnessAdded delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK"];
        }
        else{
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    CGPoint origin = textView.frame.origin;
    CGPoint point = [textView.superview convertPoint:origin toView:self.recipientOtherTableView];
    NSIndexPath * indexPath = [self.recipientOtherTableView indexPathForRowAtPoint:point];
    TrustryTableViewCell * selecCell = [_recipientOtherTableView cellForRowAtIndexPath:indexPath];
    
    
    NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
    
    [perDict setValue:[[[[otherDict valueForKey:@"others"]valueForKey:@"total"]objectAtIndex:indexPath.row]valueForKey:@"id"] forKey:@"id"];
    
    [perDict setValue:selecCell.nameTextF.text forKey:@"name"];
    [perDict setValue:selecCell.contactInfoTxtF.text forKey:@"contact_info"];
    [perDict setValue:selecCell.aditionalInfoTxtV.text forKey:@"additional_info"];
    [perDict setValue:@"" forKey:@"comments"];
    
    NSMutableArray * arr = [[[otherDict valueForKey:@"others"] valueForKey:@"total"]  mutableCopy];
    [arr replaceObjectAtIndex:indexPath.row withObject:perDict];
    NSDictionary * childs = [[otherDict valueForKey:@"others"] mutableCopy];
    [childs setValue:arr forKey:@"total"];
    [otherDict setValue:childs forKey:@"others"];
    [recipientArray removeAllObjects];
    for (NSDictionary * indD in arr) {
        [recipientArray addObject:indD];
    }
    
}



-(void)textFieldDidEndEditing:(UITextField *)textField

{
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.recipientOtherTableView];
    NSIndexPath * indexPath = [self.recipientOtherTableView indexPathForRowAtPoint:point];
    TrustryTableViewCell * selecCell = [_recipientOtherTableView cellForRowAtIndexPath:indexPath];
    
    
    NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
    
    [perDict setValue:[[[[otherDict valueForKey:@"others"]valueForKey:@"total"]objectAtIndex:indexPath.row]valueForKey:@"id"] forKey:@"id"];
    
    [perDict setValue:selecCell.nameTextF.text forKey:@"name"];
    
    [perDict setValue:selecCell.contactInfoTxtF.text forKey:@"contact_info"];
    
    [perDict setValue:selecCell.aditionalInfoTxtV.text forKey:@"additional_info"];
    
    [perDict setValue:@"" forKey:@"comments"];
    
    NSMutableArray * arr = [[[otherDict valueForKey:@"others"] valueForKey:@"total"]  mutableCopy];
    [arr replaceObjectAtIndex:indexPath.row withObject:perDict];
    NSDictionary * childs = [[otherDict valueForKey:@"others"] mutableCopy];
    [childs setValue:arr forKey:@"total"];
    [otherDict setValue:childs forKey:@"others"];
    
    [recipientArray removeAllObjects];
    for (NSDictionary * indD in arr) {
        [recipientArray addObject:indD];
    }
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textField
{
    currentField = textField;
    return YES;
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    currentField = (UITextField*)textView;
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    // _UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.recipientOtherTableView.contentInset = contentInsets;
    self.recipientOtherTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [UIView animateWithDuration:.3 animations:^(void)
     {
         self.recipientOtherTableView.contentInset = UIEdgeInsetsZero;
         self.recipientOtherTableView.scrollIndicatorInsets = UIEdgeInsetsZero;
     }];
}


@end
