//
//  AddPrimaryRecipientViewController.h
//  IWill
//
//  Created by A1AUHAIG on 6/12/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CMPopTipView.h"

@interface AddPrimaryRecipientViewController : UIViewController<CMPopTipViewDelegate>
- (IBAction)backBtnAction:(id)sender;
- (IBAction)addMoreBtnAction:(id)sender;

@property(nonatomic,weak)IBOutlet UITableView * recipientPrimaryTableView;
@property(nonatomic,strong) NSMutableDictionary * recipientPrimary;
- (IBAction)saveBtnAction:(id)sender;
@property(nonatomic,strong) NSString * banificiaryType;
@property(nonatomic,strong) IBOutlet UIButton * saveBtn;

@end
