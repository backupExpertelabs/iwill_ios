//
//  MyChildRecipientViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/8/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "MyChildRecipientViewController.h"
#import "JVFloatLabeledTextView.h"
#import "Constant.h"
#import "ConnectionManager.h"
#import "Utils.h"
#import "MyWillTrusteeListTableViewCell.h"
@interface MyChildRecipientViewController ()<UITextFieldDelegate,UITextViewDelegate,ConnectionManager_Delegate>
{
    id                currentPopTipViewTarget;
    NSMutableArray    *visiblePopTipViews;
    ConnectionManager * connectionManager;
    JVFloatLabeledTextView *footerCommentTxtf;
    UITextField * currentField;
    NSMutableDictionary*childDict;
    
    
    
}
@end

@implementation MyChildRecipientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    childDict = [[NSMutableDictionary alloc] init];
    
    
    _saveBtn.layer.cornerRadius = 3;
    
    [self registerForKeyboardNotifications];
    visiblePopTipViews = [[NSMutableArray alloc]init];
    connectionManager = [[ConnectionManager alloc] init];
    connectionManager.delegate = self;
    
    
    //    _recipientChildTableView.layer.borderWidth = 1;
    //    _recipientChildTableView.layer.borderColor = [UIColor blackColor].CGColor;
    
    
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width-0,100)];
    [tableFooterView setBackgroundColor:[UIColor clearColor]];
    
    
    footerCommentTxtf = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(0,30,self.view.frame.size.width-20,70)];
    //footerLabel.text = @"Footer view";
    footerCommentTxtf.delegate = self;
    footerCommentTxtf.placeholder = @"Comment";
    footerCommentTxtf.textColor = [UIColor whiteColor];
    footerCommentTxtf.backgroundColor = [UIColor clearColor];
    footerCommentTxtf.layer.borderWidth =0.5;
    footerCommentTxtf.layer.borderColor = [UIColor whiteColor].CGColor;
    footerCommentTxtf.layer.cornerRadius =2;
    footerCommentTxtf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [tableFooterView addSubview:footerCommentTxtf];
    [self.recipientChildTableView setTableFooterView:tableFooterView];
    
    if([_banificiaryType isEqualToString:@"personalItems"])
    {
        
        childDict = [[[_recipientChild valueForKey:@"pi_info"]valueForKey:@"beneficiary"] mutableCopy];
        
        
        
        
    }
    
    else if([_banificiaryType isEqualToString:@"estate"])
    {
        
        childDict = [[_recipientChild valueForKey:@"recipent"] mutableCopy];
        
        
    }
    
    
    if([_banificiaryType isEqualToString:@"estate_items"])
    {
        
        
        childDict = [[_recipientChild valueForKey:@"recipent"] mutableCopy];
        
    }
    
    if([_banificiaryType isEqualToString:@"individual_items"])
    {
        
        
        childDict = [[_recipientChild valueForKey:@"recipent"] mutableCopy];
        
    }
    
    
    footerCommentTxtf.text = [[childDict valueForKey:@"childs"]valueForKey:@"comments"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    //return 0;
    return [[[childDict valueForKey:@"childs"]valueForKey:@"total"]count];    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section ==0)
    {
        
        
        
        return [[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"] count];
    }
    
    else{
        
        
        
        return [[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"] count];
    }
    
    
    
    
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    MyWillTrusteeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[MyWillTrusteeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                     reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary * childDict1;
    if(indexPath.section ==0)
    {
        
        childDict1 =[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:indexPath.row];
        
        
    }
    
    if(indexPath.section ==1)
    {
        
        
        
        childDict1 =[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]objectAtIndex:indexPath.row];
        
        
    }
    
    
    
    
    cell.nameTextF.text = [NSString stringWithFormat:@"%@",[childDict1 valueForKey:@"percentage"]];
    cell.name.text =[childDict1 valueForKey:@"child_name"];
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addMoreTrusteeBtnAction:(id)sender
{
    
    
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width-0, 50)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width-0 , 50)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.layer.cornerRadius =3;
    label.backgroundColor = [Utils getBorderLineColor];
    label.layer.cornerRadius =3;

    NSString *string;
    if(section ==0)
    {
        string =@"Legal Child Information";
    }
    else{
        string =@"Adopted Child Information";
    }
    [label setText:string];
    [view addSubview:label];
    view.layer.cornerRadius =3;
    view.backgroundColor = [UIColor clearColor];
    //[view setBackgroundColor:[UIColor clearColor]]; //your background color...
    return view;
}

////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"the person or persons who will receive benefits from you in the form of cash or property .";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        /* Some options to try.
         */
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        //popTipView.hasGradientBackground = NO;
        //popTipView.cornerRadius = 2.0;
        //popTipView.sidePadding = 30.0f;
        //popTipView.topMargin = 20.0f;
        //popTipView.pointerSize = 50.0f;
        //popTipView.hasShadow = NO;
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [visiblePopTipViews addObject:popTipView];
        currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [visiblePopTipViews removeObject:popTipView];
    currentPopTipViewTarget = nil;
}


-(IBAction)updateChildRecientValue:(UIButton*)sender
{
    
    
    NSMutableArray * childDataArr = [[NSMutableArray alloc] init];
    
    int totalPercentage =0;
    
    for (int i=0; i<[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]count ];i++)
        
    {
        NSIndexPath  *legalChildInde = [NSIndexPath indexPathForRow:i inSection:0];
        
        MyWillTrusteeListTableViewCell * selecCell = [_recipientChildTableView cellForRowAtIndexPath:legalChildInde];
        
        NSString * percentageValue = selecCell.nameTextF.text;
        
        NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
        
        [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:i]valueForKey:@"id"] forKey:@"id"];
        [perDict setValue:percentageValue forKey:@"percentage"];
        
        
        totalPercentage = totalPercentage+percentageValue.intValue;
        
        [childDataArr addObject:perDict];
        
    }
    
    
    
    
    for (int i=0; i<[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]count ];i++)
        
    {
        NSIndexPath  *legalChildInde = [NSIndexPath indexPathForRow:i inSection:1];
        
        MyWillTrusteeListTableViewCell * selecCell = [_recipientChildTableView cellForRowAtIndexPath:legalChildInde];
        
        NSString * percentageValue = selecCell.nameTextF.text;
        
        NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
        
        [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]objectAtIndex:i]valueForKey:@"id"] forKey:@"id"];
        [perDict setValue:percentageValue forKey:@"percentage"];
        totalPercentage = totalPercentage+percentageValue.intValue;
        [childDataArr addObject:perDict];
        
    }
    
    
    if(totalPercentage > 100)
    {
        [Utils showAlertView:alertTitle message:@"You can not distribute your property more than 100 percentage" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        return;
    }
    
    
    //updateBeneficiaryChilds
    
    
    if (![Utils isInternetAvailable])
    {
        [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    [Utils startActivityIndicatorInView:self.view withMessage:@""];
    
    
    NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
    [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
    
    if([_banificiaryType isEqualToString:@"personalItems"])
    {
    
    [dataDict setValue:[_recipientChild valueForKey:@"pi_id"] forKey:@"item_id"];
    [dataDict setValue:@"personal_items" forKey:@"item_type"];
    }
    if([_banificiaryType isEqualToString:@"estate"])
    {
        
        [dataDict setValue:[_recipientChild valueForKey:@"estate_id"] forKey:@"item_id"];
        [dataDict setValue:@"estate" forKey:@"item_type"];
    }
    if([_banificiaryType isEqualToString:@"estate_items"])
    {
        
        [dataDict setValue:[_recipientChild valueForKey:@"estate_items_id"] forKey:@"item_id"];
        [dataDict setValue:@"estate_items" forKey:@"item_type"];
    }
    if([_banificiaryType isEqualToString:@"individual_items"])
    {
        
        [dataDict setValue:[_recipientChild valueForKey:@"individual_items_id"] forKey:@"item_id"];
        [dataDict setValue:@"individual_items" forKey:@"item_type"];
    }
    
    [dataDict setValue:childDataArr forKey:@"childs"];
    
    //(inPseudoEditMode) ? kLabelIndentedRect : kLabelRect;
    NSString * commentStr = footerCommentTxtf.text.length == 0 ?@"" :footerCommentTxtf.text;
    
    [dataDict setValue:commentStr forKey:@"comments"];
    NSLog(@"hdsghj %@",dataDict);
    [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateBeneficiaryChilds"];
}



#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"updateBeneficiaryChildsC"])
            {
                [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
            }
            
        }
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        
        else
            
        {
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(BOOL)checkTextValidation
{
    //    NSMutableArray * childDataArr = [[NSMutableArray alloc] init];
    //
    //
    //
    //    for (int i=0; i<[[[[[[_recipientChild valueForKey:@"pi_info"]valueForKey:@"beneficiary"] valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]count ];i++)
    //
    //    {
    //        NSIndexPath  *legalChildInde = [NSIndexPath indexPathForRow:i inSection:0];
    //
    //        MyWillTrusteeListTableViewCell * selecCell = [_recipientChildTableView cellForRowAtIndexPath:legalChildInde];
    //
    //        NSString * percentageValue = selecCell.nameTextF.text;
    //
    //        NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
    //
    //        [perDict setValue:[[[[[[[_recipientChild valueForKey:@"pi_info"]valueForKey:@"beneficiary"] valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:i]valueForKey:@"id"] forKey:@"id"];
    //        [perDict setValue:percentageValue forKey:@"percentage"];
    //        [childDataArr addObject:perDict];
    //
    //    }
    //
    //
    //
    //
    //    for (int i=0; i<[[[[[[_recipientChild valueForKey:@"pi_info"]valueForKey:@"beneficiary"] valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]count ];i++)
    //
    //    {
    //        NSIndexPath  *legalChildInde = [NSIndexPath indexPathForRow:i inSection:1];
    //
    //        MyWillTrusteeListTableViewCell * selecCell = [_recipientChildTableView cellForRowAtIndexPath:legalChildInde];
    //
    //        NSString * percentageValue = selecCell.nameTextF.text;
    //
    //        NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
    //
    //        [perDict setValue:[[[[[[[_recipientChild valueForKey:@"pi_info"]valueForKey:@"beneficiary"] valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]objectAtIndex:i]valueForKey:@"id"] forKey:@"id"];
    //        [perDict setValue:percentageValue forKey:@"percentage"];
    //        [childDataArr addObject:perDict];
    //
    //    }
    //
    //
    
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.recipientChildTableView];
    NSIndexPath * indexPath = [self.recipientChildTableView indexPathForRowAtPoint:point];
    MyWillTrusteeListTableViewCell * selecCell = [_recipientChildTableView cellForRowAtIndexPath:indexPath];
    NSString * percentageValue = selecCell.nameTextF.text;
    
    
    if(indexPath.section ==0)
    {
        
        NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
        
        [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:indexPath.row]valueForKey:@"id"] forKey:@"id"];
        
        [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:indexPath.row]valueForKey:@"child_name"] forKey:@"child_name"];
        
        [perDict setValue:percentageValue forKey:@"percentage"];
        
        NSMutableArray * arr = [[[[childDict valueForKey:@"childs"] valueForKey:@"total"] valueForKey:@"Legal_child"] mutableCopy];
        
        [arr replaceObjectAtIndex:indexPath.row withObject:perDict];
        
        NSDictionary * childs = [[childDict valueForKey:@"childs"] mutableCopy];
        NSDictionary * totalChilds = [[childs valueForKey:@"total"] mutableCopy];

        [totalChilds setValue:arr forKey: @"Legal_child"];
        [childs setValue:totalChilds forKey:@"total"];
        [childDict setValue:childs forKey:@"childs"];
    }
    
    else{
        
        NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
        
        [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]objectAtIndex:indexPath.row]valueForKey:@"id"] forKey:@"id"];
        
        [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]objectAtIndex:indexPath.row]valueForKey:@"child_name"] forKey:@"child_name"];
        
        [perDict setValue:percentageValue forKey:@"percentage"];
        
        NSMutableArray * arr = [[[[childDict valueForKey:@"childs"] valueForKey:@"total"] valueForKey:@"Adopted_child"] mutableCopy];
        
        [arr replaceObjectAtIndex:indexPath.row withObject:perDict];
        NSDictionary * childs = [[childDict valueForKey:@"childs"] mutableCopy];
        NSDictionary * totalChilds = [[childs valueForKey:@"total"] mutableCopy];
        
        [totalChilds setValue:arr forKey: @"Adopted_child"];
        [childs setValue:totalChilds forKey:@"total"];
        [childDict setValue:childs forKey:@"childs"];
        
        
        
        //        [childDict setValue:[NSString stringWithFormat:@"%@",percentageValue] forKey:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Adopted_child"]objectAtIndex:indexPath.row]valueForKey:@"percentage"]];
    }
    
    
    //    MyWillTrusteeListTableViewCell * selecCell = [_recipientChildTableView cellForRowAtIndexPath:indexPath];
    //
    //    NSString * percentageValue = selecCell.nameTextF.text;
    //    NSMutableDictionary * perDict = [[NSMutableDictionary alloc] init];
    //
    //    [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:indexPath.row]valueForKey:@"id"] forKey:@"id"];
    //
    //    [perDict setValue:[[[[[childDict valueForKey:@"childs"]valueForKey:@"total"]valueForKey:@"Legal_child"]objectAtIndex:indexPath.row]valueForKey:@"child_name"] forKey:@"child_name"];
    //
    //    [perDict setValue:percentageValue forKey:@"percentage"];
    //
    //
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textField
{
    currentField = textField;
     [self addToolBarForDoneButton:textField];
    return YES;
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    currentField = (UITextField*)textView;
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(string.length > 0)
    {
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return stringIsValid;
    }
    return YES;
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    // _UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.recipientChildTableView.contentInset = contentInsets;
    self.recipientChildTableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [UIView animateWithDuration:.3 animations:^(void)
     {
         self.recipientChildTableView.contentInset = UIEdgeInsetsZero;
         self.recipientChildTableView.scrollIndicatorInsets = UIEdgeInsetsZero;
     }];
}





-(void)addToolBarForDoneButton:(UITextField*)textF
{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    textF.inputAccessoryView = keyboardToolbar;
}

-(void)yourTextViewDoneButtonPressed
{
    [currentField resignFirstResponder];
}



@end
