//
//  ExecuterAddNewViewController.m
//  IWill
//
//  Created by A1AUHAIG on 6/16/18.
//  Copyright © 2018 A1AUHAIG. All rights reserved.
//

#import "ExecuterAddNewViewController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import "Constant.h"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface ExecuterAddNewViewController ()<UITextFieldDelegate,UITextViewDelegate,ConnectionManager_Delegate>
{
    ConnectionManager * connectionManager;
    UITextField * activeField;
    NSDictionary * exicuterDateDict;
    
    
}
@property (nonatomic, strong)    NSArray            *colorSchemes;
@property (nonatomic, strong)    NSDictionary    *contents;
@property (nonatomic, strong)    id                currentPopTipViewTarget;
@property (nonatomic, strong)    NSDictionary    *titles;
@property (nonatomic, strong)    NSMutableArray    *visiblePopTipViews;

@end

@implementation ExecuterAddNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerForKeyboardNotifications];
    
    connectionManager = [[ConnectionManager alloc]init];
    connectionManager.delegate = self;
    _executerCount = @"1";
    self.visiblePopTipViews = [NSMutableArray array];
    self.fullName.layer.borderWidth = 1;
    self.fullName.layer.borderColor = [UIColor whiteColor].CGColor;
    self.fullName.layer.cornerRadius = 3;
    self.fullName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    self.lastName.layer.borderWidth = 1;
    self.lastName.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lastName.layer.cornerRadius = 3;
    self.lastName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    self.dob.layer.borderWidth = 1;
    self.dob.layer.borderColor = [UIColor whiteColor].CGColor;
    self.dob.layer.cornerRadius = 3;
    self.dob.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    self.address.layer.borderWidth = 1;
    self.address.layer.borderColor = [UIColor whiteColor].CGColor;
    self.address.layer.cornerRadius = 3;
    self.address.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    self.relationship.layer.borderWidth = 1;
    self.relationship.layer.borderColor = [UIColor whiteColor].CGColor;
    self.relationship.layer.cornerRadius = 3;
    self.relationship.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    _saveBtn.layer.cornerRadius = 3;
    
    //_executerSegment.userInteractionEnabled = NO;
    int exeCount = _executerCount.intValue;
    _executerSegment.selectedSegmentIndex=exeCount-1;
    
    
    
    ////// DATE PICKER ///
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    
    self.datebackgroundView.hidden = YES;
    [self.datebackgroundView.layer insertSublayer:gradient atIndex:0];
    self.dateMainView.layer.cornerRadius = 4;
    self.iWilldatePicker.maximumDate = [NSDate date];
    self.dateSelectedBtn = [Utils buttonWithSaddowAndradius:self.dateSelectedBtn];
    
    [_saveBtn setTitle:@"SAVE" forState:UIControlStateNormal];

    
    for (NSDictionary * indi in _executerList )
    {
        if(_executerSegment.selectedSegmentIndex+1 == [[indi valueForKey:@"type"] integerValue])
        {
            
            exicuterDateDict = indi;
            _fullName.text = [indi valueForKey:@"first_name"];
            _dob.text = [indi valueForKey:@"dob"];
            _address.text = [indi valueForKey:@"address"];
            _relationship.text = [indi valueForKey:@"relationship"];
            _saveBtn.hidden = NO;
            [_saveBtn setTitle:@"UPDATE" forState:UIControlStateNormal];
            break;

        }
        
    
    }
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)executerSegmentAction:(UISegmentedControl*)sender

{
    
    int segmentCount = sender.selectedSegmentIndex+1;
    _executerCount = [NSString stringWithFormat:@"%d",sender.selectedSegmentIndex+1];
    
    for (NSDictionary * indi in _executerList )
    {
       if(segmentCount == [[indi valueForKey:@"type"] integerValue])
       {
           exicuterDateDict = indi;

           _fullName.text = [indi valueForKey:@"first_name"];
           _dob.text = [indi valueForKey:@"dob"];
           _address.text = [indi valueForKey:@"address"];
           _relationship.text = [indi valueForKey:@"relationship"];
           _saveBtn.hidden = NO;
           [_saveBtn setTitle:@"UPDATE" forState:UIControlStateNormal];
           break;
       }
        
        
       else{
           
           
           _fullName.text = @"";
           _dob.text = @"";
           _address.text = @"";
           _relationship.text = @"";
           _saveBtn.hidden = NO;
           [_saveBtn setTitle:@"SAVE" forState:UIControlStateNormal];


       }
        
    }
    
}

#pragma mark - Connection Manager Delegates

-(void) didFailWithError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:@"" message:[[error userInfo] valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    });
}

-(void)responseReceived:(id)responseObject {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils stopActivityIndicatorInView:self.view];
        NSDictionary *aResponceDic = responseObject;
        if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 1) {
            
            
            if([[aResponceDic valueForKey:@"method"] isEqualToString:@"addExecutorC"])
            {
                
                int exeCount = _executerCount.intValue;
                exeCount = exeCount+1;
                _executerCount = [NSString stringWithFormat:@"%d",exeCount];
                if(exeCount ==4)
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                [Utils showAlertView:alertTitle message:@"Executer Added successfully. Please add one other Executer" delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];

                _executerSegment.selectedSegmentIndex= exeCount-1;
                _fullName.text = @"";
                _dob.text = @"";
                _address.text = @"";
                _relationship.text = @"";
                }
            }
            
        }
        
        
        else if ([[aResponceDic objectForKey:@"isSuccess"]intValue] == 0){
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"error"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
        
        else
            
        {
            
            [Utils showAlertView:alertTitle message:[aResponceDic objectForKey:@"message"] delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        }
    });
}


-(BOOL)checkTextValidation
{
    if(self.fullName.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:exicuterName delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
        
    }
    
    //    if(self.lastName.text.length ==0)
    //    {
    //        [Utils showAlertView:alertTitle message:executerAge delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
    //
    //        return NO;
    //
    //    }
    
    
    if(self.address.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:exicuterEmail delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
    }
    
    
    if(self.relationship.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerAddress delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
    }
    
    
    
    if(self.dob.text.length ==0)
    {
        [Utils showAlertView:alertTitle message:executerDOB delegate:nil cancelButtonTitle:alertOkButtonTitle otherButtonTitles:nil];
        
        return NO;
    }
    
    return YES;
}




- (IBAction)saveBtnAction:(UIButton*)sender {
    
    
    if([self checkTextValidation])
    {
        
        if([sender.titleLabel.text isEqualToString:@"SAVE"])
        {
        
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:alertNoInternetConection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        NSMutableDictionary * infoDict = [[NSMutableDictionary alloc]init];
        [infoDict setValue:_fullName.text forKey:@"first_name"];
        [infoDict setValue:@"" forKey:@"last_name"];
        [infoDict setValue:_dob.text forKey:@"dob"];
        [infoDict setValue:_address.text forKey:@"address"];
        [infoDict setValue:_relationship.text forKey:@"relationship"];
        [infoDict setValue:_executerCount forKey:@"executorType"];
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        [dataDict setValue:infoDict forKey:@"executor_info"];
        
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"addExecutor"];
    }
        else
        {
            [self updateBtnAction:sender];
        }
    }
    
    
    else{
        
    }
    
}



- (IBAction)updateBtnAction:(id)sender
{
    
        if (![Utils isInternetAvailable])
        {
            [Utils showAlertView:@"Alert" message:@"Sorry,No Internet, please try again !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            return;
        }
        [Utils startActivityIndicatorInView:self.view withMessage:@""];
        
        
        NSDictionary * userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
        
        
        NSMutableDictionary * excuterInfoDict = [[NSMutableDictionary alloc]init];
        
        [excuterInfoDict setValue:_fullName.text forKey:@"first_name"];
        [excuterInfoDict setValue:@"" forKey:@"last_name"];
        [excuterInfoDict setValue:_dob.text forKey:@"dob"];
        [excuterInfoDict setValue:_address.text forKey:@"address"];
        [excuterInfoDict setValue:_relationship.text forKey:@"relationship"];
        
        
        NSMutableDictionary * dataDict = [[NSMutableDictionary alloc]init];
        [dataDict setValue:[userData valueForKey:@"id"] forKey:@"testator_id"];
        [dataDict setValue:[exicuterDateDict valueForKey:@"executor_id"] forKey:@"executor_id"];
        [dataDict setValue:excuterInfoDict forKey:@"executor_info"];
        NSLog(@"hdsghj %@",dataDict);
        [connectionManager getDataFromServerWithSessionTastmanager:@"" withInput:dataDict andDelegate:self andURL:@"updateExecutor"];
    }

- (IBAction)backBrnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

////////////////////////   hint view example ////////////
- (void)dismissAllPopTipViews
{
    while ([self.visiblePopTipViews count] > 0) {
        CMPopTipView *popTipView = [self.visiblePopTipViews objectAtIndex:0];
        [popTipView dismissAnimated:YES];
        [self.visiblePopTipViews removeObjectAtIndex:0];
    }
}
- (IBAction)hintBtnAction:(id)sender
{
    
    [self dismissAllPopTipViews];
    
    if (sender == self.currentPopTipViewTarget) {
        // Dismiss the popTipView and that is all
        self.currentPopTipViewTarget = nil;
    }
    else {
        NSString *contentMessage = nil;
        
        contentMessage = @"a person or institution appointed by a testator to carry out the terms of their will in their absense.";
        
        //        NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //        UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //        UIColor *textColor = [colorScheme objectAtIndex:1];
        CMPopTipView *popTipView;
        
        popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        
        
        popTipView.backgroundColor = [UIColor whiteColor];
        popTipView.titleColor = [UIColor blackColor];
        popTipView.delegate = self;
        popTipView.textColor = [UIColor blackColor];
        
        /* Some options to try.
         */
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        //popTipView.hasGradientBackground = NO;
        //popTipView.cornerRadius = 2.0;
        //popTipView.sidePadding = 30.0f;
        //popTipView.topMargin = 20.0f;
        //popTipView.pointerSize = 50.0f;
        //popTipView.hasShadow = NO;
        
        popTipView.dismissTapAnywhere = YES;
        [popTipView autoDismissAnimated:YES atTimeInterval:5.0];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            [popTipView presentPointingAtView:button inView:self.view animated:YES];
        }
        else {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        
        [self.visiblePopTipViews addObject:popTipView];
        self.currentPopTipViewTarget = sender;
    }
}


#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    [self.visiblePopTipViews removeObject:popTipView];
    self.currentPopTipViewTarget = nil;
}






-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [activeField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGPoint origin = textField.frame.origin;
    

    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    activeField  = textField;
    
    
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == _dob)
    {
        [activeField resignFirstResponder];
        activeField = textField;
        self.datebackgroundView.hidden = NO;
        return NO;
    }
    return YES;
}


- (IBAction)dateSelectedBtnAction:(id)sender {
    
    _dob.text =[Utils getDateFromDateForAPI:_iWilldatePicker.date];
    self.datebackgroundView.hidden = YES;
}


-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    [textView resignFirstResponder];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


-(void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
}

@end
